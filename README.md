# Retake Tracker

#### Setup

3rd Party dependencies can be downloaded by executing the following command from the root of the repo:
```bash
python scripts/setup.py
```

#### Deploying on MacOS
```bash
python deploy_mac.py --qt-dir "/Users/matthew/Qt/5.10.1" --application-dir "/Users/matthew/development/build-retake_tracker-Desktop_Qt_5_10_1_clang_64bit-Release" --output-path "/Users/matthew/Documents/retake_tracker.dmg"
```

#### Deploying on Windows
```bash
python deploy_windows.py --qt-dir "C:\Qt\5.10.0" --application-dir "C:\development\build-retake_tracker-Desktop_Qt_5_10_0_MSVC2017_64bit-Release" --redist-dir "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Redist\MSVC\14.12.25810\x64\Microsoft.VC141.CRT" --output-path "C:/temp/RetakeTracker.zip"
```
#include "annotationmanager.h"

#include "models/annotation.h"

#include <QDebug>
#include <QEvent>
#include <QGraphicsRectItem>

QRectF computeRect(const QPoint& p1, const QPoint& p2);

AnnotationManager::AnnotationManager(PdfView* pdfView)
{
    _pdfView = pdfView;
    _pdfView->installEventFilter(this);
}

void AnnotationManager::addAnnotation(int id, const QRectF& rect)
{
    const auto imageSize = _pdfView->calculateImageSize();
    QRectF imageRect = Db::Annotation::calculateImageRect(rect, imageSize);

    auto graphicsItem = _pdfView->scene()->addRect(imageRect);
    setItemColor(graphicsItem, _itemColor);
    graphicsItem->setZValue(1.0);

    auto annotationPtr = QSharedPointer<AnnotationItem>(new AnnotationItem{rect, graphicsItem});
    _annotations[id] = annotationPtr;
}

void AnnotationManager::selectAnnotation(int id)
{
    if (_selecteditem) {
        setItemColor(_selecteditem->graphicsItem, _itemColor);
    }

    auto result = _annotations.find(id);
    if (result != _annotations.end()) {
        _selecteditem = result.value().data();
        setItemColor(_selecteditem->graphicsItem, _selectionColor);
    }
    else {
        _selecteditem = nullptr;
    }
}

void AnnotationManager::removeAnnotation(int id)
{
    auto result = _annotations.find(id);
    if (result != _annotations.end()) {
        _pdfView->scene()->removeItem(result.value()->graphicsItem);
        delete result.value()->graphicsItem;

        if (result.value() == _selecteditem) {
            _selecteditem = nullptr;
        }

        _annotations.erase(result);
    }
}

void AnnotationManager::clearAnnotations()
{
    for (auto& annotation : _annotations) {
        _pdfView->scene()->removeItem(annotation->graphicsItem);
        delete annotation->graphicsItem;
    }

    _annotations.clear();
    _selecteditem = nullptr;
}

void AnnotationManager::setItemColor(QGraphicsRectItem* item, const QColor& color)
{
    QPen pen;
    pen.setColor(color);
    item->setPen(pen);
}

bool AnnotationManager::eventFilter(QObject *obj, QEvent *event)
{
    auto eventType = event->type();
    if (eventType == QEvent::MouseButtonPress) {
        auto mouseEvent = static_cast<QMouseEvent*>(event);
        onMouseDown(mouseEvent);
    }
    else if (eventType == QEvent::MouseButtonRelease) {
        auto mouseEvent = static_cast<QMouseEvent*>(event);
        onMouseUp(mouseEvent);
    }
    else if (eventType == QEvent::MouseMove) {
        auto mouseEvent = static_cast<QMouseEvent*>(event);
        onMouseMove(mouseEvent);
    }
    else if (eventType == QEvent::Resize) {
        updateAnnotations();
    }

    return QObject::eventFilter(obj, event);
}

void AnnotationManager::onMouseDown(QMouseEvent* event)
{
    _mouseDownPos = event->pos();
    _mouseIsDown = true;

    _activeAnnotation = _pdfView->scene()->addRect(computeRect(_mouseDownPos, _mouseDownPos));
    _activeAnnotation->setZValue(1.0);
    setItemColor(_activeAnnotation, _itemColor);

}

void AnnotationManager::onMouseMove(QMouseEvent* event)
{
    if (_mouseIsDown) {
        _activeAnnotation->setRect(computeRect(_mouseDownPos, event->pos()));
    }
}

void AnnotationManager::onMouseUp(QMouseEvent* event)
{
    auto imageRect = computeRect(_mouseDownPos, event->pos());
    auto imageSize = _pdfView->calculateImageSize();
    QRectF normalizedRect = Db::Annotation::calculateNormalizedRect(imageRect, imageSize);

    if (annotationCreatedHook) {
        auto annotationId = annotationCreatedHook(normalizedRect);
        if (annotationId) {
            auto annotationPtr = QSharedPointer<AnnotationItem>(new AnnotationItem{normalizedRect, _activeAnnotation});
            _annotations[annotationId] = annotationPtr;
        }
        else {
            _pdfView->scene()->removeItem(_activeAnnotation);
            delete _activeAnnotation;
        }
    }

    _mouseIsDown = false;
    _activeAnnotation = nullptr;
}

QRectF computeRect(const QPoint& p1, const QPoint& p2)
{
    QPointF topLeft, bottomRight;

    if (p1.x() < p2.x()) {
        topLeft.setX(static_cast<qreal>(p1.x()));
        bottomRight.setX(static_cast<qreal>(p2.x()));
    }
    else {
        topLeft.setX(static_cast<qreal>(p2.x()));
        bottomRight.setX(static_cast<qreal>(p1.x()));
    }

    if (p1.y() < p2.y()) {
        topLeft.setY(static_cast<qreal>(p1.y()));
        bottomRight.setY(static_cast<qreal>(p2.y()));
    }
    else {
        topLeft.setY(static_cast<qreal>(p2.y()));
        bottomRight.setY(static_cast<qreal>(p1.y()));
    }

    return QRectF(topLeft, bottomRight);
}

void AnnotationManager::updateAnnotations()
{
    auto imageSize = _pdfView->calculateImageSize();

    for (auto& annotation : _annotations) {
        QRectF imageRect = Db::Annotation::calculateImageRect(annotation->normalizedRect, imageSize);
        annotation->graphicsItem->setRect(imageRect);
    }
}


#ifndef ANNOTATIONMANAGER_H
#define ANNOTATIONMANAGER_H

#include "widgets/pdfview.h"

#include <QObject>
#include <QMouseEvent>
#include <QVector>
#include <QGraphicsScene>
#include <QRectF>
#include <QHash>
#include <QSharedPointer>

#include <functional>


class AnnotationManager : public QObject
{
    Q_OBJECT

public:
    AnnotationManager(PdfView* pdfView);

public:
    std::function<int(const QRectF&)> annotationCreatedHook;

    void addAnnotation(int id, const QRectF& rect);
    void removeAnnotation(int id);
    void clearAnnotations();
    void selectAnnotation(int id);

private:
    void updateAnnotations();
    void setItemColor(QGraphicsRectItem* item, const QColor& color);
protected:
    bool eventFilter(QObject *obj, QEvent *event);

private:
    void onMouseDown(QMouseEvent* event);
    void onMouseMove(QMouseEvent* event);
    void onMouseUp(QMouseEvent* event);

private:
    bool _mouseIsDown = false;
    QPoint _mouseDownPos;

    PdfView* _pdfView;

    QGraphicsRectItem* _activeAnnotation = nullptr;

    struct AnnotationItem
    {
       AnnotationItem(const QRectF& rect, QGraphicsRectItem* item)
           :normalizedRect(rect), graphicsItem(item) {}

       QRectF normalizedRect;
       QGraphicsRectItem* graphicsItem;
    };

    QColor _itemColor = Qt::GlobalColor::red;
    QColor _selectionColor = Qt::GlobalColor::yellow;

    AnnotationItem* _selecteditem = nullptr;
    QHash<int, QSharedPointer<AnnotationItem>> _annotations;
};

#endif // ANNOTATIONMANAGER_H

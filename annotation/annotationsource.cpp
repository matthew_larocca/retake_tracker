#include "annotationsource.h"

void AnnotationSource::getAnnotationsForPage(int fileId, int pageIndex, Db::AnnotationVector& annotations)
{

    for (auto it = pendingCreateAnnotations.begin(); it != pendingCreateAnnotations.end(); ++it) {
        auto& annotation = it.value();

        if (annotation.page == pageIndex) {
            annotations.push_back(annotation);
        }
    }

    if (fileId != 0)  {
        Db::AnnotationVector a;
        _database->annotations->getAnnotationsForFile(fileId, a);

        for (auto& annotation: a) {
            if (annotation.page == pageIndex) {
                annotations.push_back(annotation);
            }
        }
    }

}

bool AnnotationSource::createAnnotation(Db::Annotation& annotation)
{
    int tempId = (pendingCreateAnnotations.size() + 1) * -1;
    annotation.id = tempId;
    pendingCreateAnnotations.insert(tempId, annotation);
    return true;
}

void AnnotationSource::deleteAnnotation(int id)
{
    if (id < 0) {
        pendingCreateAnnotations.remove(id);
    }
    else {
        pendingDeletedAnnotations.append(id);
    }
}

void AnnotationSource::getAnnotation(int id, Db::Annotation& annotation)
{
    if (id < 0) {
        annotation = pendingCreateAnnotations[id];
    }
    else {
        _database->annotations->getAnnotation(id, annotation);
    }
}

void AnnotationSource::updateAnnotation(Db::Annotation& annotation)
{
    if (annotation.id < 0) {
        pendingCreateAnnotations[annotation.id] = annotation;
    }
    else {
        pendingUpdateAnnotations[annotation.id] = annotation;
    }
}

void AnnotationSource::apply(int newFileId)
{
    for (auto& annotation: pendingCreateAnnotations) {
        annotation.fileId = newFileId;
        _database->annotations->createAnnotation(annotation);
    }

    for (auto& annotation: pendingUpdateAnnotations) {
        _database->annotations->updateAnnotation(annotation);
    }

    for (const auto& id : pendingDeletedAnnotations) {
        _database->annotations->deleteAnnotation(id);
    }
}

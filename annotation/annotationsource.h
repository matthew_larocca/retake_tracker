#ifndef IANNOTATIONMANAGER_H
#define IANNOTATIONMANAGER_H

#include "database/database.h"

#include <QHash>
#include <QVector>

class AnnotationSource
{
public:
    AnnotationSource(Db::Database* database) : _database(database) {}

public:
    void getAnnotationsForPage(int fileId, int pageIndex, Db::AnnotationVector& annotations);
    void getAnnotation(int id, Db::Annotation& annotation);
    void updateAnnotation(Db::Annotation& annotation);
    bool createAnnotation(Db::Annotation& annotation);
    void deleteAnnotation(int id);

    void apply(int newFileId);

private:
    Db::Database* _database;

    QHash<int, Db::Annotation> pendingCreateAnnotations;
    QHash<int, Db::Annotation> pendingUpdateAnnotations;
    QVector<int> pendingDeletedAnnotations;
};

#endif // IANNOTATIONMANAGER_H

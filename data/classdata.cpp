#include "classdata.h"

int ClassData::createClass()
{
    Q_ASSERT(_database != nullptr);

    Db::Class c;
    c.name = _name;
    _database->classes->createClass(c);

    for (const auto& learningObjective : _learningObjectives) {
        _database->classes->addLearningObjective(learningObjective, c.id);
    }

    //TODO: only create students who are new!
    for (auto& student : _students) {
        _database->students->createStudent(student);

        _database->classes->enrollStudentInClass(student.id, c.id);
    }

    return c.id;
}

bool ClassData::addLearningObjective(int id)
{
    if (_learningObjectives.contains(id)) {
        return false;
    }
    else {
        _learningObjectives.insert(id);
        return true;
    }
}

void ClassData::removeLearningObjective(int id)
{
    _learningObjectives.remove(id);
}

void ClassData::clearLearningObjectives()
{
    _learningObjectives.clear();
}


void ClassData::addStudent(const Db::Student& student)
{
    _students.push_back(student);
}

void ClassData::addStudents(const Db::StudentVector& students)
{
    _students += students;
}

void ClassData::clearStudents()
{
    _students.clear();
}

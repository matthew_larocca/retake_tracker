#ifndef CLASSDATA_H
#define CLASSDATA_H

#include "database/database.h"
#include "data/classobjectivedata.h"

#include <QSet>
#include <QString>

class ClassData : public ILearningObjectiveData
{
public:
    ClassData(Db::Database* database) : _database(database) {}

public:
    virtual bool addLearningObjective(int id) override;
    virtual void removeLearningObjective(int id) override;
    virtual void clearLearningObjectives() override;

    void addStudent(const Db::Student& student);
    void addStudents(const Db::StudentVector& students);
    void clearStudents();

    void setName(const QString& name) { _name = name; }

    int createClass();

private:
    Db::Database* _database;
    QSet<int> _learningObjectives;
    Db::StudentVector _students;
    QString _name;
};

#endif // CLASSDATA_H

#include "classobjectivedata.h"

void EditLearningObjectiveData::setExistingObjectives(const Db::LearningObjectiveVector& learningObjectives, int classId)
{
    _classId = classId;
    for (auto& classObjective: learningObjectives) {
        initialLearningObjectives.insert(classObjective.id);
    }

    editiedLearningObjectives = initialLearningObjectives;
}

bool EditLearningObjectiveData::addLearningObjective(int id)
{
    if (editiedLearningObjectives.contains(id)) {
        return false;
    }
    else {
        editiedLearningObjectives.insert(id);
        return true;
    }
}

void EditLearningObjectiveData::removeLearningObjective(int id)
{
    editiedLearningObjectives.remove(id);
}

void EditLearningObjectiveData::clearLearningObjectives()
{
    editiedLearningObjectives.clear();
}

void EditLearningObjectiveData::applyChanges()
{
    auto deleted = deletedItems();
    auto added = addedItems();

    for (const auto& addedId : added) {
        _database->classes->addLearningObjective(addedId, _classId);
    }

    for (const auto& deletedId : deleted) {
        Db::ClassObjective classObjective;
        _database->classObjectives->getClassObjective(_classId, deletedId, classObjective);

        _database->retakes->deleteRetakesForClassObjective(classObjective.id);
        _database->classObjectives->deleteClassObjective(classObjective.id);
    }
}

bool EditLearningObjectiveData::hasDeletedObjectives() const
{
    return deletedItems().size() > 0;
}

const QSet<int> EditLearningObjectiveData::deletedItems() const
{
    QSet<int> initial = initialLearningObjectives;
    return initial.subtract(editiedLearningObjectives);
}

const QSet<int> EditLearningObjectiveData::addedItems() const
{
    QSet<int> editied = editiedLearningObjectives;
    return editied.subtract(initialLearningObjectives);
}

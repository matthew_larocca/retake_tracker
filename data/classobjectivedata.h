#ifndef CLASSOBJECTIVEDATA_H
#define CLASSOBJECTIVEDATA_H

#include "database/database.h"
#include <QSet>

class ILearningObjectiveData {
public:
    virtual bool addLearningObjective(int id) = 0;
    virtual void removeLearningObjective(int id) = 0;
    virtual void clearLearningObjectives() = 0;
};


class EditLearningObjectiveData: public ILearningObjectiveData
{
public:
    EditLearningObjectiveData(Db::Database* database) : _database(database) {}

public:
    void setExistingObjectives(const Db::LearningObjectiveVector& learningObjectives, int classId);
    void applyChanges();
    bool hasDeletedObjectives() const;

    virtual bool addLearningObjective(int id) override;
    virtual void removeLearningObjective(int id) override;
    virtual void clearLearningObjectives() override;

private:
    const QSet<int> deletedItems() const;
    const QSet<int> addedItems() const;

private:
    Db::Database* _database;

    QSet<int> initialLearningObjectives;
    QSet<int> editiedLearningObjectives;

    int _classId;
};

#endif // CLASSOBJECTIVEDATA_H

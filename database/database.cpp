#include "database.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QVariant>
#include <QStandardPaths>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

#include <QSettings>


namespace Db {
    const QString SettingsLastBackupTime = "database/last_backup_time";
    const unsigned int Database::_currentVersion = 1;

    Database::Database()
    {
        _database = QSqlDatabase::addDatabase("QSQLITE");

        retakes = new Retakes(&_database);
        students = new Students(&_database);
        learningObjectives = new LearningObjectives(&_database);
        learningObjectiveCategories = new LearningObjectiveCategories(&_database);
        classes = new Classes(&_database);
        enrollments = new Enrollments(&_database);
        files = new Files(&_database);
        retakeVersions = new RetakeVersions(&_database);
        annotations = new Annotations(&_database);
        classObjectives = new ClassObjectives(&_database);
    }

    Database::~Database()
    {
        if (_database.isOpen()){
            _database.close();
        }
    }

    bool Database::init()
    {
        QDir appDataDirectory(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        if (!appDataDirectory.exists())
        {
            appDataDirectory.mkpath(".");
        }
        _databaseFile = appDataDirectory.filePath("retake_tracker.db");

        bool isNewDatabase = !QFileInfo::exists(_databaseFile);

        if (!isNewDatabase)
        {
            autosave();
        }

        _database.setDatabaseName(_databaseFile);
        bool databaseOpened = _database.open();

        if (databaseOpened)
        {
            if (isNewDatabase)
            {
                createNewDatabase();
            }

            migrate();
        }

        return databaseOpened;
    }

    bool Database::createBackup(const QString& path)
    {
        return QFile::copy(_databaseFile, path);
    }

    bool Database::autosave()
    {
        QDir appDataDirectory(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        QString settingsPath = appDataDirectory.filePath("Retake Tracker.ini");

        QSettings settings(settingsPath, QSettings::IniFormat);
        QDateTime currentTime = QDateTime::currentDateTime();

        if (settings.contains(SettingsLastBackupTime))
        {
            // Autosave should only happen once every 24 hours
            QDateTime lastBackupTime = settings.value(SettingsLastBackupTime).toDateTime();
            if (lastBackupTime.secsTo(currentTime) < 60 * 60 * 24)
            {
                return false;
            }
        }

        QString backupDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + QDir::separator() + "autosaves";
        QDir autosaveDataDirectory(QDir::cleanPath(backupDir));

        if (!autosaveDataDirectory.exists())
        {
            autosaveDataDirectory.mkpath(".");
        }

        QString fileName = "backup_" + currentTime.toString("MM_dd_yyyy_HH_mm_ss") + ".db";

        if(createBackup(autosaveDataDirectory.filePath(fileName)))
        {
            settings.setValue(SettingsLastBackupTime, currentTime);
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Database::createNewDatabase()
    {
        return applyMigration(":sql/migration1.sql");
    }

    bool Database::applyMigration(const QString& sqlMigrationResourcePath)
    {
        QFile sqlData(sqlMigrationResourcePath);
        sqlData.open(QFile::ReadOnly | QFile::Text);
        QTextStream textStream(&sqlData);

        QString sqlText = textStream.readAll();
        QStringList statements = sqlText.split(';', QString::SkipEmptyParts);

        QSqlQuery query;

        for (int i = 0; i < statements.size(); i++)
        {
            QString statement = statements[i].trimmed();
            if (statement.size() > 0)
            {
                if (!query.exec(statement))
                {
                    _lastError = query.lastError().text();
                    QMessageBox::critical(nullptr, "error", _lastError);
                    return false;
                }
            }
        }

        return true;
    }

    bool Database::migrate()
    {
        unsigned int databaseVersion =  getDatabaseVersion();

        for (unsigned int v = databaseVersion + 1; v <= _currentVersion; ++v)
        {
            QString sqlMigrationResourcePath = QString(":sql/migration%1.sql").arg(QString::number(v));
            applyMigration(sqlMigrationResourcePath);
        }

        // If a migration was performed, update the datbase version stored on disk
        if (databaseVersion + 1 <= _currentVersion)
        {
            setDatabaseVersion(_currentVersion);
        }

        return true;
    }

    bool Database::setDatabaseVersion(unsigned int currentVersion)
    {
        QSqlQuery query;
        query.prepare("update db_info set value = :currentVersion where key = 'version';");
        query.bindValue(":currentVersion", currentVersion);

        if (query.exec())
        {
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    QString Database::getLastError() const
    {
        return _lastError;
    }

    unsigned int Database::getDatabaseVersion() {
        QSqlQuery query;
        query.prepare("SELECT value from db_info WHERE key = 'version';");

        if (query.exec())
        {
            if (query.next())
            {
                return query.value("value").toUInt();
            }
        }
        else
        {
            _lastError = query.lastError().databaseText();
        }

        return 0;
    }

}

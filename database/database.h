#ifndef DATABASE_H
#define DATABASE_H

#include "models/retake.h"
#include "models/student.h"
#include "models/learningobjective.h"
#include "models/learningobjectivecategory.h"
#include "models/class.h"
#include "models/enrollments.h"
#include "models/file.h"
#include "models/retakeversion.h"
#include "models/annotation.h"
#include "models/classobjective.h"

#include <QString>
#include <QSqlDatabase>

#include <QString>
#include <QVector>
#include <QDateTime>

namespace Db {


    class Database
    {
    public:
        Database();
        ~Database();

    public:
        bool init();
        QString getLastError() const;


    public:
        Retakes* retakes;
        Students* students;
        LearningObjectives* learningObjectives;
        LearningObjectiveCategories* learningObjectiveCategories;
        Classes* classes;
        Enrollments* enrollments;
        Files* files;
        RetakeVersions* retakeVersions;
        Annotations* annotations;
        ClassObjectives* classObjectives;

    private:
        bool createBackup(const QString& path);
        bool autosave();

        bool createNewDatabase();

        unsigned int getDatabaseVersion();
        bool setDatabaseVersion(unsigned int currentVersion);
        bool migrate();
        bool applyMigration(const QString& sqlMigrationResourcePath);

    private:
        QSqlDatabase _database;
        QString _databaseFile;
        QString _lastError;

        static const unsigned int _currentVersion;
    };
}

#endif // DATABASE_H

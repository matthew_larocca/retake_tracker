#include "classdialog.h"
#include "ui_classdialog.h"

#include "widgets/util.h"

#include <QPushButton>

ClassDialog::ClassDialog(ClassData* classData, Db::Database *database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClassDialog),
    _database(database)
{
    ui->setupUi(this);
    ui->LearningObjectives->init(classData, database);
    ui->ClassRoster->init(classData);

    // Initial name is empty.  They must type something before we let them create a class.
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

}

ClassDialog::~ClassDialog()
{
    delete ui;
}

QString ClassDialog::getClassName() const{
    return ui->ClassNameText->text();
}

void ClassDialog::on_ClassNameText_textChanged(const QString &arg1)
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(arg1.length() > 0);
}

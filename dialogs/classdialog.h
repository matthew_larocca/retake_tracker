#ifndef CLASSDIALOG_H
#define CLASSDIALOG_H

#include "database/database.h"
#include "data/classdata.h"

#include <QDialog>

#include <QVector>

namespace Ui {
class ClassDialog;
}

class ClassDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ClassDialog(ClassData* classData, Db::Database *database, QWidget *parent = 0);
    ~ClassDialog();

    QString getClassName() const;

private slots:
    void on_ClassNameText_textChanged(const QString &arg1);

private:
    Ui::ClassDialog *ui;
    ClassData* _classData;
    Db::Database *_database;
};

#endif // CLASSDIALOG_H

#include "editclassobjectivesdialog.h"
#include "ui_editclassobjectivesdialog.h"

EditClassObjectivesDialog::EditClassObjectivesDialog(int classId, EditLearningObjectiveData* classObjectiveData, Db::Database* database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditClassObjectivesDialog),
    _database(database),
    _classObjectiveData(classObjectiveData)
{
    ui->setupUi(this);
    ui->ClassObjectives->init(classObjectiveData, database);
    populateRetakes(classId);
}

EditClassObjectivesDialog::~EditClassObjectivesDialog()
{
    delete ui;
}

void EditClassObjectivesDialog::populateRetakes(int classId)
{
    Db::LearningObjectiveVector learningObjectives;
    _database->learningObjectives->getLearningObjectivesForClass(classId, learningObjectives);

    _classObjectiveData->setExistingObjectives(learningObjectives, classId);
    ui->ClassObjectives->setSelectedObjectives(learningObjectives);
}

#ifndef EDITCLASSOBJECTIVESDIALOG_H
#define EDITCLASSOBJECTIVESDIALOG_H

#include "database/database.h"
#include "data/classobjectivedata.h"

#include <QDialog>

namespace Ui {
class EditClassObjectivesDialog;
}

class EditClassObjectivesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditClassObjectivesDialog(int classId, EditLearningObjectiveData* classObjectiveData, Db::Database* database, QWidget *parent = nullptr);
    ~EditClassObjectivesDialog();

private:
    void populateRetakes(int classId);

private:
    Ui::EditClassObjectivesDialog *ui;
    Db::Database* _database;
    EditLearningObjectiveData* _classObjectiveData;
};

#endif // EDITCLASSOBJECTIVESDIALOG_H

#include "enrollstudentdialog.h"
#include "ui_enrollstudentdialog.h"

#include "widgets/util.h"

#include <QSet>

EnrollStudentDialog::EnrollStudentDialog(int classId, Db::Database *database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EnrollStudentDialog),
    _database(database)
{
    ui->setupUi(this);

    populateComboBox(classId);
}

EnrollStudentDialog::~EnrollStudentDialog()
{
    delete ui;
}

void EnrollStudentDialog::populateComboBox(int classId)
{
    Db::EnrollmentVector enrollments;
    _database->enrollments->getEnrollmentsForClass(classId, enrollments);
    QSet<int> enrolledStudents;

    for (auto& enrollment : enrollments)
    {
        enrolledStudents.insert(enrollment.studentId);
    }

    Db::StudentVector students;
    _database->students->getAllStudents(students);

    for (int i = 0; i < students.size(); ++i)
    {
        if (!enrolledStudents.contains(students[i].id))
        {
            ui->StudentPicker->addItem(Util::getStudentFullName(students[i]), students[i].id);
        }
    }
}

int EnrollStudentDialog::getSelectedStudentId() const
{
    return ui->StudentPicker->currentData().toInt();
}

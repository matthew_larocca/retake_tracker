#ifndef ENROLLSTUDENTDIALOG_H
#define ENROLLSTUDENTDIALOG_H

#include "database/database.h"

#include <QDialog>

namespace Ui {
class EnrollStudentDialog;
}

class EnrollStudentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EnrollStudentDialog(int classId, Db::Database *database, QWidget *parent = 0);
    ~EnrollStudentDialog();

public:
    int getSelectedStudentId() const;

private:
    void populateComboBox(int classId);

private:
    Ui::EnrollStudentDialog *ui;
    Db::Database *_database;
};

#endif // ENROLLSTUDENTDIALOG_H

#include "learningobjectivedialog.h"
#include "ui_learningobjectivedialog.h"

#include "widgets/util.h"

#include <QFileDialog>
#include <QFile>

LearningObjectiveDialog::LearningObjectiveDialog(Db::Database* database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LearningObjectiveDialog)
{
    _database = database;
    ui->setupUi(this);
}

LearningObjectiveDialog::~LearningObjectiveDialog()
{
    delete ui;
}

void LearningObjectiveDialog::setLearningObjective(const Db::LearningObjective& learningObjective)
{
    _learningObjectiveId = learningObjective.id;

    ui->NameBox->setText(learningObjective.name);
    ui->DescriptionBox->setText(learningObjective.description);

    Db::RetakeVersionVector retakeVersions;
    _database->retakeVersions->getVersionsForLearningObjective(learningObjective.id, retakeVersions);
}


void LearningObjectiveDialog::getLearningObjective(Db::LearningObjective& learningObjective)
{
    learningObjective.name = ui->NameBox->text();
    learningObjective.description = ui->DescriptionBox->text();
}

void LearningObjectiveDialog::setCategoryText(const QString& categoryText)
{
    ui->CategoryText->setText(categoryText);
}



#ifndef LEARNINGOBJECTIVEDIALOG_H
#define LEARNINGOBJECTIVEDIALOG_H

#include "database/database.h"
#include "models/retakeversion.h"
#include "retakeversiondialog.h"

#include <QDialog>
#include <QSqlDatabase>

namespace Ui {
class LearningObjectiveDialog;
}

class LearningObjectiveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LearningObjectiveDialog(Db::Database* database, QWidget *parent = nullptr);
    ~LearningObjectiveDialog();

public:
    void setLearningObjective(const Db::LearningObjective& learningObjective);
    void getLearningObjective(Db::LearningObjective& learningObjective);
    void setCategoryText(const QString& categoryText);

private:
    Ui::LearningObjectiveDialog *ui;
    QString _documentHandle;
    Db::Database* _database;
    int _learningObjectiveId = 0;
};

#endif // LEARNINGOBJECTIVEDIALOG_H

#include "learningobjectiveexportdialog.h"
#include "ui_learningobjectiveexportdialog.h"

#include <QListWidgetItem>
#include <QVariant>

LearningObjectiveExportDialog::LearningObjectiveExportDialog(Db::Database* database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LearningObjectiveExportDialog),
    _database(database)
{
    ui->setupUi(this);

    initControls();
}

LearningObjectiveExportDialog::~LearningObjectiveExportDialog()
{
    delete ui;
}

void LearningObjectiveExportDialog::initControls()
{
    Db::LearningObjectiveCategoryVector categories;
    _database->learningObjectiveCategories->getCategories(categories);

    for (const auto& category : categories) {
        ui->CategoryBox->addItem(category.name, category.id);
    }
}

QVector<int> LearningObjectiveExportDialog::getLearningObjectives()
{
    QVector<int> learningObjectives;

    for (int i = 0; i < ui->LearningObjectiveList->count(); ++i) {
        auto item = ui->LearningObjectiveList->item(i);
        if (item->checkState() == Qt::CheckState::Checked) {
            learningObjectives.push_back(item->data(Qt::UserRole).toInt());
        }
    }

    return learningObjectives;
}

void LearningObjectiveExportDialog::on_CategoryBox_currentIndexChanged(int index)
{
    Q_UNUSED(index);

    if (ui->CategoryBox->currentIndex() < 0) {
        return;
    }
    const auto currentIndex = ui->CategoryBox->currentData().toInt();

    ui->LearningObjectiveList->clear();

    Db::LearningObjectiveVector learningObjectives;
    _database->learningObjectives->getLearningObjectivesInCategory(currentIndex, learningObjectives);

    for (const auto& learningObjective : learningObjectives) {
        auto item = new QListWidgetItem{};
        item->setData(Qt::UserRole, learningObjective.id);
        item->setText(learningObjective.name);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);

        ui->LearningObjectiveList->addItem(item);
    }
}

void LearningObjectiveExportDialog::on_SelectAllButton_clicked()
{
    setCheckState(Qt::CheckState::Checked);
}

void LearningObjectiveExportDialog::on_SelectNoneButton_clicked()
{
    setCheckState(Qt::CheckState::Unchecked);
}

void LearningObjectiveExportDialog::setCheckState(Qt::CheckState checkState)
{
    for (int i = 0; i < ui->LearningObjectiveList->count(); ++i)
    {
        ui->LearningObjectiveList->item(i)->setCheckState(checkState);
    }
}

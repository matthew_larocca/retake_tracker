#ifndef LEARNINGOBJECTIVEEXPORTDIALOG_H
#define LEARNINGOBJECTIVEEXPORTDIALOG_H

#include "database/database.h"

#include <QDialog>
#include <QVector>

namespace Ui {
class LearningObjectiveExportDialog;
}

class LearningObjectiveExportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LearningObjectiveExportDialog(Db::Database* database, QWidget *parent = nullptr);
    ~LearningObjectiveExportDialog();

public:
    QVector<int> getLearningObjectives();

private slots:
    void on_CategoryBox_currentIndexChanged(int index);

    void on_SelectAllButton_clicked();

    void on_SelectNoneButton_clicked();

private:
    void initControls();
    void setCheckState(Qt::CheckState checkState);

private:
    Ui::LearningObjectiveExportDialog *ui;

    Db::Database* _database;
};

#endif // LEARNINGOBJECTIVEEXPORTDIALOG_H

#include "learningobjectiveimportdialog.h"
#include "ui_learningobjectiveimportdialog.h"

#include "widgets/util.h"

#include <QFileDialog>

LearningObjectiveImportDialog::LearningObjectiveImportDialog(Db::Database* database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LearningObjectiveImportDialog),
    _database(database)
{
    ui->setupUi(this);

    Db::LearningObjectiveCategoryVector categories;
    _database->learningObjectiveCategories->getCategories(categories);

    for (const auto& category : categories) {
        ui->ExistingCategoryBox->addItem(category.name, category.id);
    }

    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(false);
}

LearningObjectiveImportDialog::~LearningObjectiveImportDialog()
{
    delete ui;
}

QString LearningObjectiveImportDialog::filePath() const
{
    return ui->FilePathText->text();
}

QString LearningObjectiveImportDialog::newCategoryText() const
{
    return ui->NewCategoryText->text();
}

bool LearningObjectiveImportDialog::isNewCategory() const
{
    return ui->NewCategoryRadioButton->isChecked();
}

void LearningObjectiveImportDialog::on_ExistingCategoryRadioButton_toggled(bool checked)
{
    if (ui->ExistingCategoryBox->count() > 0) {
        ui->ExistingCategoryBox->setEnabled(checked);
    }
}

int LearningObjectiveImportDialog::existingCategoryId() const
{
    if (ui->ExistingCategoryBox->count() > 0) {
        return ui->ExistingCategoryBox->currentData(Qt::UserRole).toInt();
    }
    else {
        return -1;
    }
}

void LearningObjectiveImportDialog::on_NewCategoryRadioButton_toggled(bool checked)
{
    ui->NewCategoryText->setEnabled(checked);
}

void LearningObjectiveImportDialog::on_BrowseFileButton_clicked()
{
    const auto fileName = QFileDialog::getOpenFileName(
                nullptr,
                "Open Data File",
                "",
                "Data (*.data)");

    if (!fileName.isEmpty()) {
        ui->FilePathText->setText(fileName);
        ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(true);
    }
}

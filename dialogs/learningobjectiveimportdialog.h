#ifndef LEARNINGOBJECTIVEIMPORTDIALOG_H
#define LEARNINGOBJECTIVEIMPORTDIALOG_H

#include "database/database.h"

#include <QDialog>

namespace Ui {
class LearningObjectiveImportDialog;
}

class LearningObjectiveImportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LearningObjectiveImportDialog(Db::Database* database, QWidget *parent = nullptr);
    ~LearningObjectiveImportDialog();

    QString filePath() const;
    QString newCategoryText() const;
    bool isNewCategory() const;
    int existingCategoryId() const;

private slots:
    void on_ExistingCategoryRadioButton_toggled(bool checked);

    void on_NewCategoryRadioButton_toggled(bool checked);

    void on_BrowseFileButton_clicked();

private:
    Ui::LearningObjectiveImportDialog *ui;
    Db::Database* _database;
};

#endif // LEARNINGOBJECTIVEIMPORTDIALOG_H

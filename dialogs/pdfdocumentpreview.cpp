#include "pdfdocumentpreview.h"
#include "ui_pdfdocumentpreview.h"

#include "widgets/util.h"

#include <QDebug>
#include <QGraphicsView>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QInputDialog>

PdfDocumentPreview::PdfDocumentPreview(AnnotationSource* annotationSource, Db::Database* database, int fileId, const QString& documentPath, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PdfDocumentPreview),
    _annotationSource(annotationSource)
{
    ui->setupUi(this);

    _fileId = fileId;
    _database = database;
    _library = new QtPdfium::Library();
    _document = _library->open(documentPath);
    _annotationManager = new AnnotationManager(ui->Pdf);
    _annotationManager->annotationCreatedHook = [this] (const QRectF& rect) -> int {
        return onAnnotationCreated(rect);
    };

    ui->Pdf->setDocument(_document);
    ui->Pdf->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->Pdf->installEventFilter(_annotationManager);
    setAnnotationsForPage(0);

    auto pageCount = _document->pageCount();

    ui->TotalPages->setText(" / " + QString::number(pageCount));
    ui->CurrentPage->setMaximum(pageCount);

    setWindowTitle(Util::fileNameFromPath(documentPath));
}

PdfDocumentPreview::~PdfDocumentPreview()
{
    delete ui;

    if (_document) {
        delete _document;
    }

    if (_library) {
        delete _library;
    }
}

void PdfDocumentPreview::on_CurrentPage_valueChanged(int arg1)
{
    int pageIndex = arg1 - 1;
    ui->Pdf->setPage(pageIndex);
    setAnnotationsForPage(pageIndex);
}

void PdfDocumentPreview::setAnnotationsForPage(int pageIndex)
{
    _annotationManager->clearAnnotations();
    ui->AnnotationList->clear();

    Db::AnnotationVector annotations;
    _annotationSource->getAnnotationsForPage(_fileId, pageIndex, annotations);

    for (auto& annotation: annotations) {
        const auto rect = Db::Annotation::fromDataString(annotation.data);
        _annotationManager->addAnnotation(annotation.id, rect);

        addAnnotationToList(annotation);
    }
}

void PdfDocumentPreview::addAnnotationToList(const Db::Annotation& annotation)
{
    auto item = new QListWidgetItem{};
    item->setText(Db::Annotation::TypeString(annotation.type));
    item->setData(Qt::UserRole, annotation.id);

    ui->AnnotationList->addItem(item);
}

int PdfDocumentPreview::onAnnotationCreated(const QRectF& rect)
{
    Q_UNUSED(rect);

    Db::Annotation annotation;
    annotation.fileId = _fileId;
    annotation.page = ui->Pdf->page()->getIndex();
    annotation.type = _newAnnotationType;
    annotation.data = Db::Annotation::toDataString(rect);

    const auto result = _annotationSource->createAnnotation(annotation);
    if (result){
        addAnnotationToList(annotation);
        return annotation.id;
    }
    else {
        return 0;
    }
}

void PdfDocumentPreview::on_DeleteAnnotation_clicked()
{
    auto currentItem = ui->AnnotationList->currentItem();
    if (!currentItem) return;

    QMessageBox messageBox;
    messageBox.setText("Delete Annotation");
    messageBox.setInformativeText("Are you sure to delete the selected annotation?");
    messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    messageBox.setDefaultButton(QMessageBox::No);

    int ret = messageBox.exec();

    if (ret == QMessageBox::Yes) {
        auto annotationId = currentItem->data(Qt::UserRole).toInt();
        _annotationSource->deleteAnnotation(annotationId);
        _annotationManager->removeAnnotation(annotationId);

        delete currentItem;
        ui->AnnotationList->setCurrentItem(nullptr);
    }
}

void PdfDocumentPreview::on_AnnotationList_itemClicked(QListWidgetItem *item)
{
    _annotationManager->selectAnnotation(item->data(Qt::UserRole).toInt());
}

void PdfDocumentPreview::on_comboBox_currentIndexChanged(int index)
{
    _newAnnotationType = index + 1;
}

void PdfDocumentPreview::on_toolButton_clicked()
{
    auto currentItem = ui->AnnotationList->currentItem();
    if (!currentItem) {
        return;
    }

    QStringList items = {"Name", "Date", "Class"};
    int current = items.indexOf(ui->AnnotationList->currentItem()->text());
    auto item = QInputDialog::getItem(nullptr, "Change Annotation Type", "Select a new type for this annotation", items, current);

    if (item.isEmpty()){
        return;
    }

    Db::Annotation annotation;
    _annotationSource->getAnnotation(currentItem->data(Qt::UserRole).toInt(), annotation);
    annotation.type = items.indexOf(item) + 1;
    _annotationSource->updateAnnotation(annotation);
    currentItem->setText(item);
}

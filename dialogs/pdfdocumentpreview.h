#ifndef PDFDOCUMENTPREVIEW_H
#define PDFDOCUMENTPREVIEW_H

#include "database/database.h"
#include "qtpdfium/qtpdfium.h"
#include "annotation/annotationmanager.h"
#include "annotation/annotationsource.h"

#include <QDialog>
#include <QRectF>
#include <QListWidgetItem>



namespace Ui {
class PdfDocumentPreview;
}

class PdfDocumentPreview : public QDialog
{
    Q_OBJECT

public:
    explicit PdfDocumentPreview(AnnotationSource* annotationSource, Db::Database* database, int fileId, const QString& documentPath, QWidget *parent = nullptr);
    ~PdfDocumentPreview();

private slots:
    void on_CurrentPage_valueChanged(int arg1);

    void on_DeleteAnnotation_clicked();

    void on_AnnotationList_itemClicked(QListWidgetItem *item);

    void on_comboBox_currentIndexChanged(int index);

    void on_toolButton_clicked();

private:
    void addAnnotationToList(const Db::Annotation& annotation);
    int onAnnotationCreated(const QRectF& rect);
    void setAnnotationsForPage(int pageIndex);

private:
    Ui::PdfDocumentPreview *ui;
    QtPdfium::Document* _document;
    QtPdfium::Library* _library;
    AnnotationManager* _annotationManager;
    AnnotationSource* _annotationSource;
    Db::Database* _database;
    int _fileId;

    int _newAnnotationType = 1;
};

#endif // PDFDOCUMENTPREVIEW_H

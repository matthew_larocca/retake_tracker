#include "retakedialog.h"
#include "ui_retakedialog.h"

#include "widgets/util.h"
#include "util/retakeprintjob.h"

#include <QPrintDialog>
#include <QPrinter>

RetakeDialog::RetakeDialog(Db::Database *database, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RetakeDialog),
    _database(database),
    _retakeId(-1),
    _useUiScheduledTime(false)
{
    ui->setupUi(this);
}

RetakeDialog::~RetakeDialog()
{
    delete ui;
}

void RetakeDialog::lockStudentSelect(int studentId)
{
    Db::Student student;
    _database->students->getStudent(studentId, student);
    ui->StudentBox->addItem(Util::getStudentFullName(student), studentId);
    ui->StudentBox->setEnabled(false);
}

void RetakeDialog::lockClassSelect(int classId)
{
    Db::Class c;
    _database->classes->getClass(classId, c);
    ui->ClassPicker->addItem(c.name, c.id);
    ui->ClassPicker->setEnabled(false);
}

void RetakeDialog::setClassSelectForStudent(int studentId)
{
    Db::ClassVector classes;
    _database->classes->getClassesForStudent(studentId, classes);
    Util::addClassesToComboBox(classes, ui->ClassPicker);

    if (classes.size() > 0)
    {
        setLearningObjectivesForClass(classes[0].id);
        ui->LearningObjectivePicker->setEnabled(true);
    }
    else
    {
        ui->LearningObjectivePicker->setEnabled(false);
    }
}

void RetakeDialog::initScheduleWidget(){
    ui->ScheduledDate->setDateTime(QDateTime::currentDateTime());
    connect(ui->ScheduledDate, &QDateTimeEdit::dateTimeChanged, this, &RetakeDialog::scheduledTimeChanged);

    setScheduleWidgetColorText(Qt::gray);
}

void RetakeDialog::newRetakeForStudent(int studentId)
{
    lockStudentSelect(studentId);
    setClassSelectForStudent(studentId);

    connect(ui->ClassPicker, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &RetakeDialog::classChanged);

    initScheduleWidget();
}

void RetakeDialog::newRetakeForClass(int classId)
{
    lockClassSelect(classId);
    setLearningObjectivesForClass(classId);
    setStudentsForClass(classId);

    initScheduleWidget();
}

void RetakeDialog::setStudentsForClass(int classId)
{
    Db::StudentVector students;
    _database->students->getStudentsInClass(classId, students);

    for(Db::Student& student : students)
    {
        ui->StudentBox->addItem(Util::getStudentFullName(student), student.id);
    }
}

void RetakeDialog::setLearningObjectivesForClass(int classId)
{
    Db::LearningObjectiveVector learningObjectives;
    _database->learningObjectives->getLearningObjectivesForClass(classId, learningObjectives);

    Util::addLearningObjectivesToComboBox(learningObjectives, ui->LearningObjectivePicker);

    if (learningObjectives.size() > 0) {
        setRetakeVersionsForLearningObjective(learningObjectives[0].id);
    }
}

void RetakeDialog::classChanged(int newIndex)
{
    ui->LearningObjectivePicker->clear();

    if (newIndex >= 0)
    {
        int classId = ui->ClassPicker->itemData(newIndex).toInt();
        setLearningObjectivesForClass(classId);

        ui->LearningObjectivePicker->setEnabled(true);
    }
    else
    {
        ui->LearningObjectivePicker->setEnabled(false);
    }

}

void RetakeDialog::setSelectIndexForId(QComboBox* comboBox, int id)
{
    for (int i = 0; i < comboBox->count(); ++i)
    {
        if (comboBox->itemData(i).toInt() == id)
        {
            comboBox->setCurrentIndex(i);
            return;
        }
    }
}

void RetakeDialog::modifyRetakeForStudent(int retakeId)
{
    _retakeId = retakeId;

    Db::Retake retake;
    _database->retakes->getRetake(retakeId, retake);

    lockStudentSelect(retake.studentId);

    setClassSelectForStudent(retake.studentId);

    setRetakeValues(retake);
}

void RetakeDialog::setRetakeVersionsForLearningObjective(int learningObjectiveId)
{
    ui->VersionBox->clear();

    Db::RetakeVersionVector retakeVersions;
    _database->retakeVersions->getVersionsForLearningObjective(learningObjectiveId, retakeVersions);
    Util::addRetakeVersionsToComboBox(ui->VersionBox, retakeVersions);
}

void RetakeDialog::setRetakeValues(const Db::Retake &retake)
{
    setSelectIndexForId(ui->ClassPicker, retake.classId);
    setSelectIndexForId(ui->LearningObjectivePicker, retake.learningObjectiveId);

    setRetakeVersionsForLearningObjective(retake.learningObjectiveId);
    setSelectIndexForId(ui->VersionBox, retake.versionId);

    ui->RetakeCompleteCheckbox->setChecked(retake.retakeCompleted);
    ui->CommentsText->setText(retake.comments);

    if (retake.scheduledDate.isValid())
    {
        ui->ScheduledDate->setDateTime(retake.scheduledDate.toLocalTime());
        _useUiScheduledTime = true;
        setScheduleWidgetColorText(Qt::black);
    }
    else
    {
        ui->ScheduledDate->setDateTime(QDateTime::currentDateTime());
        setScheduleWidgetColorText(Qt::gray);
    }

    connect(ui->ScheduledDate, &QDateTimeEdit::dateTimeChanged, this, &RetakeDialog::scheduledTimeChanged);

    void(QComboBox::*comboBoxIndexChangedFunc)(int) = &QComboBox::currentIndexChanged;
    connect(ui->LearningObjectivePicker, comboBoxIndexChangedFunc, this, &RetakeDialog::onLearningObjectiveChanged);
}

void RetakeDialog::modifyRetakeForClass(int retakeId)
{
    _retakeId = retakeId;

    Db::Retake retake;
    _database->retakes->getRetake(retakeId, retake);

    lockStudentSelect(retake.studentId);
    setClassSelectForStudent(retake.studentId);
    ui->LearningObjectivePicker->setEnabled(false);

    setClassSelectForStudent(retake.studentId);
}

void RetakeDialog::getRetake(Db::Retake& retake)
{
    retake.id = _retakeId;
    retake.learningObjectiveId = ui->LearningObjectivePicker->currentData().toInt();
    retake.classId = ui->ClassPicker->currentData().toInt();
    retake.studentId = ui->StudentBox->currentData().toInt();

    retake.retakeCompleted = false;

    auto versionIndex = ui->VersionBox->currentIndex();
    retake.versionId =  (versionIndex >= 0) ? ui->VersionBox->currentData().toInt() : 0;
    retake.comments = ui->CommentsText->text();

    retake.retakeCompleted = ui->RetakeCompleteCheckbox->isChecked();

    if (_useUiScheduledTime) {
        retake.scheduledDate = ui->ScheduledDate->dateTime().toUTC();
    }
    else {
        retake.scheduledDate = QDateTime();
    }

    //This is temporary until i rework the retake dialog.
    Db::ClassObjective classObjective;
    _database->classObjectives->getClassObjective(retake.classId, retake.learningObjectiveId, classObjective);
    retake.classObjectiveId = classObjective.id;
}

void RetakeDialog::scheduledTimeChanged(const QDateTime &dateTime)
{
    Q_UNUSED(dateTime);

    if (!_useUiScheduledTime) {
        _useUiScheduledTime = true;
        setScheduleWidgetColorText(Qt::black);
    }
}

void RetakeDialog::setScheduleWidgetColorText(const QColor& color)
{
    QPalette palette = QGuiApplication::palette();
    palette.setColor(QPalette::Text, color);

    ui->ScheduledDate->setPalette(palette);
}

void RetakeDialog::onLearningObjectiveChanged(int index)
{
    Q_UNUSED(index);

    int learningObjectiveId = ui->LearningObjectivePicker->currentData().toInt();

    Db::RetakeVersionVector retakeVersions;
    _database->retakeVersions->getVersionsForLearningObjective(learningObjectiveId, retakeVersions);

    ui->VersionBox->clear();
    Util::addRetakeVersionsToComboBox(ui->VersionBox, retakeVersions);
}

void RetakeDialog::on_LearningObjectivePicker_currentIndexChanged(int index)
{
    Q_UNUSED(index);

    auto learningObjectiveId = ui->LearningObjectivePicker->currentData().toInt();
    setRetakeVersionsForLearningObjective(learningObjectiveId);
}

void RetakeDialog::on_PrintButton_clicked()
{
    QPrinter printer;
    QPrintDialog dialog(&printer, nullptr);

    if (dialog.exec() == QDialog::Accepted) {
        Db::RetakeVector retakes;
        Db::Retake retake;
        _database->retakes->getRetake(_retakeId, retake);
        retakes.push_back(retake);

        RetakePrintJob printJob(_database);
        printJob.init(retakes);
        printJob.print(printer);
    }
}

#ifndef RETAKEDIALOG_H
#define RETAKEDIALOG_H

#include "database/database.h"

#include <QDialog>
#include <QComboBox>

namespace Ui {
class RetakeDialog;
}

class RetakeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RetakeDialog(Db::Database *database, QWidget *parent = 0);
    ~RetakeDialog();

public:
   void newRetakeForStudent(int studentId);
   void newRetakeForClass(int classId);
   void modifyRetakeForStudent(int retakeId);
   void modifyRetakeForClass(int retakeId);
   void getRetake(Db::Retake& retake);

private slots:
   void on_LearningObjectivePicker_currentIndexChanged(int index);

   void on_PrintButton_clicked();

private:
   void classChanged(int newIndex);
   void setLearningObjectivesForClass(int classId);
   void setRetakeVersionsForLearningObjective(int learningObjectiveId);
   void lockStudentSelect(int studentId);

   void setClassSelectForStudent(int studentId);
   void setSelectIndexForId(QComboBox* comboBox, int id);

   void lockClassSelect(int classId);
   void setStudentsForClass(int classId);

   void setRetakeValues(const Db::Retake& retake);
   void scheduledTimeChanged(const QDateTime &dateTime);

   void initScheduleWidget();
   void setScheduleWidgetColorText(const QColor& color);

   void onLearningObjectiveChanged(int index);

private:
    Ui::RetakeDialog *ui;
    Db::Database *_database;
    int _retakeId;
    bool _useUiScheduledTime;
};

#endif // RETAKEDIALOG_H

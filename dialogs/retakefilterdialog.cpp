#include "retakefilterdialog.h"
#include "ui_retakefilterdialog.h"

RetakeFilterDialog::RetakeFilterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RetakeFilterDialog)
{
    ui->setupUi(this);
}

RetakeFilterDialog::~RetakeFilterDialog()
{
    delete ui;
}


void RetakeFilterDialog::initWithParams(const Db::RetakeQueryParams& params)
{
    bool beginTimeValid = params.beginTime.isValid();
    bool endTimeValid = params.endTime.isValid();

    if (beginTimeValid && endTimeValid)
    {
        ui->StartDate->setDateTime(params.beginTime);
        ui->EndDate->setDateTime(params.endTime);
    }
    else if (beginTimeValid && !endTimeValid)
    {
        ui->StartDate->setDateTime(params.beginTime);
        ui->EndDate->setDateTime(params.beginTime.addDays(1));
        ui->EndTimeCheckbox->setChecked(false);
    }
    else if (!beginTimeValid && endTimeValid)
    {
        ui->StartDate->setDateTime(params.endTime.addDays(-1));
        ui->StartTimeCheckbox->setChecked(false);
        ui->EndDate->setDateTime(params.endTime);
    }
    else{
        QDateTime now(QDate::currentDate(), QTime(0,0));

        ui->StartDate->setDateTime(now);
        ui->StartTimeCheckbox->setChecked(false);

        ui->EndDate->setDateTime(now.addDays(1));
        ui->EndTimeCheckbox->setChecked(false);
    }

    ui->LimitBox->setValue((int)params.limit);

    switch(params.orderBy)
    {
    case Db::RetakeQueryParams::OrderBy::ScheduledDate:
        ui->OrderByBox->setCurrentIndex(0);
        break;

    case Db::RetakeQueryParams::OrderBy::CreatedDate:
        ui->OrderByBox->setCurrentIndex(1);
        break;
    }

    switch (params.orderDirection)
    {
    case Db::RetakeQueryParams::OrderDirection::Ascending:
        ui->OrderDirectionBox->setCurrentIndex(0);
        break;

    case Db::RetakeQueryParams::OrderDirection::Descending:
        ui->OrderDirectionBox->setCurrentIndex(1);
        break;
    }
}

void RetakeFilterDialog::applyParams(Db::RetakeQueryParams& params)
{
    if (ui->StartTimeCheckbox->checkState() == Qt::Checked)
    {
        params.beginTime = ui->StartDate->dateTime();
    }
    else
    {
        params.beginTime = QDateTime();
    }

    if (ui->EndTimeCheckbox->checkState() == Qt::Checked)
    {
        params.endTime = ui->EndDate->dateTime();
    }
    else
    {
        params.endTime = QDateTime();
    }

    switch (ui->OrderByBox->currentIndex())
    {
    case 0:
        params.orderBy = Db::RetakeQueryParams::OrderBy::ScheduledDate;
        break;

    case 1:
        params.orderBy = Db::RetakeQueryParams::OrderBy::CreatedDate;
        break;
    }

    switch (ui->OrderDirectionBox->currentIndex())
    {
    case 0:
        params.orderDirection = Db::RetakeQueryParams::OrderDirection::Ascending;
        break;

    case 1:
        params.orderDirection = Db::RetakeQueryParams::OrderDirection::Descending;
        break;
    }

    params.limit = ui->LimitBox->value();
}

void RetakeFilterDialog::on_EndDate_dateTimeChanged(const QDateTime &dateTime)
{
    Q_UNUSED(dateTime);
    ui->EndTimeCheckbox->setChecked(true);
}

void RetakeFilterDialog::on_StartDate_dateTimeChanged(const QDateTime &dateTime)
{
    Q_UNUSED(dateTime);
    ui->StartTimeCheckbox->setChecked(true);
}

#ifndef RETAKEFILTERDIALOG_H
#define RETAKEFILTERDIALOG_H

#include "models/retake.h"

#include <QDialog>

namespace Ui {
class RetakeFilterDialog;
}

class RetakeFilterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RetakeFilterDialog(QWidget *parent = 0);
    ~RetakeFilterDialog();

public:
    void initWithParams(const Db::RetakeQueryParams& params);
    void applyParams(Db::RetakeQueryParams& params);

private slots:
    void on_EndDate_dateTimeChanged(const QDateTime &dateTime);

    void on_StartDate_dateTimeChanged(const QDateTime &dateTime);

private:
    Ui::RetakeFilterDialog *ui;
};

#endif // RETAKEFILTERDIALOG_H

#include "retakeimportdialog.h"
#include "ui_retakeimportdialog.h"

#include <QFileDialog>
#include <QTreeWidgetItem>
#include <QList>

RetakeImportDialog::RetakeImportDialog(RetakeImporter& importer, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RetakeImportDialog),
    _importer(importer)
{
    ui->setupUi(this);
}

RetakeImportDialog::~RetakeImportDialog()
{
    delete ui;
}

void RetakeImportDialog::on_DocumentBrowseButton_clicked()
{
    QFileDialog dialog(this);
    if (dialog.exec() == QDialog::Accepted) {
        dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
        dialog.setNameFilter("Tab Separated Value (*.tsv)");

        auto filePath = dialog.selectedFiles()[0];
        ui->DocumentPath->setText(filePath);

        _importer.importRetakesFromTSV(filePath);
        populateImportedRetakes();
    }
}

void RetakeImportDialog::populateImportedRetakes()
{
    const auto& retakes = _importer.getRetakes();
    const auto& emails = _importer.getRetakeEmails();

    QList<QTreeWidgetItem*> importedRetakes;
    QList<QTreeWidgetItem*> importErrors;

    for (int i = 0; i < retakes.size(); i++) {
        const auto& retake = retakes[i];
        const auto& email = emails[i];

        auto widgetItem = new QTreeWidgetItem{};
        widgetItem->setText(0, retake.studentName);
        widgetItem->setText(1, email);

        if (retake.isValid()) {
            widgetItem->setText(2, retake.className);
            widgetItem->setText(3, retake.LearningObjectiveName);
            widgetItem->setText(4, retake.retakeVersionName);
            widgetItem->setText(5, retake.scheduledDate.date().toString());
            importedRetakes.push_back(widgetItem);
        }
        else {
            QString error;
            if (retake.studentId == -1) {
                error += "Unable to locate Student for email. ";
            }

            if (retake.classId == -1) {
                error += "Unable to locate class: " + retake.className + " ";
            }

            if (retake.learningObjectiveId == -1) {
                error += "Unable to locate learning objective: " + retake.LearningObjectiveName;
            }

            if (retake.versionId == -1) {
                error += "Selected Learning Objective has no versions entered into the system.";
            }

            widgetItem->setText(2, error);
            importErrors.push_back(widgetItem);
        }
    }

    ui->ImportSuccessList->addTopLevelItems(importedRetakes);
    ui->ImportSucessText->setText(QString("Sucessfully parsed %1 records.").arg(importedRetakes.size()));
    ui->TabContainer->setTabText(0, QString("Sucessful (%1)").arg(importedRetakes.size()));

    ui->ImportErrorList->addTopLevelItems(importErrors);
    ui->ImportErrorText->setText(QString("Unable to parse %1 records.").arg(importErrors.size()));
    ui->TabContainer->setTabText(1, QString("Errors (%1)").arg(importErrors.size()));
}

void RetakeImportDialog::on_ImportAsCompletedBox_toggled(bool checked)
{
    _importer.setImportAsComleted(checked);
}

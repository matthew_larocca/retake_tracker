#ifndef RETAKEIMPORTDIALOG_H
#define RETAKEIMPORTDIALOG_H

#include "import/retakeimporter.h"

#include <QDialog>

namespace Ui {
class RetakeImportDialog;
}

class RetakeImportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RetakeImportDialog(RetakeImporter& importer, QWidget *parent = nullptr);
    ~RetakeImportDialog();

private slots:
    void on_DocumentBrowseButton_clicked();

    void on_ImportAsCompletedBox_toggled(bool checked);

private:
    void populateImportedRetakes();

private:
    Ui::RetakeImportDialog *ui;

    RetakeImporter& _importer;
};

#endif // RETAKEIMPORTDIALOG_H

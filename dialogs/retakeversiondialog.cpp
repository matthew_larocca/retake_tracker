#include "retakeversiondialog.h"
#include "ui_retakeversiondialog.h"

#include "dialogs/pdfdocumentpreview.h"
#include "models/file.h"
#include "widgets/util.h"

#include <QFileDialog>


RetakeVersionDialog::RetakeVersionDialog(Db::Database* database, AnnotationSource* annotationSource, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RetakeVersionDialog),
    _database(database),
    _annotationSource(annotationSource)
{
    ui->setupUi(this);
}

RetakeVersionDialog::~RetakeVersionDialog()
{
    delete ui;
}

void RetakeVersionDialog::setRetakeVersion(const Db::RetakeVersion& retakeVersion)
{
    _versionId = retakeVersion.id;
    _fileId = retakeVersion.file_id;

    ui->NameBox->setText(retakeVersion.name);
    ui->DescriptionBox->setText(retakeVersion.description);

    // need to retrieve PDF file from the DB
    if (retakeVersion.file_id != 0) {
        Db::File file;
        _database->files->getFile(retakeVersion.file_id, file);

        _tempFile.open();
        _tempFile.write(file.data);
        _documentHandle = _tempFile.fileName();

        ui->DocumentPath->setText(file.name);

        ui->ViewDocumentButton->setEnabled(true);
    }
}

void RetakeVersionDialog::on_ViewDocumentButton_clicked()
{
    //TODO save retake version so we dont have to go to DB again?
    Db::RetakeVersion retakeVersion;
    _database->retakeVersions->getRetakeVersion(_versionId, retakeVersion);

    PdfDocumentPreview documentPreview(_annotationSource, _database, retakeVersion.file_id, _documentHandle, this);
    documentPreview.exec();
}

void RetakeVersionDialog::on_ClearDocumentButton_clicked()
{
    ui->DocumentPath->setText("");
    _documentHandle = "";
    ui->ViewDocumentButton->setEnabled(false);
}

void RetakeVersionDialog::on_DocumentBrowseButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
    dialog.setNameFilter("PDF Documents (*.pdf)");

    if (dialog.exec() == QDialog::Accepted) {
        _documentHandle = dialog.selectedFiles()[0];
        QFileInfo fileInfo(_documentHandle);
        ui->DocumentPath->setText(fileInfo.fileName());
        ui->ViewDocumentButton->setEnabled(true);
        _fileChanged = true;
    }
}

// Note that updateFile should be called before this method to ensure that the most up to date file_id is set for the retake version.
void RetakeVersionDialog::getRetakeVersion(Db::RetakeVersion& retakeVersion)
{
    retakeVersion.name = ui->NameBox->text();
    retakeVersion.description = ui->DescriptionBox->text();
    retakeVersion.file_id = _fileId;
}

QString RetakeVersionDialog::getFilePath() const
{
    return _documentHandle;
}

int RetakeVersionDialog::updateFile()
{
    if (_fileChanged) {
        if (_fileId > 0) {
            _database->files->release(_fileId);
        }

        _fileId = createFile();
    }

    return _fileId;
}

int RetakeVersionDialog::createFile()
{
    QString filePath = getFilePath();

    Db::File file;
    file.name = Util::fileNameFromPath(filePath);

    QFile qfile(filePath);
    qfile.open(QIODevice::ReadOnly);
    file.data = qfile.readAll();

    if (_database->files->createFile(file)) {
        return file.id;
    }
    else {
        return 0;
    }
}



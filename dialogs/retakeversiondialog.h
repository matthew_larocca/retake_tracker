#ifndef RETAKEVERSIONDIALOG_H
#define RETAKEVERSIONDIALOG_H

#include <QDialog>
#include <QString>
#include <QTemporaryFile>
#include <QSharedPointer>

#include "annotation/annotationsource.h"
#include "models/retakeversion.h"
#include "database/database.h"

namespace Ui {
class RetakeVersionDialog;
}

class RetakeVersionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RetakeVersionDialog(Db::Database* database, AnnotationSource* annotationSource, QWidget *parent = 0);
    virtual ~RetakeVersionDialog();

public:
    QString getFilePath() const;
    void getRetakeVersion(Db::RetakeVersion& retakeVersion);
    void setRetakeVersion(const Db::RetakeVersion& retakeVersion);

    int updateFile();

private slots:
    void on_ViewDocumentButton_clicked();

    void on_ClearDocumentButton_clicked();

    void on_DocumentBrowseButton_clicked();

private:
    int createFile();

private:
    Db::Database* _database;
    Ui::RetakeVersionDialog *ui;
    QString _documentHandle;
    bool _fileChanged = false;
    QTemporaryFile _tempFile;
    int _versionId;
    int _fileId = 0;
    AnnotationSource* _annotationSource;
};

#endif // RETAKEVERSIONDIALOG_H

#include "studentdialog.h"
#include "ui_studentdialog.h"

StudentDialog::StudentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StudentDialog)
{
    ui->setupUi(this);
}

StudentDialog::~StudentDialog()
{
    delete ui;
}

void StudentDialog::getStudent(Db::Student& student) const
{
    student.firstName = ui->firstNameText->text();
    student.lastName = ui->lastNameText->text();
    student.email = ui->emailText->text();
}

void StudentDialog::setStudent(const Db::Student& student) const
{
    ui->firstNameText->setText(student.firstName);
    ui->lastNameText->setText(student.lastName);
    ui->emailText->setText(student.email);
}

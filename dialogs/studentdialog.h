#ifndef STUDENTDIALOG_H
#define STUDENTDIALOG_H

#include "database/database.h"

#include <QDialog>

namespace Ui {
class StudentDialog;
}

class StudentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StudentDialog(QWidget *parent = 0);
    ~StudentDialog();

public:
    void getStudent(Db::Student& student) const;
    void setStudent(const Db::Student& student) const;

private:
    Ui::StudentDialog *ui;
};

#endif // STUDENTDIALOG_H

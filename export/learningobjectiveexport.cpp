#include "learningobjectiveexport.h"

#include "util/filepacker.h"

#include <QTemporaryDir>
#include <QHash>
#include <QString>
#include <QFile>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

bool LearningObjectiveExport::exportData(const QString& destFile)
{
    _filePack.clear();
    QTemporaryDir tempDir;
    if (!tempDir.isValid()) {
        return false;
    }

    QHash<int, Db::LearningObjective> learningObjectiveHash;
    QHash<int, Db::RetakeVersion> retakeVesrionHash;
    QHash<int, QString> fileMap;
    QHash<int, Db::Annotation> annotations;


    Db::LearningObjectiveVector learningObjectives;
    _database->learningObjectives->getLearningObjectives(_learningObjectives, learningObjectives);

    for (auto& learningObjective : learningObjectives) {
        learningObjectiveHash[learningObjective.id] = learningObjective;

        Db::RetakeVersionVector retakeVersions;
        _database->retakeVersions->getVersionsForLearningObjective(learningObjective.id, retakeVersions);

        for (auto& retakeVesion : retakeVersions) {
            retakeVesrionHash[retakeVesion.id] = retakeVesion;
        }

        QVector<int> fileIds;
        _database->retakeVersions->getVersionFileIdsForLearningObjective(learningObjective.id, fileIds);

        //write all files to temp dir
        for (const auto fileId : fileIds) {
            Db::File file;
            _database->files->getFile(fileId, file);

            const auto fileName = QString("%1_%2").arg(QString::number(file.id), file.name);
            const auto filePath = tempDir.filePath(fileName);
            _filePack.append(FilePacker::Item{fileName, filePath});

            fileMap[file.id] = file.name;

            QFile outfile(filePath);
            outfile.open(QIODevice::WriteOnly);
            outfile.write(file.data);
            outfile.close();

            Db::AnnotationVector annotationVec;
            _database->annotations->getAnnotationsForFile(file.id, annotationVec);

            for (const auto& annotation : annotationVec) {
                annotations[annotation.id] = annotation;
            }
        }
    }

    writeFileData(tempDir, fileMap);
    writeAnnotations(tempDir, annotations);
    writeRetakeVersions(tempDir, retakeVesrionHash);
    writeLearningObjectives(tempDir, learningObjectiveHash);

    FilePacker::pack(_filePack, destFile);

    return true;
}

void LearningObjectiveExport::writeLearningObjectives(const QTemporaryDir& dir, const QHash<int, Db::LearningObjective>& learningObjectiveHash)
{
    const QString fileName = "learning_objectives.json";
    const auto filePath = dir.filePath(fileName);
    _filePack.append(FilePacker::Item{fileName, filePath});

    QJsonArray learningObjectives;

    for (auto it = learningObjectiveHash.begin(); it != learningObjectiveHash.end(); ++it) {
        const auto& learningObjective = it.value();
        QJsonObject learningObjectiveObj;

        learningObjectiveObj["id"] = learningObjective.id;
        learningObjectiveObj["name"] = learningObjective.name;
        learningObjectiveObj["description"] = learningObjective.description;

        learningObjectives.append(learningObjectiveObj);
    }

    QJsonDocument document;
    document.setArray(learningObjectives);
    auto json = document.toJson();

    QFile outfile(filePath);
    outfile.open(QIODevice::WriteOnly);
    outfile.write(json);
    outfile.close();
}

void LearningObjectiveExport::writeAnnotations(const QTemporaryDir& dir, const QHash<int, Db::Annotation>& annotationMap)
{
    const QString fileName = "annotations.json";
    const auto filePath = dir.filePath(fileName);
    _filePack.append(FilePacker::Item{fileName, filePath});

    QJsonArray annotations;

    for (auto it = annotationMap.begin(); it != annotationMap.end(); ++it) {
        const auto& annotation = it.value();
        QJsonObject annotationObj;

        annotationObj["id"] = annotation.id;
        annotationObj["file_id"] = annotation.fileId;
        annotationObj["page"] = annotation.page;
        annotationObj["type"] = annotation.type;
        annotationObj["data"] = annotation.data;

        annotations.append(annotationObj);
    }

    QJsonDocument document;
    document.setArray(annotations);
    auto json = document.toJson();

    QFile outfile(filePath);
    outfile.open(QIODevice::WriteOnly);
    outfile.write(json);
    outfile.close();
}

void LearningObjectiveExport::writeFileData(const QTemporaryDir& dir, const QHash<int, QString>& fileMap)
{
    const QString fileName = "files.json";
    const auto filePath = dir.filePath(fileName);
    _filePack.append(FilePacker::Item{fileName, filePath});

    QJsonArray files;

    for (auto it = fileMap.begin(); it != fileMap.end(); ++it) {
        QJsonObject file;
        file["id"] = it.key();
        file["name"] = fileMap[it.key()];

        files.append(file);
    }

    QJsonDocument document;
    document.setArray(files);
    auto json = document.toJson();

    QFile outfile(filePath);
    outfile.open(QIODevice::WriteOnly);
    outfile.write(json);
    outfile.close();
}

void  LearningObjectiveExport::writeRetakeVersions(const QTemporaryDir& dir, const QHash<int, Db::RetakeVersion>& retakeVesrionMap)
{
    const QString fileName = "retake_versions.json";
    const auto filePath = dir.filePath(fileName);
    _filePack.append(FilePacker::Item{fileName, filePath});

    QJsonArray retakeVersions;

    for (auto it = retakeVesrionMap.begin(); it != retakeVesrionMap.end(); ++it) {
        const auto& retakeVersion = it.value();
        QJsonObject retakeVersionObj;

        retakeVersionObj["id"] = retakeVersion.id;
        retakeVersionObj["name"] = retakeVersion.name;
        retakeVersionObj["description"] = retakeVersion.description;
        retakeVersionObj["file_id"] = retakeVersion.file_id;
        retakeVersionObj["learning_objective_id"] = retakeVersion.learning_objective_id;

        retakeVersions.append(retakeVersionObj);
    }

    QJsonDocument document;
    document.setArray(retakeVersions);
    auto json = document.toJson();

    QFile outfile(filePath);
    outfile.open(QIODevice::WriteOnly);
    outfile.write(json);
    outfile.close();
}

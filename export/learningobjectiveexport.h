#ifndef LEARNINGOBJECTIVEEXPORT_H
#define LEARNINGOBJECTIVEEXPORT_H

#include "database/database.h"
#include "util/filepacker.h"

#include <QVector>
#include <QSet>
#include <QTemporaryDir>

class LearningObjectiveExport
{
public:
    LearningObjectiveExport(Db::Database* database) : _database(database) {}

    void setLearningObjectives(const QVector<int>& learningObjectives) { _learningObjectives = learningObjectives; }

    bool exportData(const QString& destFile);

private:
    void writeFileData(const QTemporaryDir& dir, const QHash<int, QString>& fileMap);
    void writeAnnotations(const QTemporaryDir& dir, const QHash<int, Db::Annotation>& annotationMap);
    void writeRetakeVersions(const QTemporaryDir& dir, const QHash<int, Db::RetakeVersion>& retakeVesrionMap);
    void writeLearningObjectives(const QTemporaryDir& dir, const QHash<int, Db::LearningObjective>& learningObjectiveHash);
private:
    Db::Database* _database;
    QVector<int> _learningObjectives;
    FilePacker::List _filePack;
};

#endif // LEARNINGOBJECTIVEEXPORT_H

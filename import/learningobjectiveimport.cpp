#include "learningobjectiveimport.h"

#include "util/filepacker.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

bool LearningObjectiveImport::importPack(int categoryId, const QString& path)
{
    QTemporaryDir tempDir;

    FilePacker::unpack(path, tempDir.path());

    importRetakeFiles(tempDir);
    importAnnotations(tempDir);
    importLearningObjectives(categoryId, tempDir);
    importRetakeVersions(tempDir);

    return true;
}

bool LearningObjectiveImport::importLearningObjectives(int categoryId, const QTemporaryDir& tempDir)
{
    const auto learningObjectiveFilePath = tempDir.filePath("learning_objectives.json");
    QFile file(learningObjectiveFilePath);
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }

    auto jsonDocument = QJsonDocument::fromJson(file.readAll());

    auto learningObjectives = jsonDocument.array();
    for (int i = 0; i < learningObjectives.count(); ++i) {
        const auto obj = learningObjectives.at(i).toObject();

        int importId = obj["id"].toInt();

        Db::LearningObjective learningObjective;
        learningObjective.categoryId = categoryId;
        learningObjective.description = obj["description"].toString();
        learningObjective.name = obj["name"].toString();

        _database->learningObjectives->createLearningObjective(learningObjective);

        _learningObjectiveMap[importId] = learningObjective.id;
    }

    file.close();
    return true;
}

bool LearningObjectiveImport::importRetakeVersions(const QTemporaryDir& tempDir)
{
    const auto retakeVersionPath = tempDir.filePath("retake_versions.json");
    QFile retakeVersionsFile(retakeVersionPath);
    if (!retakeVersionsFile.open(QIODevice::ReadOnly)) {
        return false;
    }

    auto jsonDocument = QJsonDocument::fromJson(retakeVersionsFile.readAll());
    auto retakeVersions = jsonDocument.array();

    for (int i = 0; i < retakeVersions.size(); i++){
        const auto& retakeVersion = retakeVersions.at(i).toObject();

        const auto importedLearningObjectiveId = retakeVersion["learning_objective_id"].toInt();
        const auto importedFileId = retakeVersion["file_id"].toInt();

        Db::RetakeVersion rtv;
        rtv.learning_objective_id = _learningObjectiveMap[importedLearningObjectiveId];
        rtv.file_id = importedFileId == 0 ? importedFileId : _fileMap[importedFileId];
        rtv.name = retakeVersion["name"].toString();
        rtv.description = retakeVersion["description"].toString();

        _database->retakeVersions->createRetakeVersion(rtv);
    }

    retakeVersionsFile.close();
    return true;
}

bool LearningObjectiveImport::importRetakeFiles(const QTemporaryDir& tempDir)
{
    const auto filesPath = tempDir.filePath("files.json");
    QFile file(filesPath);
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }

    auto jsonDocument = QJsonDocument::fromJson(file.readAll());
    auto files = jsonDocument.array();

    for (int i = 0; i < files.size(); ++i) {
        const auto fileObj = files.at(i).toObject();
        int importedFileId = fileObj["id"].toInt();

        Db::File f;
        f.name = fileObj["name"].toString();

        const auto importFileName = QString("%1_%2").arg(QString::number(importedFileId), f.name);
        const auto importFilePath = tempDir.filePath(importFileName);

        QFile importedFile(importFilePath);
        importedFile.open(QIODevice::ReadOnly);
        f.data = importedFile.readAll();

        _database->files->createFile(f);
        _fileMap[importedFileId] = f.id;

        importedFile.close();
    }

    file.close();
    return true;
}

bool LearningObjectiveImport::importAnnotations(const QTemporaryDir& tempDir)
{
    const auto annotationsPath = tempDir.filePath("annotations.json");
    QFile annotationsFile(annotationsPath);
    if (!annotationsFile.open(QIODevice::ReadOnly)) {
        return false;
    }

    auto jsonDocument = QJsonDocument::fromJson(annotationsFile.readAll());
    auto annotations = jsonDocument.array();

    for (int i = 0; i < annotations.size(); ++i) {
        const auto& annotation = annotations.at(i).toObject();

        int importedFileId = annotation["file_id"].toInt();
        Db::Annotation a;
        a.fileId = _fileMap[importedFileId];
        a.page = annotation["page"].toInt();
        a.type = annotation["type"].toInt();
        a.data = annotation["data"].toString();

        _database->annotations->createAnnotation(a);
    }

    annotationsFile.close();
    return true;
}

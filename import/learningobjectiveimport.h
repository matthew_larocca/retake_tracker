#ifndef LEARNINGOBJECTIVEIMPORT_H
#define LEARNINGOBJECTIVEIMPORT_H

#include "database/database.h"

#include <QHash>
#include <QTemporaryDir>


class LearningObjectiveImport
{
public:
    LearningObjectiveImport(Db::Database* database) : _database(database) {}

    bool importPack(int categoryId, const QString& path);

private:
    bool importLearningObjectives(int categoryId, const QTemporaryDir& tempDir);
    bool importRetakeVersions(const QTemporaryDir& tempDir);
    bool importRetakeFiles(const QTemporaryDir& tempDir);
    bool importAnnotations(const QTemporaryDir& tempDir);

private:
    Db::Database* _database;

    QHash<int, int> _learningObjectiveMap;
    QHash<int, int> _fileMap;
};

#endif // LEARNINGOBJECTIVEIMPORT_H

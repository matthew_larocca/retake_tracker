#include "retakeimporter.h"

#include <QFile>
#include <QByteArray>
#include <QSet>

constexpr int TsvFirstNameColumn = 2;
constexpr int TsvLastNameColumn = 3;
constexpr int TsvEmailColumn = 4;
constexpr int TsvClassColumn = 5;
constexpr int TsvLearningObjectiveColumn = 6;
constexpr int TsvRetakeDayColumn = 8;

bool RetakeImporter::importRetakesFromTSV(const QString& path)
{
    clear();

    QFile file(path);
    if (file.open(QIODevice::ReadOnly)) {
        loadClassData();

        //read header
        file.readLine();       

        // We will key students off of their email, so will build up a set of those while parsing the input file
        QSet<QString> studentEmails;


        while (!file.atEnd()) {
            auto line = file.readLine();
            auto fields = line.split('\t');

            Db::Retake retake;

            retake.studentName = fields[TsvFirstNameColumn] + " " + fields[TsvLastNameColumn];
            auto studentEmailAddress = fields[TsvEmailColumn].toLower();
            emails.push_back(studentEmailAddress);
            studentEmails.insert(studentEmailAddress);

            //grab the class id based on the string parsed from the file
            auto classIdResult = _classNameToId.find(fields[TsvClassColumn]);
            if (classIdResult != _classNameToId.end()) {
                retake.className = classIdResult.key();
                const auto classId = classIdResult.value();

                //next need to search the particular objectives for the class to get the id of the correct
                //class objective to schedule the retake.
                auto& classObjectives = getClassObjectiveData(classId);
                auto classObjectiveResult = classObjectives.find(fields[TsvLearningObjectiveColumn]);

                if (classObjectiveResult != classObjectives.end()) {
                    retake.LearningObjectiveName = classObjectiveResult.key();
                    retake.classObjectiveId = classObjectiveResult.value()->id;
                }
            }

            auto scheduledDate = QDateTime::fromString(fields[TsvRetakeDayColumn].trimmed(), "M/d/yyyy");
            if (scheduledDate.isValid()) {
                scheduledDate.setTime(QTime(12,0,0));
                retake.scheduledDate = scheduledDate;
            }

            retakes.push_back(retake);
        }

        //now we need to get the correct student id's based on the email's we have parsed in the file.
        Db::StudentVector students;
        _database->students->getStudentsForEmails(studentEmails, students);
        QHash<QString, int> studentEmailToId;
        for (auto& student : students) {
            studentEmailToId[student.email] = student.id;
        }

        for (int i = 0; i < emails.size(); i++){
            auto studentId = studentEmailToId.find(emails[i]);
            if (studentId != studentEmailToId.end()) {
                retakes[i].studentId = studentId.value();

                assignRetakeVersion(retakes[i]);
            }
        }

        return true;
    }
    else {
        return false;
    }
}


// need to be able to get the class id and learning objective Id from their names
// build up some structures to make that easier.
void RetakeImporter::loadClassData()
{
    Db::ClassQueryParams params;
    params.active = Db::QueryParams::Bool::True;
    Db::ClassVector classes;
    _database->classes->getClasses(classes, params);


    for (auto& c : classes) {
        _classNameToId[c.name] = c.id;
    }
}

const QHash<QString, QSharedPointer<Db::ClassObjective>>& RetakeImporter::getClassObjectiveData(int classId)
{
    auto result = _learningObjectivesForClasses.find(classId);
    if (result == _learningObjectivesForClasses.end()) {
        //if we do not have the objectives for this class betch them

        Db::ClassObjectiveVector classObjectives;
        _database->classObjectives->getObjectivesForClass(classId, classObjectives);

        //create new entry and add all the objectives to the list
        auto& objectiveMap = _learningObjectivesForClasses[classId];
        for (auto& classObjective : classObjectives) {
            auto objective_ptr = QSharedPointer<Db::ClassObjective>(new Db::ClassObjective{});
            * objective_ptr = classObjective;

            _classObjectives[classObjective.id] = objective_ptr;
            objectiveMap[classObjective.objectiveName] = objective_ptr;
        }

        return objectiveMap;
    }
    else {
        return result.value();
    }
}

// need to assign a retake version by picking  a retake that a student has taken the least amount of times
void RetakeImporter::assignRetakeVersion(Db::Retake& retake)
{
    if (!retake.readyForVersionAssignment()) {
        return;
    }

    auto learningObjectiveIdResult = _classObjectives.find(retake.classObjectiveId);
    if (learningObjectiveIdResult == _classObjectives.end()) {
        return;
    }

    Db::RetakeVersionCounts versionCounts;
    _database->retakeVersions->getRetakeVersionCountsForStudent(learningObjectiveIdResult.value()->learningObjectiveId, retake.classObjectiveId, retake.studentId, versionCounts);

    int minCount = INT_MAX;

    for (auto it = versionCounts.begin(); it != versionCounts.end(); ++it) {
        if (it.value().second < minCount) {
            retake.versionId = it.key();
            retake.retakeVersionName = it.value().first;
            minCount = it.value().second;
        }
    }
}

void RetakeImporter::createRetakes()
{
    for (auto& retake : retakes) {
        if (retake.isValid()) {
            retake.retakeCompleted = _importAsCompleted;

            _database->retakes->createRetake(retake);
        }
    }
}

void RetakeImporter::clear()
{
    _classNameToId.clear();
    _learningObjectivesForClasses.clear();
    _classObjectives.clear();
    retakes.clear();
    emails.clear();
}

#ifndef RETAKEIMPORTER_H
#define RETAKEIMPORTER_H

#include <database/database.h>

#include <QHash>
#include <QSharedPointer>
#include <QStringList>

class RetakeImporter
{
public:
    RetakeImporter(Db::Database* database) : _database(database) {}

public:
    bool importRetakesFromTSV(const QString& path);
    void createRetakes();

    void clear();

    const Db::RetakeVector& getRetakes() const { return retakes; }
    const QStringList& getRetakeEmails() const { return emails; }

    void setImportAsComleted(bool importAsCompleted) { _importAsCompleted = importAsCompleted; }

private:
    void loadClassData();
    const QHash<QString, QSharedPointer<Db::ClassObjective>>& getClassObjectiveData(int classId);
    void assignRetakeVersion(Db::Retake& retake);


private:
    Db::Database* _database;

    QHash<QString, int> _classNameToId;
    QHash<int, QHash<QString, QSharedPointer<Db::ClassObjective>>> _learningObjectivesForClasses;
    QHash<int, QSharedPointer<Db::ClassObjective>> _classObjectives;

    Db::RetakeVector retakes;
    QStringList emails;

    bool _importAsCompleted = false;
};

#endif // RETAKEIMPORTER_H

#include "studentimporter.h"

#include <QFile>
#include <QByteArray>
#include <QSet>
#include <QHash>

bool StudentImporter::parseClassRoster(const QString& path)
{
    _students.clear();
    _studentEmails.clear();

    QFile file(path);
    if (file.open(QIODevice::ReadOnly)) {
        while (!file.atEnd()) {
            auto line = file.readLine();
            auto fields = line.split('\t');

            Db::Student student;
            student.id = 0;
            student.active = true;
            student.firstName = fields[1];
            student.lastName = fields[0];
            student.email = fields[5].trimmed();

            _students.push_back(student);
            _studentEmails.push_back(student.email);
        }

        return true;
    }
    else {
        return false;
    }
}

bool StudentImporter::importStudentsToClass(int classId)
{
    createNewStudents();
    enrollStudents(classId);

    return true;
}

void StudentImporter::createNewStudents()
{
    // get all the existing students that were imported and save off their id's
    Db::StudentVector existingStudentVector;
    QHash<QString, int> existingStudentEmailToId;

    _database->students->getStudents(_studentEmails, existingStudentVector);

    for (auto& existingStudent : existingStudentVector) {
        existingStudentEmailToId[existingStudent.email] = existingStudent.id;
    }

    //for every student we are going to import into the class, create them if they do not exist
    auto notFound = existingStudentEmailToId.end();
    for (auto& student : _students) {
        auto result = existingStudentEmailToId.find(student.email);

        if (result == notFound) {
            _database->students->createStudent(student);
        }
        else {
            student.id = result.value();
        }
    }
}

void StudentImporter::enrollStudents(int classId)
{
    Db::EnrollmentVector enrollments;
    _database->enrollments->getEnrollmentsForClass(classId, enrollments);

    QSet<int> currentlyEnrolled;
    for (auto& enrollment : enrollments) {
        currentlyEnrolled.insert(enrollment.studentId);
    }

    for (auto& student : _students) {
        if (!currentlyEnrolled.contains(student.id)) {
            _database->classes->enrollStudentInClass(student.id, classId);
        }
    }
}

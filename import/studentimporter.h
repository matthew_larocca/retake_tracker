#ifndef STUDENTIMPORTER_H
#define STUDENTIMPORTER_H

#include <QString>
#include <QStringList>


#include "database/database.h"

class StudentImporter
{
public:
    StudentImporter(Db::Database* database) : _database(database) {}

public:
    bool parseClassRoster(const QString& path);
    bool importStudentsToClass(int classId);

    const Db::StudentVector& students() const { return _students; }

private:
    void createNewStudents();
    void enrollStudents(int classId);

private:
    Db::Database* _database;
    Db::StudentVector _students;
    QStringList _studentEmails;
};

#endif // STUDENTIMPORTER_H

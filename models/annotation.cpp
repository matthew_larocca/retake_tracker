#include "annotation.h"

#include "queryutil.h"

#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>

namespace Db {

QString Annotation::TypeString(int type)
{
    switch (type) {
    case 1: return "Name";
    case 2: return "Date";
    case 3: return "Class";
    default: return "Unknown";
    };
}

QString Annotation::toDataString(const QRectF& rect)
{
    return QString("%1 %2 %3 %4").arg(
                QString::number(rect.x()), QString::number(rect.y()),
                QString::number(rect.width()), QString::number(rect.height()));
}

QRectF Annotation::fromDataString(const QString& str)
{
    auto strValues = str.split(' ');
    return QRectF(strValues[0].toDouble(), strValues[1].toDouble(), strValues[2].toDouble(), strValues[3].toDouble());
}

QRectF Annotation::calculateNormalizedRect(const QRectF& imageRect, const QSize& imageSize)
{
    return QRectF(
                imageRect.x() / imageSize.width(),
                imageRect.y() / imageSize.height(),
                imageRect.width() / imageSize.width(),
                imageRect.height() / imageSize.height()
                );
}

QRectF Annotation::calculateImageRect(const QRectF& normalizedRect, const QSize& imageSize)
{
    return QRectF (
                normalizedRect.x() * imageSize.width(),
                normalizedRect.y() * imageSize.height(),
                normalizedRect.width() * imageSize.width(),
                normalizedRect.height() * imageSize.height()
                );
}

void bindAnnotationValues(QSqlQuery& query, const Annotation& annotation);
void queryResultToAnnotation(QSqlQuery& query, Annotation& annotation);
void annotationQueryToVector(QSqlQuery& query, AnnotationVector& annotations);

bool Annotations::createAnnotation(Annotation& annotation)
{
    QSqlQuery query;
    query.prepare("INSERT INTO annotations (file_id, page, type, data) VALUES (:file_id, :page, :type, :data);");
    bindAnnotationValues(query, annotation);

    if (query.exec()) {
        annotation.id = query.lastInsertId().toInt();
        return true;
    }
    else {
         _lastError = query.lastError().databaseText();
         return false;
    }
}

bool Annotations::getAnnotation(int id, Annotation& annotation)
{
    QSqlQuery query;
    query.prepare("SELECT id, file_id, page, type, data FROM annotations where id = :id;");
    query.bindValue(":id", id);

    if (query.exec() && query.next()) {
        queryResultToAnnotation(query, annotation);
        return true;
    }
    else {
         _lastError = query.lastError().databaseText();
         return false;
    }
}

bool Annotations::getAnnotationsForFile(int fileId, AnnotationVector& annotations)
{
    QSqlQuery query;
    query.prepare("SELECT id, file_id, page, type, data FROM annotations where file_id = :file_id;");
    query.bindValue(":file_id", fileId);

    if (query.exec()) {
        annotationQueryToVector(query, annotations);
        return true;
    }
    else {
         _lastError = query.lastError().databaseText();
         return false;
    }
}

bool Annotations::deleteAnnotation(int annotationId)
{
    QSqlQuery query;
    query.prepare("DELETE FROM annotations WHERE id = :id");
    query.bindValue(":id", annotationId);

    if (query.exec()) {
        return true;
    }
    else {
         _lastError = query.lastError().databaseText();
         return false;
    }
}

bool Annotations::deleteAnnotationsForFiles(const QVector<int>& fileIds)
{
    if (fileIds.size() == 0) {
        return true;
    }

    QSqlQuery query;
    query.prepare("DELETE from annotations where file_id in " + QueryUtil::buildValueList(fileIds.count()));
    for (auto fileId : fileIds) {
        query.addBindValue(fileId);
    }

    return query.exec();
}

bool Annotations::updateAnnotation(Annotation& annotation)
{
    QSqlQuery query;
    query.prepare("UPDATE annotations set file_id = :file_id, page = :page, type = :type, data = :data WHERE id = :id;");
    bindAnnotationValues(query, annotation);
    query.bindValue(":id", annotation.id);

    if (query.exec()) {
        return true;
    }
    else {
         _lastError = query.lastError().databaseText();
         return false;
    }
}

void bindAnnotationValues(QSqlQuery& query, const Annotation& annotation)
{
    query.bindValue(":file_id", annotation.fileId);
    query.bindValue(":page", annotation.page);
    query.bindValue(":type", annotation.type);
    query.bindValue(":data", annotation.data);
}

void queryResultToAnnotation(QSqlQuery& query, Annotation& annotation)
{
    annotation.id = query.value("id").toInt();
    annotation.fileId = query.value("file_id").toInt();
    annotation.page = query.value("page").toInt();
    annotation.type = query.value("type").toInt();
    annotation.data = query.value("data").toString();
}

void annotationQueryToVector(QSqlQuery& query, AnnotationVector& annotations)
{
    Annotation annotation;
    while (query.next()) {
        queryResultToAnnotation(query, annotation);
        annotations.push_back(annotation);
    }
}

}

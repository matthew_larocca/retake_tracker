#ifndef ANNOTATION_H
#define ANNOTATION_H

#include <QString>
#include <QVector>
#include <QRectF>
#include <QSize>

#include <QSqlDatabase>

namespace Db {
    struct Annotation {
        int id;
        int fileId;
        int page;
        int type;
        QString data;

        static QString toDataString(const QRectF& rect);
        static QRectF fromDataString(const QString& str);
        static QRectF calculateNormalizedRect(const QRectF& imageRect, const QSize& imageSize);
        static QRectF calculateImageRect(const QRectF& normalizedRect, const QSize& imageSize);

        static QString TypeString(int type);

        enum Type {
            Unknown = 0,
            Name = 1,
            Date = 2,
            Class = 3
        };
    };

    using AnnotationVector = QVector<Annotation>;

    class Annotations {
    public:
        Annotations(QSqlDatabase* database) : _database(database) {}

    public:
        bool getAnnotation(int id, Annotation& annotation);
        bool createAnnotation(Annotation& annotation);
        bool updateAnnotation(Annotation& annotation);
        bool getAnnotationsForFile(int fileId, AnnotationVector& annotations);
        bool deleteAnnotation(int annotationId);
        bool deleteAnnotationsForFiles(const QVector<int>& fileIds);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };
}


#endif // ANNOTATION_H

#include "class.h"

#include "queryutil.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QVariant>

namespace Db {
    void bindClassValues(QSqlQuery& query, const Class& c);
    void queryResultToClass(QSqlQuery& query, Class& c);
    void classQueryToVector(QSqlQuery& query, ClassVector& classes);

    Classes::Classes(QSqlDatabase* database)
        :_database(database)
    {}

    bool Classes::createClass(Class& c)
    {
        QSqlQuery query;
        query.prepare("INSERT INTO classes (name, active) VALUES (:name, :active);");
        bindClassValues(query, c);

        bool result = query.exec();

        if (result)
        {
            c.id = query.lastInsertId().toInt();
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
        }

        return false;
    }

    bool Classes::updateClass(Class& c)
    {
        QSqlQuery query;
        query.prepare("update classes set name = :name , active = :active where id = :id");
        bindClassValues(query, c);
        query.bindValue(":id", c.id);

        bool result = query.exec();

        if (!result)
        {
            _lastError = query.lastError().databaseText();
        }

        return false;
    }

    bool Classes::execQueryToVector(QSqlQuery& query, ClassVector& classes)
    {
        if (query.exec()){
            classQueryToVector(query, classes);
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool Classes::getClasses(ClassVector& classVector, const ClassQueryParams& params)
    {
        QString sql = "select classes.id, classes.name, classes.active from classes ";
        sql = params.filterSelect(sql, 0);

        QSqlQuery query;
        query.prepare(sql);

        return execQueryToVector(query, classVector);
    }

    bool Classes::deleteClass(int classId)
    {
        QSqlQuery query;
        query.prepare("delete from classes where id = :id;");
        query.bindValue(":id", classId);

        bool result = query.exec();

        if (!result)
        {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool Classes::getClass(int id, Class& c)
    {
        QSqlQuery query;
        query.prepare("select classes.id, classes.name, classes.active from classes where id = :id;");
        query.bindValue(":id", id);

        if (query.exec()){
            if (query.next())
            {
                queryResultToClass(query, c);
                return true;
            }
        }
        else
        {
            _lastError = query.lastError().databaseText();
        }

        return false;
    }

     bool Classes::getClassesForStudent(int studentId, ClassVector& classes, const ClassQueryParams& params)
     {
         QString sql = "SELECT classes.id, classes.name, classes.active from enrollments JOIN classes on classes.id = enrollments.class_id WHERE enrollments.student_id = :studentId ";
         sql = params.filterSelect(sql, 1);

         QSqlQuery query;
         query.prepare(sql);
         query.bindValue(":studentId", studentId);

         return execQueryToVector(query, classes);
     }

    bool Classes::enrollStudentInClass(int studentId, int classId)
    {
        QSqlQuery query;
        query.prepare("insert into enrollments (student_id, class_id)  VALUES (:studentId, :classId);");
        query.bindValue(":studentId", studentId);
        query.bindValue(":classId", classId);

        if (!query.exec())
        {
             _lastError = query.lastError().databaseText();
             return false;
        }
        else
        {
            return true;
        }
    }

    bool Classes::addLearningObjective(int learningObjectiveId, int classId)
    {
        QSqlQuery query;
        query.prepare("INSERT into class_objectives (learning_objective_id, class_id) VALUES (:learningObjectiveId, :classId);");
        query.bindValue(":learningObjectiveId", learningObjectiveId);
        query.bindValue(":classId", classId);

        return query.exec();
    }

    void bindClassValues(QSqlQuery& query, const Class& c)
    {
        query.bindValue(":name", c.name);
        query.bindValue(":active", c.active);
    }

    void queryResultToClass(QSqlQuery& query, Class& c)
    {
        c.id = query.value("id").toInt();
        c.name = query.value("name").toString();
        c.active = query.value("active").toBool();
    }

    void classQueryToVector(QSqlQuery& query, ClassVector& classes)
    {
        Class c;
        while (query.next()) {
            queryResultToClass(query, c);

            classes.append(c);
        }
    }

    ClassQueryParams::ClassQueryParams()
        :QueryParams(),
          orderBy(OrderBy::Name),
          active(Bool::True)
    {}


    QString ClassQueryParams::filterSelect(const QString& selectStatement, int clause) const
    {
        QString sql = selectStatement;
        if (filterBool(clause, "classes.active", active, sql)) clause += 1;

        filterOrder(sql);
        filterLimit(sql);

        return sql;
    }

    void ClassQueryParams::filterOrder(QString& sql) const
    {
        switch(orderBy){
        case OrderBy::Name:
            filterOrderBy("classes.name ", sql);
            break;

        case OrderBy::CreatedTime:
            filterOrderBy("classes.id ", sql);
            break;
        }
    }
}

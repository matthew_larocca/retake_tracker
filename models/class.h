#ifndef CLASS_H
#define CLASS_H

#include "queryparams.h"

#include <QString>
#include <QSqlDatabase>
#include <QVector>

namespace Db {
    struct Class
    {
        int id;
        QString name;
        bool active = true;
    };

    using ClassVector = QVector<Class>;

    class ClassQueryParams : public QueryParams{
    public:
        enum class OrderBy{
            Name,
            CreatedTime
        };

    public:
        ClassQueryParams();

        QString filterSelect(const QString& selectStatement, int clause) const;

    private:
        void filterOrder(QString& sql) const;

    public:
        OrderBy orderBy;
        Bool active;
    };

    class Classes{
    public:
        Classes(QSqlDatabase* database);

    public:
        bool createClass(Class& c);
        bool updateClass(Class& c);
        bool deleteClass(int classId);
        bool getClasses(ClassVector& classVector, const ClassQueryParams& params = ClassQueryParams());
        bool getClass(int id, Class& c);
        bool getClassesForStudent(int studentId, ClassVector& classes, const ClassQueryParams& params = ClassQueryParams());

        bool enrollStudentInClass(int studentId, int classId);
        bool addLearningObjective(int learningObjectiveId, int classId);

    private:
        bool execQueryToVector(QSqlQuery& query, ClassVector& classes);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };
}

#endif // CLASS_H

#include "classobjective.h"

#include <QSqlQuery>
#include <QVariant>

namespace Db {
    void queryResultToClassObjective(QSqlQuery& query, ClassObjective& classObjective);
    void classObjectiveQueryToVector(QSqlQuery& query, ClassObjectiveVector& classObjectives);

const QString selectSQL =
        "select class_objectives.id, class_objectives.learning_objective_id, class_objectives.class_id, learning_objectives.name "
        "from class_objectives "
        "JOIN learning_objectives on class_objectives.learning_objective_id = learning_objectives.id ";

bool ClassObjectives::getObjectivesForClass(int classId, ClassObjectiveVector& classObjectives)
{
    QSqlQuery query;
    query.prepare(selectSQL + "where class_objectives.class_id = :classId;");
    query.bindValue(":classId", classId);

    if (query.exec()) {
        classObjectiveQueryToVector(query, classObjectives);
        return true;
    }
    else {
        return false;
    }
}

bool ClassObjectives::getClassObjective(int classId, int learningObjectiveId, ClassObjective& classObjective)
{
    QSqlQuery query;
    query.prepare(selectSQL + "where class_objectives.class_id = :classId AND class_objectives.learning_objective_id = :learningObjectiveId;");
    query.bindValue(":classId", classId);
    query.bindValue(":learningObjectiveId", learningObjectiveId);

    if (query.exec() && query.next()) {
        queryResultToClassObjective(query, classObjective);
        return true;
    }
    else {
        return false;
    }
}

bool ClassObjectives::deleteClassObjective(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM class_objectives WHERE id = :id;");
    query.bindValue(":id", id);

    return query.exec();
}

bool ClassObjectives::deleteClassObjectivesForLearningObjective(int learningObjectiveId)
{
    QSqlQuery query;
    query.prepare("DELETE from class_objectives where learning_objective_id = :learningObjectiveId;");
    query.bindValue(":learningObjectiveId", learningObjectiveId);

    return query.exec();
}

void queryResultToClassObjective(QSqlQuery& query, ClassObjective& classObjective)
{
    classObjective.id = query.value("id").toInt();
    classObjective.learningObjectiveId = query.value("learning_objective_id").toInt();
    classObjective.classId = query.value("class_id").toInt();
    classObjective.objectiveName = query.value("name").toString();
}

void classObjectiveQueryToVector(QSqlQuery& query, ClassObjectiveVector& classObjectives)
{
    ClassObjective classObjective;
    while (query.next()) {
        queryResultToClassObjective(query, classObjective);
        classObjectives.push_back(classObjective);
    }
}

}

#ifndef CLASSOBJECTIVE_H
#define CLASSOBJECTIVE_H

#include <QString>
#include <QVector>

#include <QSqlDatabase>

namespace Db {

struct ClassObjective
{
    int id = -1;
    int classId = -1;
    int learningObjectiveId = -1;

    QString objectiveName;
};

using ClassObjectiveVector = QVector<ClassObjective>;

class ClassObjectives
{
public:
    ClassObjectives(QSqlDatabase* database) : _database(database) {}

public:
    bool getObjectivesForClass(int classId, ClassObjectiveVector& classObjectives);
    bool getClassObjective(int classId, int learningObjectiveId, ClassObjective& classObjective);
    bool deleteClassObjective(int id);
    bool deleteClassObjectivesForLearningObjective(int learningObjectiveId);

private:
    QSqlDatabase* _database;
};
}

#endif // CLASSOBJECTIVE_H

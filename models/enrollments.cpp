#include "enrollments.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QVariant>

namespace Db {
    void enrollmentQueryToVector(QSqlQuery& query, EnrollmentVector& enrollments);
    void queryResultToEnrollment(QSqlQuery& query, Enrollment& enrollment);

    Enrollments::Enrollments(QSqlDatabase* database)
        : _database(database)
    {}

    bool Enrollments::deleteEnrollmentsForClass(int classId)
    {
        return deleteEntrollmentsByid("class_id", classId);
    }

    bool Enrollments::deleteEntrollmentsByid(const QString& columnName, int id)
    {
        QSqlQuery query;
        query.prepare("delete from enrollments where " + columnName + " = :id;");
        query.bindValue(":id", id);

        bool result = query.exec();

        if (!result)
        {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool Enrollments::getEnrollmentsForClass(int classId, EnrollmentVector& enrollments)
    {
        return getEnrollmentsById("class_id", classId, enrollments);
    }

    bool Enrollments::execQueryToVector(QSqlQuery& query, EnrollmentVector& enrollments)
    {
        if (query.exec())
        {
            enrollmentQueryToVector(query, enrollments);
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool Enrollments::getEnrollmentsById(const QString& columnName, int id, EnrollmentVector& enrollments)
    {
        QSqlQuery query;
        query.prepare("select id, student_id, class_id from enrollments where " + columnName + " = :id;");
        query.bindValue(":id", id);

        return execQueryToVector(query, enrollments);
    }

    void queryResultToEnrollment(QSqlQuery& query, Enrollment& enrollment)
    {
        enrollment.id = query.value("id").toInt();
        enrollment.classId = query.value("class_id").toInt();
        enrollment.studentId = query.value("student_id").toInt();
    }

    void enrollmentQueryToVector(QSqlQuery& query, EnrollmentVector& enrollments)
    {
        Enrollment enrollment;
        while(query.next())
        {
            queryResultToEnrollment(query, enrollment);
            enrollments.append(enrollment);
        }
    }
}

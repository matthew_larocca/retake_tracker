#ifndef ENROLLMENTS_H
#define ENROLLMENTS_H

#include <QString>
#include <QSqlDatabase>
#include <QVector>

namespace Db {
    struct Enrollment
    {
        int id;
        int studentId;
        int classId;
    };

    using EnrollmentVector = QVector<Enrollment>;

    class Enrollments{
     public:
        Enrollments(QSqlDatabase* database);

    public:
        bool getEnrollmentsForClass(int classId, EnrollmentVector& enrollments);
        bool deleteEnrollmentsForClass(int classId);

    private:
        bool getEnrollmentsById(const QString& columnName, int id, EnrollmentVector& enrollments);
        bool deleteEntrollmentsByid(const QString& columnName, int id);

        bool execQueryToVector(QSqlQuery& query, EnrollmentVector& enrollments);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };
}


#endif // ENROLLMENTS_H

#include "file.h"

#include "queryutil.h"

#include <QSqlError>
#include <QVariant>

namespace Db {
    void bindFileValues(QSqlQuery& query, const File& file);
    void queryResultToFile(QSqlQuery& query, File& file, bool getData = true);

    Files::Files(QSqlDatabase* database)
        :_database(database)
    {}

    bool Files::createFile(File& file)
    {
        QSqlQuery query;
        query.prepare("INSERT INTO files (name, data, ref_count) VALUES (:name, :data, 1);");
        bindFileValues(query, file);

        bool result = execBasicQuery(query);

        if (result) {
            file.id = query.lastInsertId().toInt();
            file.refCount = 1;
        }

        return result;
    }

    bool Files::updateFile(File& file)
    {
        QSqlQuery query;
        query.prepare("UPDATE files set name = :name, description = :description, ref_count = :ref_count where id = :id;");
        bindFileValues(query, file);
        query.bindValue(":id", file.id);

        return execBasicQuery(query);
    }

    bool Files::deleteFile(int fileId)
    {
        QSqlQuery query;
        query.prepare("DELETE FROM files where id = :id;");
        query.bindValue(":id", fileId);

        return execBasicQuery(query);
    }

    bool Files::deleteFiles(const QVector<int>& ids)
    {
        if (ids.size() == 0) {
            return true;
        }

        QSqlQuery query;
        query.prepare("DELETE FROM files where id in " + QueryUtil::buildValueList(ids.size()));
        for (auto id : ids) {
            query.addBindValue(id);
        }

        return query.exec();
    }

    bool Files::getFile(int fileId, File& file)
    {
        QSqlQuery query;
        query.prepare("SELECT id, name, ref_count, data from files where id = :id;");
        query.bindValue(":id", fileId);

        if (query.exec()){
            if (query.next()) {
                queryResultToFile(query, file);
                return true;
            }
        }
        else {
            _lastError = query.lastError().databaseText();
        }

        return false;
    }

    bool Files::execBasicQuery(QSqlQuery& query)
    {
        bool result = query.exec();

        if (!result) {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool Files::release(int fileId)
    {
        File file;
        auto result = getFile(fileId, file);

        if (result) {
            if (file.refCount <= 1) {
                deleteFile(file.id);
            }
            else {
                file.refCount -= 1;
                updateFile(file);
            }

            return true;
        }
        else {
            return false;
        }
    }

    void bindFileValues(QSqlQuery& query, const File& file)
    {
        query.bindValue(":name", file.name);
        query.bindValue(":data", file.data);
        query.bindValue(":ref_count", file.refCount);
    }

    void queryResultToFile(QSqlQuery& query, File& file, bool getData)
    {
        file.id = query.value("id").toInt();
        file.name = query.value("name").toString();
        file.refCount = query.value("ref_count").toInt();

        if (getData) {
            file.data = query.value("data").toByteArray();
        }
    }
}

#ifndef FILE_H
#define FILE_H

#include <QString>
#include <QByteArray>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVector>

namespace Db {

    struct File
    {
        int id;
        int refCount;
        QString name;
        QByteArray data;
    };

    class Files
    {
    public:
        Files(QSqlDatabase* database);

    public:
        bool createFile(File& file);
        bool deleteFile(int fileId);
        bool deleteFiles(const QVector<int>& ids);
        bool updateFile(File& file);
        bool getFile(int fileId, File& file);

        bool release(int fileId);

    private:
        bool execBasicQuery(QSqlQuery& query);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };

}

#endif // FILE_H

#include "learningobjective.h"

#include "queryutil.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QVariant>

namespace Db {
    void learningObjectiveQueryToVector(QSqlQuery& query, LearningObjectiveVector& learningObjectives);
    void bindLearningObjectiveValues(QSqlQuery& query, const LearningObjective& learningObjective);

    LearningObjectives::LearningObjectives(QSqlDatabase* database)
        :_database(database)
    {}

    bool LearningObjectives::createLearningObjective(LearningObjective& learningObjective)
    {
        QSqlQuery query;
        query.prepare("insert into learning_objectives (name, description, category_id) VALUES (:name, :description, :categoryId)");
        bindLearningObjectiveValues(query, learningObjective);

        if (query.exec())
        {
            learningObjective.id = query.lastInsertId().toInt();

            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool LearningObjectives::updateLearningObjective(const LearningObjective& learningObjective)
    {
        QSqlQuery query;
        query.prepare("update learning_objectives set name = :name, description = :description where id = :id;");
        bindLearningObjectiveValues(query, learningObjective);
        query.bindValue(":id", learningObjective.id);

        bool result = query.exec();

        if (!result)
        {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool LearningObjectives::getLearningObjective(int id, LearningObjective& learningObjective)
    {
        LearningObjectiveVector learningObjectives;
        bool result =  getLearningObjectivesById("id", id, learningObjectives);

        if (result && learningObjectives.size() > 0)
        {
            learningObjective = learningObjectives[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    bool LearningObjectives::getLearningObjectives(const QVector<int>& learningObjectiveIds, LearningObjectiveVector& learningObjectives)
    {
        if (learningObjectiveIds.size() == 0)
        {
            return true;
        }

        QString sql = "select id, name, description, category_id from learning_objectives where id in ";
        sql += Db::QueryUtil::buildInClause(learningObjectiveIds);

        QSqlQuery query;
        query.prepare(sql);

        return execQueryToVector(query, learningObjectives);
    }

    bool LearningObjectives::getLearningObjectivesForClass(int classId, LearningObjectiveVector& learningObjectives)
    {
        QSqlQuery query;
        query.prepare("select learning_objectives.id, learning_objectives.name, learning_objectives.description, learning_objectives.category_id from class_objectives JOIN learning_objectives on learning_objectives.id = class_objectives.learning_objective_id where class_objectives.class_id = :classId;");
        query.bindValue(":classId", classId);

        return execQueryToVector(query, learningObjectives);
    }

    bool LearningObjectives::getLearningObjectivesById(const QString& columnName, int id, LearningObjectiveVector& learningObjectives)
    {
        QSqlQuery query;
        query.prepare("select id, name, description, category_id from learning_objectives where " + columnName + " = :id;");
        query.bindValue(":id", id);

        return execQueryToVector(query, learningObjectives);
    }

    bool LearningObjectives::getLearningObjectivesInCategory(int categoryId, LearningObjectiveVector& learningObjectives)
    {
        QSqlQuery query;
        query.prepare("select id, name, description, category_id from learning_objectives where category_id = :categoryId;");
        query.bindValue(":categoryId", categoryId);

        return execQueryToVector(query, learningObjectives);
    }

    bool LearningObjectives::execQueryToVector(QSqlQuery& query, LearningObjectiveVector& learningObjectives)
    {
        if (query.exec())
        {
            learningObjectiveQueryToVector(query, learningObjectives);

            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool LearningObjectives::deleteLearningObjectivesForClass(int classId)
    {
        return deleteLearningObjectivesById("class_id", classId);
    }

    bool LearningObjectives::deleteLearningObjectivesById(const QString& columnName, int id)
    {
        QSqlQuery query;
        query.prepare("delete from learning_objectives where " + columnName + " = :id;");
        query.bindValue(":id", id);

        bool result = query.exec();

        if (!result)
        {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool LearningObjectives::deleteLearningObjective(int learningObjectiveId)
    {
        QSqlQuery query;
        query.prepare("delete from learning_objectives where id = :id");
        query.bindValue(":id", learningObjectiveId);

        if (query.exec())
        {
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    void learningObjectiveQueryToVector(QSqlQuery& query, LearningObjectiveVector& learningObjectives){
        LearningObjective learningObjective;
        while (query.next())
        {
            learningObjective.id = query.value("id").toInt();
            learningObjective.name = query.value("name").toString();
            learningObjective.description = query.value("description").toString();
            learningObjective.categoryId = query.value("category_id").toInt();

            learningObjectives.append(learningObjective);
        }
    }

    void bindLearningObjectiveValues(QSqlQuery& query, const LearningObjective& learningObjective)
    {
        query.bindValue(":name", learningObjective.name);
        query.bindValue(":description", learningObjective.description);
        query.bindValue(":categoryId", learningObjective.categoryId);
    }

}

#ifndef LEARNINGOBJECTIVE_H
#define LEARNINGOBJECTIVE_H

#include <QString>
#include <QSqlDatabase>
#include <QVector>


namespace Db{
    struct LearningObjective
    {
        int id;
        int categoryId;
        QString name;
        QString description;
    };

    using LearningObjectiveVector = QVector<LearningObjective>;

    class LearningObjectives{
    public:
        LearningObjectives(QSqlDatabase* database);

    public:
        bool getLearningObjective(int id, LearningObjective& learningObjective);
        bool getLearningObjectives(const QVector<int>& learningObjectiveIds, LearningObjectiveVector& learningObjectives);
        bool updateLearningObjective(const LearningObjective& learningObjective);
        bool createLearningObjective(LearningObjective& learningObjective);

        //TODO: fix me
        bool getLearningObjectivesForClass(int classId, LearningObjectiveVector& learningObjectives);
        bool deleteLearningObjective(int learningObjectiveId);
        bool deleteLearningObjectivesForClass(int classId);

        bool getLearningObjectivesInCategory(int categoryId, LearningObjectiveVector& learningObjectives);

    private:
        bool getLearningObjectivesById(const QString& columnName, int id, LearningObjectiveVector& learningObjectives);
        bool deleteLearningObjectivesById(const QString& columnName, int id);
        bool execQueryToVector(QSqlQuery& query, LearningObjectiveVector& learningObjectives);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };
}

#endif // LEARNINGOBJECTIVE_H

#include "learningobjectivecategory.h"

#include <QSqlQuery>
#include <QVariant>

namespace Db {

bool LearningObjectiveCategories::createCateogry(LearningObjectiveCategory& category)
{
    QSqlQuery query;
    query.prepare("INSERT into learning_objective_categories (name) VALUES (:name);");
    query.bindValue(":name", category.name);

    bool result = query.exec();

    if (result) {
        category.id = query.lastInsertId().toInt();
    }

    return result;
}

bool LearningObjectiveCategories::getCategories(LearningObjectiveCategoryVector& categories)
{
    QSqlQuery query;
    query.prepare("select id, name from learning_objective_categories;");

    bool result = query.exec();

    if (result) {
        LearningObjectiveCategory category;

        while (query.next()) {
            category.id = query.value("id").toInt();
            category.name = query.value("name").toString();

            categories.push_back(category);
        }
    }

    return result;
}

bool LearningObjectiveCategories::updateCategory(LearningObjectiveCategory& category)
{
    QSqlQuery query;
    query.prepare("UPDATE learning_objective_categories SET name = :name WHERE id = :id;");
    query.bindValue(":name", category.name);
    query.bindValue(":id", category.id);

    return query.exec();
}

}

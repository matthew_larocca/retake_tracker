#ifndef LEARNINGOBJECTIVECATEGORY_H
#define LEARNINGOBJECTIVECATEGORY_H

#include <QSqlDatabase>
#include <QString>
#include <QVector>

namespace Db {

struct LearningObjectiveCategory
{
    int id;
    QString name;
};

using LearningObjectiveCategoryVector = QVector<LearningObjectiveCategory>;

class LearningObjectiveCategories {
public:
    LearningObjectiveCategories(QSqlDatabase* database) : _database(database) {}

    bool createCateogry(LearningObjectiveCategory& category);
    bool updateCategory(LearningObjectiveCategory& category);
    bool getCategories(LearningObjectiveCategoryVector& categories);

private:
    QSqlDatabase* _database;
};

}



#endif // LEARNINGOBJECTIVECATEGORY_H

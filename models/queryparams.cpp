#include "queryparams.h"

#include "queryutil.h"

namespace Db{
    void QueryParams::reset()
    {
        orderDirection = OrderDirection::Descending;
        limit = 500;
    }

    void QueryParams::apply(QString& sql) const
    {
        sql += " LIMIT " + QString::number(limit);
    }

    void QueryParams::filterOrderBy(const QString& columnName, QString& sql) const
    {
        sql += " ORDER BY " + columnName;

        switch (orderDirection) {
        case OrderDirection::Ascending:
            sql += " ASC";
            break;
        case OrderDirection::Descending:
            sql += " DESC";
            break;
        }
    }

    void QueryParams::filterLimit(QString& sql) const
    {
        sql += " LIMIT " + QString::number(limit);
    }

    void QueryParams::applyOrderDirection(QString& sql) const{
        switch (orderDirection) {
        case OrderDirection::Ascending:
            sql += " ASC";
            break;
        case OrderDirection::Descending:
            sql += " DESC";
            break;
        }
    }

    void QueryParams::checkClause(int clause, QString& sql)
    {
        if (clause == 0)
            sql += " WHERE ";
        else
            sql += " AND ";
    }

    bool QueryParams::filterColumnById(int clause, const QString& columnName, const QVector<int>& idList, QString& sql)
    {
        size_t filterSize = idList.size();
        if (filterSize > 0 && clause == 0)
        {
            checkClause(clause, sql);
        }

        if (filterSize == 1)
        {
            sql += columnName + " = " + QString::number(idList[0]);
        }
        else if (filterSize > 1)
        {
            sql += columnName + " in " + QueryUtil::buildInClause(idList);
        }

        return filterSize > 0;
    }

    bool QueryParams::filterColumnByDateRange(int clause, const QString& columnName, const QDateTime& begin, const QDateTime& end, QString& sql)
    {
        bool beginValid = begin.isValid();
        bool endValid = end.isValid();

        if (beginValid || endValid)
        {
            checkClause(clause, sql);
        }

        if (beginValid && endValid)
        {
            sql += columnName + " between :begin AND :end";
        }
        else if (beginValid)
        {
            sql += columnName + " > :begin";
        }
        else if (endValid)
        {
            sql += columnName + " < :end";
        }

        return beginValid || endValid;
    }

    bool QueryParams::filterStatus(int clause, const QString& columnName, Status status, QString& sql) {
        switch (status)
        {
        case Status::Null:
            checkClause(clause, sql);
            sql += columnName + " is NULL ";
            return true;

        case Status::NotNull:
            checkClause(clause, sql);
            sql += columnName + " is not NULL ";
            return true;
        };

        return false;
    }

    bool QueryParams::filterBool(int clause, const QString& columnName, Bool b, QString& sql){
        switch (b)
        {
        case Bool::True:
            checkClause(clause, sql);
            sql += columnName + " is 1 ";
            return true;

        case Bool::False:
            checkClause(clause, sql);
            sql += columnName + " is 0 ";
            return true;
        };

        return false;
    }

    bool QueryParams::filterString(int clause, const QString& columnName, QString& str, QString& sql)
    {
        checkClause(clause, sql);
        sql += columnName + " is " + str + " ";

        return true;
    }
}

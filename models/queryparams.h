#ifndef QUERYPARAMS_H
#define QUERYPARAMS_H

#include <QString>
#include <QDateTime>
#include <QVector>

namespace Db {
    class QueryParams{
    public:
        enum class OrderDirection{
            Ascending,
            Descending
        };

        enum class Status{
            Any,
            Null,
            NotNull
        };

        enum class Bool{
            True,
            False,
            Any
        };

    public:
        QueryParams() :
            orderDirection(OrderDirection::Descending),
            limit(500)
        {}

        void apply(QString& sql) const;
        void reset();

        void filterOrderBy(const QString& columnName, QString& sql) const;
        void filterLimit(QString& sql) const;

    protected:
        void applyOrderDirection(QString& sql) const;

        static bool filterColumnById(int clause, const QString& columnName, const QVector<int>& idList, QString& sql);
        static bool filterColumnByDateRange(int clause, const QString& columnName, const QDateTime& begin, const QDateTime& end, QString& sql);
        static bool filterStatus(int clause, const QString& columnName, Status status, QString& sql);
        static bool filterBool(int clause, const QString& columnName, Bool b, QString& sql);
        static bool filterString(int clause, const QString& columnName, QString& str, QString& sql);

        static void checkClause(int clause, QString& sql);

    public:
        OrderDirection orderDirection;
        unsigned int limit;
    };
}

#endif // QUERYPARAMS_H

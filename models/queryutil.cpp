#include "queryutil.h"

namespace Db { namespace QueryUtil {

    QString buildInClause(const QVector<int>& idList)
    {
        QString sql = " (";

        for (int i = 0; i < idList.size(); ++i) {
            if (i) {
                sql += ", ";
            }

            sql += QString::number(idList[i]);
        }

        sql += ')';

        return sql;
    }

    QString buildInClause(const QStringList& strings)
    {
        QString sql = " (";

        for (int i = 0; i < strings.size(); ++i) {
            if (i) {
                sql += ", ";
            }

            sql += "'" + strings[i] + "'";
        }

        sql += ')';

        return sql;
    }

    QString buildValueList(int count)
    {
        QString sql = " (";

        for (int i = 0; i < count; i++) {
            if (i) {
                sql += ", ";
            }

            sql += '?';
        }

        sql += ')';

        return sql;
    }

}}

#ifndef QUERYUTIL_H
#define QUERYUTIL_H

#include <QStringList>
#include <QVector>

namespace Db { namespace QueryUtil {
    QString buildInClause(const QVector<int>& idList);
    QString buildInClause(const QStringList& strings);
    QString buildValueList(int count);
}}


#endif // QUERYUTIL_H

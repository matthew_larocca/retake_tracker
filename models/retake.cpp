#include "retake.h"

#include "queryutil.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QVariant>

namespace Db {
    void retakeQueryToVector(QSqlQuery& query, RetakeVector& retakes);
    void queryResultToRetake(QSqlQuery& query, Retake& retake);
    void bindRetakeValues(QSqlQuery& query, const Retake& retake);

    const QString retakeSelectSql =
        "SELECT retakes.id, retakes.student_id, retakes.scheduled_date, retakes.version_id, learning_objectives.id as learning_objective_id, classes.id as class_id, "
        "retakes.class_objective_id, retakes.retake_completed, retakes.comments, students.first_name, students.last_name, "
        "classes.name as class_name, learning_objectives.name as learning_objective_name, retake_versions.name as retake_version_name "
        "from retakes "
        "join class_objectives on class_objectives.id = retakes.class_objective_id "
        "join students on retakes.student_id = students.id "
        "join classes on class_objectives.class_id = classes.id "
        "join learning_objectives on class_objectives.learning_objective_id = learning_objectives.id "
        "join retake_versions on retakes.version_id = retake_versions.id ";

    Retakes::Retakes(QSqlDatabase* database)
        : _database(database)
    {}

    bool Retakes::execQueryToVector(QSqlQuery& query, RetakeVector& retakes)
    {
        if (query.exec())
        {
            retakeQueryToVector(query, retakes);
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool Retakes::getAllRetakes(RetakeVector& retakes, const RetakeQueryParams& params)
    {
        QString sql = params.filterSelect(retakeSelectSql, 0);
        QSqlQuery query;
        query.prepare(sql);

        if (params.beginTime.isValid())
        {
            query.bindValue(":begin", params.beginTime.toUTC());
        }

        if (params.endTime.isValid())
        {
            query.bindValue(":end", params.endTime.toUTC());
        }

        return execQueryToVector(query, retakes);
    }

    bool Retakes::getRetakes(const QVector<int>& ids, RetakeVector& retakes)
    {
        QString sqlText = retakeSelectSql + " WHERE retakes.id in " + Db::QueryUtil::buildValueList(ids.size());
        QSqlQuery query;
        query.prepare(sqlText);

        for (auto& id : ids) {
            query.addBindValue(id);
        }

        return execQueryToVector(query, retakes);
    }

    bool Retakes::getRetakesById(const QString& columnName, int id, RetakeVector& retakes, const RetakeQueryParams& params)
    {
        QString sql = retakeSelectSql + " where "+ columnName +" = :id";
        params.apply(sql);

        QSqlQuery query;
        query.prepare(sql);
        query.bindValue(":id", id);

        return execQueryToVector(query, retakes);
    }


    bool Retakes::deleteRetakesForClass(int classId)
    {
        //TODO: Implement me
        return false;
    }

    bool Retakes::deleteRetakesForLearningObjective(int learningObjectiveId)
    {
        QSqlQuery query;
        query.prepare("delete from retakes where class_objective_id in (select id from class_objectives where class_objectives.learning_objective_id = :learningObjectiveId)");
        query.bindValue(":learningObjectiveId", learningObjectiveId);

        return query.exec();
    }

    bool Retakes::deleteRetakesForClassObjective(int classObjectiveId)
    {
        return deleteRetakesById("retakes.class_objective_id", classObjectiveId);
    }

    bool Retakes::getRetakesForClass(int classId, RetakeVector& retakes)
    {
        return getRetakesById("class_objectives.class_id", classId, retakes, RetakeQueryParams());
    }

    bool Retakes::deleteRetakesById(const QString& columnName, int id)
    {
        QString sql = "DELETE from retakes where " + columnName + " = :id;";
        QSqlQuery query;
        query.prepare(sql);
        query.bindValue(":id", id);

        bool result = query.exec();

        if (!result)
        {
            _lastError = query.lastError().text();
        }

        return result;
    }

    bool Retakes::getRetake(int retakeId, Retake& retake)
    {
        RetakeVector retakes;

        if (getRetakesById("retakes.id", retakeId, retakes, RetakeQueryParams()))
        {
            retake = retakes[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Retakes::createRetake(Retake& retake)
    {
        QString sql = "INSERT into retakes (student_id, version_id, retake_completed, comments, scheduled_date, class_objective_id) VALUES (:studentId, :version_id, :retakeCompleted, :comments, :scheduledDate, :classObjectiveId );";
        QSqlQuery query;
        query.prepare(sql);
        bindRetakeValues(query, retake);

        if (query.exec())
        {
            retake.id = query.lastInsertId().toInt();
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }

    }

    bool Retakes::updateRetake(Retake& retake)
    {
        QString sql = "UPDATE retakes set student_id = :studentId, version_id = :version_id, retake_completed = :retakeCompleted, comments = :comments, scheduled_date = :scheduledDate WHERE id = :id;";
        QSqlQuery query;
        query.prepare(sql);
        bindRetakeValues(query, retake);
        query.bindValue(":id", retake.id);

        if (query.exec())
        {
            return true;
        }
        else
        {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool Retakes::deleteRetake(int retakeId)
    {
        QSqlQuery query;
        query.prepare("Delete from retakes where id = :id;");
        query.bindValue(":id", retakeId);

        return query.exec();
    }

    void queryResultToRetake(QSqlQuery& query, Retake& retake)
    {
        QString studentFirstName, studentLastName;

        retake.id = query.value("id").toInt();
        retake.classObjectiveId = query.value("class_objective_id").toInt();
        retake.studentId = query.value("student_id").toInt();
        retake.retakeCompleted = query.value("retake_completed").toBool();
        retake.versionId = query.value("version_id").toInt();
        retake.comments = query.value("comments").toString();
        retake.scheduledDate = query.value("scheduled_date").toDateTime();
        retake.classObjectiveId = query.value("class_objective_id").toInt();

        studentFirstName = query.value("first_name").toString();
        studentLastName = query.value("last_name").toString();
        retake.studentName = studentFirstName + " " + studentLastName;

        retake.className = query.value("class_name").toString();
        retake.LearningObjectiveName = query.value("learning_objective_name").toString();
        retake.retakeVersionName = query.value("retake_version_name").toString();

        retake.classId = query.value("class_id").toInt();
        retake.learningObjectiveId = query.value("learning_objective_id").toInt();
    }

    void bindRetakeValues(QSqlQuery& query, const Retake& retake)
    {
        query.bindValue(":classObjectiveId", retake.classObjectiveId);
        query.bindValue(":studentId", retake.studentId);
        query.bindValue(":retakeCompleted", retake.retakeCompleted);

        query.bindValue(":version_id", retake.versionId);
        query.bindValue(":comments", retake.comments.length() > 0 ? retake.comments : QVariant(QVariant::String));

        if (retake.scheduledDate.isValid())
        {
            query.bindValue(":scheduledDate", retake.scheduledDate);
        }
        else
        {
            query.bindValue(":scheduledDate", QVariant(QVariant::Date));
        }
    }

    void retakeQueryToVector(QSqlQuery& query, RetakeVector& retakes)
    {
        Retake retake;

        while(query.next())
        {
            queryResultToRetake(query, retake);
            retakes.append(retake);
        }
    }

    Retake::Retake()
    {
        id = -1;
        classObjectiveId = -1;
        studentId = -1;
        versionId = -1;
        retakeCompleted = false;
    }

    RetakeQueryParams::RetakeQueryParams()
        :QueryParams(),
          orderBy(OrderBy::ScheduledDate),
          scheduled(Status::Any),
          completed(Bool::Any)
    {}

    void RetakeQueryParams::reset()
    {
        orderBy = OrderBy::ScheduledDate;
        scheduled = Status::Any;
        completed = Bool::Any;

        beginTime = QDateTime();
        endTime = QDateTime();

        studentIds.clear();

        QueryParams::reset();
    }

    void RetakeQueryParams::filterOrder(QString& sql) const
    {
        switch (orderBy) {
        case OrderBy::ScheduledDate:
            filterOrderBy("retakes.scheduled_date", sql);
            break;

        case OrderBy::CreatedDate:
            filterOrderBy("retakes.id", sql);
            break;
        }
    }

    QString RetakeQueryParams::filterSelect(const QString& selectStatement, int clause) const {
        QString sql = selectStatement;


        if (filterColumnById(clause, "retakes.student_id", studentIds, sql)) clause += 1;
        if (filterColumnById(clause, "retakes.version_id", versionIds, sql)) clause += 1;
        if (filterColumnByDateRange(clause, "retakes.scheduled_date", beginTime, endTime, sql)) clause += 1;
        if (filterBool(clause, "retakes.retake_completed", completed, sql)) clause += 1;
        if (filterStatus(clause, "retakes.scheduled_date", scheduled, sql)) clause += 1;

        filterOrder(sql);
        filterLimit(sql);

        return sql;
    }
}

#ifndef RETAKE_H
#define RETAKE_H

#include "queryparams.h"

#include <QString>
#include <QSqlDatabase>
#include <QString>
#include <QVector>
#include <QDateTime>

namespace Db {
    struct Retake
    {
        Retake();

        int id;
        int classObjectiveId;
        int versionId;
        int studentId;
        bool retakeCompleted;
        QString comments;

        QDateTime scheduledDate;

        QString studentName;
        QString className;
        QString LearningObjectiveName;
        QString retakeVersionName;

        //read only
        int classId;
        int learningObjectiveId;


        // this retake is ready to have a version assigned to it
        bool readyForVersionAssignment() const
        {
            return classObjectiveId != -1 && studentId != -1;
        }

        // This retake can be sucessfully inserted into the database
        bool isValid() const
        {
            return  readyForVersionAssignment() && versionId != -1;
        }
    };

    using RetakeVector = QVector<Retake>;

    class RetakeQueryParams : public QueryParams{
    public:
        enum class OrderBy{
            ScheduledDate,
            CreatedDate
        };

    public:
        RetakeQueryParams();

        QString filterSelect(const QString& selectStatement, int clause) const;
        void reset();

    private:
        void filterOrder(QString& sql) const;

    public:

        OrderBy orderBy;
        Status scheduled;
        Bool completed;

        QVector<int> studentIds;
        QVector<int> versionIds;
        QDateTime beginTime;
        QDateTime endTime;
    };

    class Retakes{
    public:
        Retakes(QSqlDatabase* database);

    public:
        bool createRetake(Retake& retake);
        bool updateRetake(Retake& retake);
        bool deleteRetake(int retakeId);
        bool getRetakesForClass(int classId, RetakeVector& retakes);
        bool deleteRetakesForClass(int classId);
        bool deleteRetakesForLearningObjective(int learningObjectiveId);
        bool deleteRetakesForClassObjective(int classObjectiveId);
        bool getRetake(int retakeId, Retake& retake);
        bool getAllRetakes(RetakeVector& retakes, const RetakeQueryParams& params = RetakeQueryParams());
        bool getRetakes(const QVector<int>& ids, RetakeVector& retakes);

    private:
        bool getRetakesById(const QString& columnName, int id,RetakeVector& retakes, const RetakeQueryParams& params);
        bool deleteRetakesById(const QString& columnName, int id);

        bool execQueryToVector(QSqlQuery& query, RetakeVector& retakes);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };
}

#endif // RETAKE_H

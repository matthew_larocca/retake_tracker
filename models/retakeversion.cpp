#include "retakeversion.h"

#include "queryutil.h"

#include <QVariant>
#include <QSqlError>

namespace Db {
    void bindRetakeValues(QSqlQuery& query, const RetakeVersion& retakeVersion);
    void queryResultToRetakeVersion(QSqlQuery& query, RetakeVersion& retakeVersion);
    void retakeQueryToVector(QSqlQuery& query, RetakeVersionVector& retakeVersions);

    RetakeVersions::RetakeVersions(QSqlDatabase* database)
        :_database(database)
    {}

    const QString fileSelectStatement = "SELECT id, name, description, learning_objective_id, file_id FROM retake_versions ";

    bool RetakeVersions::createRetakeVersion(RetakeVersion& retakeVersion)
    {
        QSqlQuery query;
        query.prepare("INSERT into retake_versions (name, description, learning_objective_id, file_id) VALUES (:name, :description, :learning_objective_id, :file_id);");
        bindRetakeValues(query, retakeVersion);

        const auto result = execQuery(query);

        if (result) {
            retakeVersion.id = query.lastInsertId().toInt();
        }

        return result;
    }

    bool RetakeVersions::getVersionsForLearningObjective(int learningObjectiveId, RetakeVersionVector& versions)
    {
        QSqlQuery query;
        query.prepare(fileSelectStatement + "WHERE learning_objective_id = :learning_objective_id;");
        query.bindValue(":learning_objective_id", learningObjectiveId);

        const auto result = execQuery(query);

        if (result) {
            retakeQueryToVector(query, versions);
        }

        return result;
    }

    bool RetakeVersions::getRetakeVersion(int id, RetakeVersion& retakeVersion)
    {
        QSqlQuery query;
        query.prepare(fileSelectStatement + "WHERE id = :id;");
        query.bindValue(":id", id);

        const auto result = execQuery(query);

        if (result && query.next()) {
            queryResultToRetakeVersion(query, retakeVersion);
        }

        return result;
    }

    bool RetakeVersions::deleteRetakeVersionsForLearningObjective(int learningObjectiveId)
    {
        QSqlQuery query;
        query.prepare("DELETE from retake_versions where learning_objective_id = :learningObjectiveId;");
        query.bindValue(":learningObjectiveId", learningObjectiveId);

        return query.exec();
    }

    bool RetakeVersions::getVersionFileIdsForLearningObjective(int learningObjectiveId, QVector<int>& ids)
    {
        QSqlQuery query;
        query.prepare("select distinct file_id from retake_versions where learning_objective_id = :learningObjectiveId;");
        query.bindValue(":learningObjectiveId", learningObjectiveId);

        if (query.exec()) {
            while (query.next()) {
                ids.push_back(query.value(0).toInt());
            }
            return true;
        }
        else {
            return false;
        }
    }

    bool RetakeVersions::deleteRetakeVersion(int id)
    {
        QVector<int> idVec = {id};
        return deleteRetakeVersions(idVec);
    }

    bool RetakeVersions::deleteRetakeVersions(const QVector<int>& ids)
    {
        QSqlQuery query;
        query.prepare("DELETE from retake_versions where id in " + QueryUtil::buildValueList(ids.size()));
        for (auto id : ids) {
            query.addBindValue(id);
        }

        return query.exec();
    }

    bool RetakeVersions::getRetakeVersionCountsForStudent(int learningObjectiveId, int classObjectiveId, int studentId, RetakeVersionCounts& counts)
    {
        //get list of all the retake versions available for this objective
        QSqlQuery q1;
        q1.prepare("select name, id from retake_versions where learning_objective_id = ?;");
        q1.addBindValue(learningObjectiveId);

        q1.exec();
        while (q1.next()) {
            counts[q1.value(1).toInt()] = QPair<QString, int>{q1.value(0).toString(), 0};
        }

        //get the counts of all the versions this student has taken for this objective
        QSqlQuery q2;
        q2.prepare("select version_id, count(version_id) from retakes where student_id = ? and class_objective_id = ? Group By version_id;");
        q2.addBindValue(studentId);
        q2.addBindValue(classObjectiveId);

        q2.exec();

        while (q2.next()) {
            counts[q2.value(0).toInt()].second = q2.value(1).toInt();
        }

        return true;
    }

    bool RetakeVersions::execQuery(QSqlQuery& query)
    {
        if (query.exec()) {
            return true;
        }
        else {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool RetakeVersions::updateRetakeVersion(RetakeVersion& retakeVersion)
    {
        QSqlQuery query;
        query.prepare("UPDATE retake_versions SET name = :name, description = :description, learning_objective_id = :learning_objective_id, file_id = :file_id WHERE id = :id;");
        bindRetakeValues(query, retakeVersion);
        query.bindValue(":id", retakeVersion.id);

        return execQuery(query);
    }

    void bindRetakeValues(QSqlQuery& query, const RetakeVersion& retakeVersion)
    {
        query.bindValue(":name", retakeVersion.name);
        query.bindValue(":description", retakeVersion.description);
        query.bindValue(":learning_objective_id", retakeVersion.learning_objective_id);
        query.bindValue(":file_id", retakeVersion.file_id);
    }

    void queryResultToRetakeVersion(QSqlQuery& query, RetakeVersion& retakeVersion)
    {
        retakeVersion.id = query.value("id").toInt();
        retakeVersion.name = query.value("name").toString();
        retakeVersion.description = query.value("description").toString();
        retakeVersion.learning_objective_id = query.value("learning_objective_id").toInt();
        retakeVersion.file_id = query.value("file_id").toInt();
    }

    void retakeQueryToVector(QSqlQuery& query, RetakeVersionVector& retakeVersions)
    {
        RetakeVersion retakeVersion;
        while (query.next()) {
            queryResultToRetakeVersion(query, retakeVersion);
            retakeVersions.append(retakeVersion);
        }
    }
}

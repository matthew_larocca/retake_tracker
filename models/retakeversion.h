#ifndef RETAKEVERSION_H
#define RETAKEVERSION_H

#include <QString>
#include <QVector>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QHash>
#include <QPair>

namespace Db {

    struct RetakeVersion
    {
        int id;
        QString name;
        QString description;
        int learning_objective_id;
        int file_id = 0;
    };

    using RetakeVersionVector = QVector<RetakeVersion>;
    using RetakeVersionCounts = QHash<int, QPair<QString, int>>;

    class RetakeVersions
    {
    public:
        RetakeVersions(QSqlDatabase* database);

    public:
        bool createRetakeVersion(RetakeVersion& retakeVersion);
        bool updateRetakeVersion(RetakeVersion& retakeVersion);
        bool getRetakeVersion(int id, RetakeVersion& retakeVersion);
        bool getVersionsForLearningObjective(int learningObjectiveId, RetakeVersionVector& versions);
        bool getRetakeVersionCountsForStudent(int learningObjectiveId, int classObjectiveId, int studentId, RetakeVersionCounts& counts);
        bool deleteRetakeVersion(int id);
        bool deleteRetakeVersions(const QVector<int>& ids);
        bool deleteRetakeVersionsForLearningObjective(int learningObjectiveId);

        bool getVersionFileIdsForLearningObjective(int learningObjectiveId, QVector<int>& ids);
    private:
        bool execQuery(QSqlQuery& query);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };

}

#endif // RETAKEVERSION_H

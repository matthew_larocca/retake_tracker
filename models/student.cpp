#include "student.h"

#include "queryutil.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QVariant>
#include <QSet>

namespace Db {
    void studentQueryToVector(QSqlQuery& query, StudentVector& students);
    void queryResultToStudent(QSqlQuery& query, Student& student);
    void bindStudentValues(QSqlQuery& query, const Student& student);

    Students::Students(QSqlDatabase* database)
            :_database(database)
        {}


    bool Students::createStudent(Student& student)
    {
        QSqlQuery query;
        query.prepare("INSERT INTO students (first_name, last_name, active, email) VALUES (:firstName, :lastName, :active, :email)");
        bindStudentValues(query, student);

        bool result = query.exec();

        if (result) {
            student.id = query.lastInsertId().toInt();
        }
        else {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool Students::updateStudent(Student& student)
    {
        QSqlQuery query;
        query.prepare("update students set first_name = :firstName, last_name = :lastName, active = :active, email = :email where id = :id;");
        bindStudentValues(query, student);
        query.bindValue(":id", student.id);

        bool result = query.exec();

        if (!result) {
            _lastError = query.lastError().databaseText();
        }

        return result;
    }

    bool Students::getStudent(int id, Student& student)
    {
        QSqlQuery query;
        query.prepare("select id, first_name, last_name, email, active from students where id = :id;");
        query.bindValue(":id", id);

        if (query.exec()) {
            if (query.next()) {
                queryResultToStudent(query, student);
                return true;
            }
        }
        else {
            _lastError = query.lastError().databaseText();
        }

        return false;
    }

    bool Students::getStudents(const QVector<int> idList, StudentVector& studentVector, const StudentQueryParams& queryParams)
    {
        if (idList.size() == 0) {
            return true;
        }
        else {
            return execInQuery(Db::QueryUtil::buildInClause(idList), "id", studentVector, queryParams);
        }
    }

    bool Students::execInQuery(const QString whereClause, const QString& columnName, StudentVector& studentVector, const StudentQueryParams& queryParams)
    {
        QString sql = "select students.id, students.first_name, students.last_name, students.active, students.email from students where " + columnName + " in ";
        sql += whereClause;
        sql = queryParams.filterSelect(sql, 1);

        QSqlQuery query;
        query.prepare(sql);

        return execQueryToVector(query, studentVector);
    }

    bool Students::getStudents(const QStringList emailList, StudentVector& studentVector, const StudentQueryParams& queryParams)
    {
        if (emailList.size() == 0) {
            return true;
        }
        else {
            return execInQuery(Db::QueryUtil::buildInClause(emailList), "email", studentVector, queryParams);
        }
    }

    bool Students::getAllStudents(StudentVector& studentVector, const StudentQueryParams& queryParams)
    {
        QString sql = "select students.id, students.first_name, students.last_name, students.active, students.email from students";
        sql = queryParams.filterSelect(sql, 0);

        QSqlQuery query;
        query.prepare(sql);

        return execQueryToVector(query, studentVector);
    }

    bool Students::execQueryToVector(QSqlQuery& query, StudentVector& studentVector)
    {
        if (query.exec()){
            studentQueryToVector(query, studentVector);
            return true;
        }
        else {
            _lastError = query.lastError().databaseText();
            return false;
        }
    }

    bool Students::getStudentsInClass(int classId, StudentVector& studentVector, const StudentQueryParams& queryParams)
    {
        QString sql = "select students.id, students.first_name, students.last_name, students.active, students.email from enrollments join students on enrollments.student_id where students.id = enrollments.student_id AND enrollments.class_id = :classId";
        sql = queryParams.filterSelect(sql, 2);

        QSqlQuery query;
        query.prepare(sql);
        query.bindValue(":classId", classId);

        return execQueryToVector(query, studentVector);
    }

    // Precodition: all email address in emails have had .toLower() called on them.
    bool Students::getStudentsForEmails(const QSet<QString>& emails, Db::StudentVector& students)
    {
        QString queryText = "SELECT id, first_name, last_name, active, email from students where lower(email) in ";
        queryText += Db::QueryUtil::buildValueList(emails.size()) + " AND active = 1;";

        QSqlQuery query;
        query.prepare(queryText);

        for (auto& email : emails) {
            query.addBindValue(email);
        }

        const auto result = query.exec();

        if (!result) {
            return false;
        }

        studentQueryToVector(query, students);

        return true;
    }

    // Note that we store all emails as lower case in the database.
    // This is for simplicity's sake and allows for easy searching and email address mathcing when inporting retakes..
    void bindStudentValues(QSqlQuery& query, const Student& student)
    {
        query.bindValue(":firstName", student.firstName);
        query.bindValue(":lastName", student.lastName);
        query.bindValue(":active", student.active);
        query.bindValue(":email", student.email);
    }

    void studentQueryToVector(QSqlQuery& query, StudentVector& students)
    {
        Student student;
        while (query.next()) {
            queryResultToStudent(query, student);

            students.append(student);
        }
    }

    void queryResultToStudent(QSqlQuery& query, Student& student)
    {
        student.id = query.value("id").toInt();
        student.firstName = query.value("first_name").toString();
        student.lastName = query.value("last_name").toString();
        student.active = query.value("active").toBool();
        student.email = query.value("email").toString();
    }


    StudentQueryParams::StudentQueryParams()
        : QueryParams(),
          orderBy(OrderBy::LastName),
          active(Bool::True)
    {}

    void StudentQueryParams::filterOrder(QString& sql) const
    {
        switch (orderBy) {
        case OrderBy::LastName:
            filterOrderBy("students.last_name", sql);
            break;

            case OrderBy::FirstName:
            filterOrderBy("students.first_name", sql);
            break;
        }
    }

    QString StudentQueryParams::filterSelect(const QString& selectStatement, int clause) const
    {
        QString sql = selectStatement;
        if (filterBool(clause, "students.active", active, sql)) clause += 1;

        filterOrder(sql);
        filterLimit(sql);

        return sql;
    }
}

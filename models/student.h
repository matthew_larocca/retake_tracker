#ifndef STUDENT_H
#define STUDENT_H

#include "queryparams.h"

#include <QString>
#include <QSqlDatabase>
#include <QVector>

namespace  Db {
    struct Student
    {
        int id;
        QString firstName;
        QString lastName;
        QString email;
        bool active;
    };

    using StudentVector = QVector<Student>;

    class StudentQueryParams : public QueryParams{
    public:
        enum class OrderBy{
          FirstName,
          LastName
        };

    public:
        StudentQueryParams();

        QString filterSelect(const QString& selectStatement, int clause) const;
    private:
        void filterOrder(QString& sql) const;

    public:
        OrderBy orderBy;
        Bool active;
    };

    class Students{

    public:
        Students(QSqlDatabase* database);

    public:
        bool createStudent(Student& student);
        bool updateStudent(Student& student);
        bool getAllStudents(StudentVector& studentVector, const StudentQueryParams& queryParams = StudentQueryParams());
        bool getStudent(int id, Student& student);
        bool getStudents(const QVector<int> idList, StudentVector& studentVector, const StudentQueryParams& queryParams = StudentQueryParams());
        bool getStudents(const QStringList emailList, StudentVector& studentVector, const StudentQueryParams& queryParams = StudentQueryParams());
        bool getStudentsInClass(int classId, StudentVector& studentVector, const StudentQueryParams& queryParams = StudentQueryParams());
        bool getStudentsForEmails(const QSet<QString>& emails, Db::StudentVector& students);
    private:
        bool execQueryToVector(QSqlQuery& query, StudentVector& studentVector);
        bool execInQuery(const QString whereClause, const QString& columnName, StudentVector& studentVector, const StudentQueryParams& queryParams);

    private:
        QSqlDatabase* _database;
        QString _lastError;
    };
}

#endif // STUDENT_H

#include "pdfdocument.h"

#include <QHash>

#include <fpdfview.h>
#include <string>

namespace QtPdfium {

class PdfiumPage : public Page {
public:
    PdfiumPage(FPDF_PAGE page, int index);
    ~PdfiumPage();


    virtual QSizeF getSize() const;
    virtual QImage render(const QSize& size) const;
    virtual int getIndex() const;

    void close();

    FPDF_PAGE _page;
    int _index;
};

struct Document::Impl {
    QString path;
    FPDF_DOCUMENT document;
    QHash<int, PdfiumPage*> pages;
};

Document::Document(const QString& path) {
    _impl = new Impl();

    _impl->path = path;

    std::string localPath = path.toLocal8Bit().constData();
    _impl->document = FPDF_LoadDocument(localPath.c_str(), nullptr);
}

Document::~Document() {
    if (_impl->document){
        closeAllPages();
        FPDF_CloseDocument(_impl->document);
    }

    delete _impl;
}

int Document::pageCount() const {
    return FPDF_GetPageCount(_impl->document);
}

Page* Document::getPage(int index){
    if (index >= pageCount()) {
        return nullptr;
    }

    auto page = _impl->pages.find(index);
    if (page == _impl->pages.end()) {
        FPDF_PAGE pdfPage = FPDF_LoadPage(_impl->document, index);
        PdfiumPage* p = new PdfiumPage(pdfPage, index);

        _impl->pages[index] = p;

        return p;
    }
    else{
        return page.value();
    }
}

void Document::closeAllPages() {
    auto end = _impl->pages.end();
    for (auto it = _impl->pages.begin(); it != end; ++it) {
        delete it.value();
    }

    _impl->pages.clear();
}

void Document::closePage(int index) {
    auto result = _impl->pages.find(index);

    if (result != _impl->pages.end()) {
        delete result.value();
        _impl->pages.erase(result);
    }
}


PdfiumPage::PdfiumPage(FPDF_PAGE page, int index) {
    _page = page;
    _index = index;
}

PdfiumPage::~PdfiumPage() {
    close();
}

void PdfiumPage::close(){
    if (_page != nullptr) {
        FPDF_ClosePage(_page);
        _page = nullptr;
    }
}

QSizeF PdfiumPage::getSize() const {
    return QSizeF(FPDF_GetPageWidth(_page), FPDF_GetPageHeight(_page));
}

QImage PdfiumPage::render(const QSize& size) const {
    float scaleFactor = 1.0f;
    int width = static_cast<int>(size.width() * scaleFactor);
    int height = static_cast<int>(size.height() * scaleFactor);

    QImage image(width, height, QImage::Format_RGBA8888);
    image.fill(0xFFFFFFFF);

    // Render out an image with Pdfium into the QImage created above
    // Note that Pdfium renders to BGRA
    FPDF_BITMAP bitmap = FPDFBitmap_CreateEx(image.width(), image.height(),FPDFBitmap_BGRA,
                                             image.scanLine(0), image.bytesPerLine());

    FPDF_RenderPageBitmap(bitmap, _page, 0, 0, width, height, 0, 0);
    FPDFBitmap_Destroy(bitmap);


    //Need to convert Pdfium BGRA to RGBA
    for(int i = 0; i < image.height(); ++i) {
        uchar *pixels = image.scanLine(i);

        for(int j = 0; j < image.width(); ++j) {
            std::swap(pixels[0], pixels[2]);
            pixels += 4;
        }
    }

    return image;
}

int PdfiumPage::getIndex() const {
    return _index;
}

}

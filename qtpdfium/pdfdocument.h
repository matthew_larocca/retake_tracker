#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QString>
#include <QImage>
#include <QSizeF>

namespace QtPdfium {

class Page {
public:
    virtual QSizeF getSize() const = 0;
    virtual QImage render(const QSize& size) const = 0;
    virtual int getIndex() const = 0;
    virtual ~Page(){}
};


class Document {
public:
    Document(const QString& path);
    ~Document();

public:

    int pageCount() const;
    QString path() const;

    Page* getPage(int index);
    void closePage(int index);
    void closeAllPages();

private:
    struct Impl;
    Impl* _impl;
};

}



#endif // DOCUMENT_H

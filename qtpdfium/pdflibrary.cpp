#include "pdflibrary.h"

#include <fpdfview.h>

namespace QtPdfium {

struct Library::Impl {
    Error lastError;
};

Library::Library(){
    FPDF_LIBRARY_CONFIG config;
    config.version = 2;
    config.m_pUserFontPaths = NULL;
    config.m_pIsolate = NULL;
    config.m_v8EmbedderSlot = 0;

    FPDF_InitLibraryWithConfig(&config);

    _impl = new Impl();
    _impl->lastError = Error::None;
}

Library::~Library(){
    FPDF_DestroyLibrary();
    delete _impl;
}

Document* Library::open(const QString& path){
    return new Document(path);
}

Library::Error Library::lastError() const{
    return _impl->lastError;
}

}

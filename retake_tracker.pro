#-------------------------------------------------
#
# Project created by QtCreator 2018-01-01T23:09:01
#
#-------------------------------------------------

QT       += core gui sql widgets printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RetakeTracker
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#sets the application icon for the dock in MacOS
ICON = retake_tracker.icns

SOURCES += \
        main.cpp \
    views/studentslist.cpp \
    dialogs/studentdialog.cpp \
    views/studentdetail.cpp \
    views/classeslist.cpp \
    dialogs/classdialog.cpp \
    views/classdetail.cpp \
    dialogs/enrollstudentdialog.cpp \
    dialogs/learningobjectivedialog.cpp \
    dialogs/retakedialog.cpp \
    views/home.cpp \
    views/retakeslist.cpp \
    dialogs/retakefilterdialog.cpp \
    dialogs/pdfdocumentpreview.cpp \
    qtpdfium/pdflibrary.cpp \
    qtpdfium/pdfdocument.cpp \
    dialogs/retakeversiondialog.cpp \
    import/retakeimporter.cpp \
    dialogs/retakeimportdialog.cpp \
    views/learningobjectiveslist.cpp \
    widgets/learningobjectivepicker.cpp \
    data/classdata.cpp \
    widgets/classrosterimport.cpp \
    dialogs/editclassobjectivesdialog.cpp \
    data/classobjectivedata.cpp \
    dialogs/learningobjectiveexportdialog.cpp \
    export/learningobjectiveexport.cpp \
    import/learningobjectiveimport.cpp \
    util/filepacker.cpp \
    dialogs/learningobjectiveimportdialog.cpp \
    import/studentimporter.cpp \
    annotation/annotationmanager.cpp \
    annotation/annotationsource.cpp \
    widgets/pdfview.cpp \
    util/retakeprintjob.cpp \
    views/tabmanager.cpp \
    widgets/util.cpp \
    views/mainwindow.cpp \
    models/annotation.cpp \
    models/class.cpp \
    models/classobjective.cpp \
    models/enrollments.cpp \
    models/file.cpp \
    models/learningobjective.cpp \
    models/learningobjectivecategory.cpp \
    models/queryparams.cpp \
    models/queryutil.cpp \
    models/retake.cpp \
    models/retakeversion.cpp \
    models/student.cpp \
    database/database.cpp

HEADERS += \
    views/studentslist.h \
    dialogs/studentdialog.h \
    views/studentdetail.h \
    views/classeslist.h \
    dialogs/classdialog.h \
    views/classdetail.h \
    dialogs/enrollstudentdialog.h \
    dialogs/learningobjectivedialog.h \
    dialogs/retakedialog.h \
    views/home.h \
    views/retakeslist.h \
    dialogs/retakefilterdialog.h \
    dialogs/pdfdocumentpreview.h \
    qtpdfium/qtpdfium.h \
    qtpdfium/pdflibrary.h \
    qtpdfium/pdfdocument.h \
    dialogs/retakeversiondialog.h \
    import/retakeimporter.h \
    dialogs/retakeimportdialog.h \
    views/learningobjectiveslist.h \
    widgets/learningobjectivepicker.h \
    data/classdata.h \
    widgets/classrosterimport.h \
    dialogs/editclassobjectivesdialog.h \
    data/classobjectivedata.h \
    dialogs/learningobjectiveexportdialog.h \
    export/learningobjectiveexport.h \
    import/learningobjectiveimport.h \
    util/filepacker.h \
    dialogs/learningobjectiveimportdialog.h \
    import/studentimporter.h \
    annotation/annotationmanager.h \
    annotation/annotationsource.h \
    widgets/pdfview.h \
    views/idataview.h \
    views/mainwindow.h \
    util/retakeprintjob.h \
    views/tabmanager.h \
    widgets/util.h \
    models/annotation.h \
    models/class.h \
    models/classobjective.h \
    models/enrollments.h \
    models/file.h \
    models/learningobjective.h \
    models/learningobjectivecategory.h \
    models/queryparams.h \
    models/queryutil.h \
    models/retake.h \
    models/retakeversion.h \
    models/student.h \
    database/database.h

FORMS += \
    views/studentslist.ui \
    dialogs/studentdialog.ui \
    views/studentdetail.ui \
    views/classeslist.ui \
    dialogs/classdialog.ui \
    views/classdetail.ui \
    dialogs/enrollstudentdialog.ui \
    dialogs/learningobjectivedialog.ui \
    dialogs/retakedialog.ui \
    views/home.ui \
    views/retakeslist.ui \
    dialogs/retakefilterdialog.ui \
    dialogs/pdfdocumentpreview.ui \
    dialogs/retakeversiondialog.ui \
    dialogs/retakeimportdialog.ui \
    views/learningobjectiveslist.ui \
    widgets/learningobjectivepicker.ui \
    widgets/classrosterimport.ui \
    dialogs/editclassobjectivesdialog.ui \
    dialogs/learningobjectiveexportdialog.ui \
    dialogs/learningobjectiveimportdialog.ui \
    views/mainwindow.ui

RESOURCES += \
    icons.qrc \
    sql.qrc

CONFIG(debug, debug|release) {
    CONFIG_DIR = debug
}

CONFIG(release, debug|release) {
    CONFIG_DIR = release
}

INCLUDEPATH += "$$PWD/external/pdfium/include"
LIBS += "-L$$PWD/external/pdfium/lib/x64.$$CONFIG_DIR"

win32:LIBS += pdfium.dll.lib user32.lib advapi32.lib gdi32.lib
macx:LIBS += -lpdfium -framework AppKit

# This will copy the dll into the application directory
win32 {
    PDFIUM_BIN_SRC_DIR = "\"$$PWD/external/pdfium/bin/x64.$$CONFIG_DIR\""
    PDFIUM_BIN_DST_DIR = "\"$$OUT_PWD/$$CONFIG_DIR\""

    copydata.commands = $(COPY_DIR) $$PDFIUM_BIN_SRC_DIR $$PDFIUM_BIN_DST_DIR
    first.depends = $(first) copydata
    export(first.depends)
    export(copydata.commands)
    QMAKE_EXTRA_TARGETS += first copydata
}

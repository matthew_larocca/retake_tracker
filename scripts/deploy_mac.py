import subprocess
import os
import sys
import shutil
import tempfile
import argparse

def main():
    parser = argparse.ArgumentParser(description="Deployment Script for Retake Tracker on MacOS")
    parser.add_argument("--qt-dir", dest="qt_dir", help="Path to qt installation.  e.g. /Users/matthew/Qt/5.10.1", type=str, required=True)
    parser.add_argument("--application-dir", dest="application_dir", help="Directory that contains the app bundle.", type=str, required=True)
    parser.add_argument("--output-path", dest="output_path", help="Path for deployed application.", type = str, required=True)
    args = parser.parse_args()

    # script params
    qt_dir = args.qt_dir
    application_dir = args.application_dir
    dmg_path = args.output_path

    macdeployqt_path = os.path.join(qt_dir, "clang_64/bin/macdeployqt")
    src_application_bundle = os.path.join(application_dir, "RetakeTracker.app")

    if os.path.exists(macdeployqt_path):
        print "Using macdeployqt at: {}".format(macdeployqt_path)
    else:
        print "Unable to locate macdeployqt executable at: {}.  Did you specify the correct path to your qt folder?".format(macdeployqt_path)
        return 1

    if os.path.exists(src_application_bundle):
        print "Deploying application bundle: {}".format(src_application_bundle)
    else:
        print "Unable to location application bundle at: {}.  Are you sure it is built and in the specified location?".format(src_application_bundle)
        return 1

    
    try:
        tmp_dir = tempfile.mkdtemp()
        application_bundle_path = os.path.join(tmp_dir, "RetakeTracker.app")
        shutil.copytree(src_application_bundle, application_bundle_path)
        
        print "Running macdeployqt on Application Bundle."

        # calls the macdeployqt command to copy the required QT libraries into the app bundle
        macdeployqt_command = [
            macdeployqt_path,
            application_bundle_path,
            "-appstore-compliant",
            "-always-overwrite",
            "-verbose=0"
        ]

        subprocess.call(macdeployqt_command)

        # need to copy and fix the pdfium dylib for deployment
        script_dir = os.path.dirname(os.path.realpath(__file__))
        pdfium_dylib_src = os.path.abspath(os.path.join(script_dir, "..", "external/pdfium/lib/x64.release/libpdfium.dylib"))
        pdfium_dylib_dest = os.path.abspath(os.path.join(application_bundle_path, "Contents/Frameworks/libpdfium.dylib"))
        retake_tracker_executable_path = os.path.join(application_bundle_path, "Contents/MacOS/RetakeTracker")

        print "Copying pdfium dylib to application bundle"

        if not os.path.exists(pdfium_dylib_src):
            print "Unable to locate pdfium release library.  Expected location: {}".format(pdfium_dylib_src)
            return 1
        
        shutil.copyfile(pdfium_dylib_src, pdfium_dylib_dest)


        print "Adding Frameworks directory to RetakeTracker Application rpath"

        install_name_tool_rpath_command = [
            "install_name_tool",
            "-add_rpath", "@executable_path/../Frameworks/", retake_tracker_executable_path
        ]

        subprocess.call(install_name_tool_rpath_command)

        print "Modifying pdfium load command in retake tracker executable"

        install_name_tool_padfium_name_command = [
            "install_name_tool",
            "-change", "./libpdfium.dylib", "@rpath/libpdfium.dylib", retake_tracker_executable_path
        ]

        subprocess.call(install_name_tool_padfium_name_command)

        print "Cretating DMG File"

        create_dmg_command = [
            "hdiutil",
            "create",  dmg_path,
            "-volname", "RetakeTracker",
            "-fs", "HFS+",
            "-srcfolder", tmp_dir
        ]

        subprocess.call(create_dmg_command)
    finally:
        print "Cleaning up Temporary Files"
        shutil.rmtree(tmp_dir)

    print "Operation Complete"
    return 0


if __name__ == "__main__":
    sys.exit(main())

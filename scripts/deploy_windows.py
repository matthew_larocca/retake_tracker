import subprocess
import os
import sys
import shutil
import tempfile
import argparse

def main():
    parser = argparse.ArgumentParser(description="Deployment Script for Retake Tracker on Windows")
    parser.add_argument("--qt-dir", dest="qt_dir", help="Path to qt installation.  e.g. /Users/matthew/Qt/5.10.1", type=str, required=True)
    parser.add_argument("--application-dir", dest="application_dir", help="Directory that contains the compiled application.", type=str, required=True)
    parser.add_argument("--redist-dir", dest="redist_dir", help="Directory that contains the visual studio redist DLL's.", type=str)
    parser.add_argument("--output-path", dest="output_path", help="path for output zip file.", type=str, required=True)
    args = parser.parse_args()
    
    qt_dir = args.qt_dir
    application_dir = args.application_dir
    redist_dir = args.redist_dir
    output_path = args.output_path
    
    windeployqt_path = os.path.join(qt_dir, "msvc2017_64/bin/windeployqt.exe")
    src_retake_tracker_exe = os.path.join(application_dir, "release/RetakeTracker.exe")
    
    if os.path.exists(windeployqt_path):
        print("Using windeployqt at: {}".format(windeployqt_path))
    else:
        print("Unable to locate windeployqt executable at: {}.  Did you specify the correct path to your qt folder?".format(windeployqt_path))
        return 1
        
    if os.path.exists(src_retake_tracker_exe):
        print("Deploying application: {}".format(src_retake_tracker_exe))
    else:
        print("Unable to location application at: {}.  Are you sure it is built and in the specified location?".format(src_retake_tracker_exe))
        return 1
        
    script_dir = os.path.dirname(os.path.realpath(__file__))
    
    try:
        tmp_dir = tempfile.mkdtemp()
        
        windeployqt_command = [
            windeployqt_path, src_retake_tracker_exe,
            "--dir", tmp_dir,
            "-verbose=0"
        ]
        
        subprocess.call(windeployqt_command)
        
        dst_retake_tracker_exe = os.path.join(tmp_dir, "RetakeTracker.exe")
        shutil.copyfile(src_retake_tracker_exe, dst_retake_tracker_exe)
        
        src_pdfium_dll = os.path.abspath(os.path.join(script_dir, "..", "external/pdfium/bin/x64.release/pdfium.dll"))
        dest_pdfium_dll = os.path.join(tmp_dir, "pdfium.dll")
        shutil.copyfile(src_pdfium_dll, dest_pdfium_dll)
        
        if redist_dir is not None:
            print("Copying redist DLL's from: {}".format(redist_dir))
            for filename in os.listdir(redist_dir):
                src_dll = os.path.join(redist_dir, filename)
                dst_dll = os.path.join(tmp_dir, filename)
                shutil.copyfile(src_dll, dst_dll)
        
        print("Creating application archive")
        shutil.make_archive(os.path.splitext(output_path)[0], 'zip', tmp_dir)
        
    finally:
        print("Cleaning up Temporary Files")
        shutil.rmtree(tmp_dir)
    
    print("Operation Complete")
    return 0


if __name__ == "__main__":
    sys.exit(main())

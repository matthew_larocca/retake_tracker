import platform
import requests
import zipfile
import tempfile
import shutil
import os
import sys

s3_base_url = "https://s3-us-west-1.amazonaws.com/pdfium-binaries/"
script_dir = os.path.dirname(os.path.realpath(__file__))

def main():
	archive_name = "pdfium_{}.zip".format(platform.system()).lower()
	archive_url = s3_base_url + archive_name
	
	temp_dir = tempfile.mkdtemp()
	print("Using temporary directory: {}".format(temp_dir))
	temp_archive_path = os.path.join(temp_dir, archive_name)
	
	try:
		print("Download pdfium binaries from: {}".format(archive_url))
		download_file(archive_url, temp_archive_path)
		
		destination_path = os.path.abspath(os.path.join(script_dir, "..", "external"))
		print ("Extracting archive to:{}".format(destination_path))
		with zipfile.ZipFile(temp_archive_path,"r") as zip_ref:
			zip_ref.extractall(destination_path)
	finally:
		print("Removing temporary directory")
		shutil.rmtree(temp_dir)
	
def download_file(url, dest):
	sys.stdout.write("Download Starting")

	r = requests.get(url, stream=True)
	
	total = int(r.headers.get('content-length'))
	received = 0
	last = 0
	
	with open(dest, "wb") as f:
		for chunk in r.iter_content(chunk_size=1024): 
			if chunk:
				received += len(chunk)
				f.write(chunk)
				
				if received - last > 100 * 1024:
					sys.stdout.write("\rDownload Progress: {0:.2f}%".format((float(received)/ float(total)) * 100.0))
					last = received
					
	sys.stdout.write("\rDownload Complete\n")

if __name__ == "__main__":
	main()
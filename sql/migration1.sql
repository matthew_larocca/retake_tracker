CREATE TABLE students
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL
, active BOOLEAN DEFAULT 1 NOT NULL, email TEXT NULL);
CREATE TABLE classes
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT NOT NULL
, active BOOLEAN DEFAULT 1 NOT NULL);
CREATE TABLE enrollments
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    student_id INTEGER NOT NULL,
    class_id INTEGER NOT NULL
);
CREATE TABLE db_info
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    key TEXT NOT NULL,
    value TEXT
);
CREATE TABLE files

(

    id INTEGER PRIMARY KEY AUTOINCREMENT,

    name TEXT NOT NULL,

    data BLOB NOT NULL

, ref_count INTEGER DEFAULT 0 NOT NULL);
CREATE TABLE retake_versions
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    learning_objective_id INTEGER NOT NULL,
    file_id INTEGER DEFAULT 0
, description TEXT NULL);
CREATE TABLE IF NOT EXISTS "annotations"
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    file_id INTEGER NOT NULL,
    page INTEGER NOT NULL,
    type INTEGER NOT NULL,
    data TEXT NOT NULL
, style TEXT NULL);
CREATE TABLE IF NOT EXISTS "class_objectives"
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    learning_objective_id INTEGER,
    class_id INTEGER
);
CREATE TABLE IF NOT EXISTS "learning_objective_categories"
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "learning_objectives"
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT NOT NULL,
    category_id INTEGER NOT NULL,
    description TEXT
);
CREATE TABLE IF NOT EXISTS "retakes"
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    student_id INTEGER NOT NULL,
    retake_completed BOOLEAN DEFAULT FALSE,
    comments TEXT,
    scheduled_date DATETIME,
    version_id INTEGER NOT NULL,
    class_objective_id INTEGER
);
CREATE UNIQUE INDEX students_id_uindex ON students (id);
CREATE UNIQUE INDEX classes_id_uindex ON classes (id);
CREATE UNIQUE INDEX enrollments_id_uindex ON enrollments (id);
CREATE INDEX enrollments_student_id_index ON enrollments (student_id);
CREATE INDEX enrollments_class_id_index ON enrollments (class_id);
CREATE UNIQUE INDEX db_info_id_uindex ON db_info (id);
CREATE INDEX db_info_key_index ON db_info (key);
CREATE INDEX retake_versions_learning_objective_id_index ON retake_versions (learning_objective_id);
CREATE INDEX retake_versions_file_id_index ON retake_versions (file_id);
CREATE INDEX students_email_index ON students (email);
CREATE INDEX annotations_file_id_index ON "annotations" (file_id);
CREATE UNIQUE INDEX learning_objectives_id_uindex ON "learning_objectives" (id);
CREATE INDEX learning_objectives_category_id_index
	on learning_objectives (category_id);
CREATE INDEX class_objectives_learning_objective_id_index ON class_objectives (learning_objective_id);
CREATE INDEX class_objectives_class_id_index ON class_objectives (class_id);
CREATE INDEX retakes_student_id_index ON "retakes" (student_id);
CREATE INDEX retakes_version_id_index ON "retakes" (version_id);
CREATE INDEX retakes_class_objective_id_index ON "retakes" (class_objective_id);
CREATE INDEX class_objectives_class_id_learning_objective_id_index ON class_objectives (class_id, learning_objective_id);

INSERT INTO db_info (key, value) VALUES ('version', '1');

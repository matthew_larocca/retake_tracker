#include "filepacker.h"

#include <QFile>
#include <QDir>
#include <QDataStream>

namespace FilePacker {

    bool pack(const List& files, const QString& dest)
    {
        QFile destFile(dest);
        if (!destFile.open(QIODevice::WriteOnly)) {
            return false;
        }

        QDataStream data;
        data.setDevice(&destFile);

        const qint32 fileCount = static_cast<qint32>(files.size());
        data << fileCount;

        for (const auto& file : files) {
            data << file.first;

            QFile f(file.second);
            if (!f.open(QIODevice::ReadOnly)) {
                return false;
            }

            data << f.readAll();
            f.close();
        }

        destFile.close();

        return true;
    }

    bool unpack(const QString& src, const QString& destDirPath)
    {
        QFile srcFile(src);
        if (!srcFile.exists()) {
            return false;
        }

        srcFile.open(QIODevice::ReadOnly);

        QDir destDir(destDirPath);
        if (!destDir.exists()) {
            return false;
        }

        QDataStream data;
        data.setDevice(&srcFile);

        qint32 fileCount;
        data >> fileCount;

        for (qint32 i = 0; i < fileCount; i++) {
            QString fileName;
            QByteArray fileData;

            data >> fileName >> fileData;

            const auto destFilePath = destDir.filePath(fileName);
            QFile destFile(destFilePath);
            destFile.open(QIODevice::WriteOnly);
            destFile.write(fileData);
        };


        return true;
    }
}

#ifndef FILEPACKER_H
#define FILEPACKER_H

#include <QPair>
#include <QVector>
#include <QString>

namespace FilePacker {
    using Item = QPair<QString, QString>;
    using List = QVector<Item>;

    bool pack(const List& files, const QString& dest);
    bool unpack(const QString& src, const QString& destDirPath);
}

#endif // FILEPACKER_H

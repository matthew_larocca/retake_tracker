#include "retakeprintjob.h"

#include "qtpdfium/pdfdocument.h"

#include <QTemporaryFile>
#include <QPainter>
#include <QSize>
#include <QSet>
#include <QPrinter>
#include <QPrinterInfo>

const double TargetDpi = 300.0;

void RetakePrintJob::init(const Db::RetakeVector& retakes)
{
    _retakes = retakes;

    //sort all the retakes by the file ID for printing convenience
    for (auto& retake : _retakes) {
        _mappedRetakes.insert(retake.versionId, &retake);
    }
}

void RetakePrintJob::setPrinterResolution(QPrinter& printer)
{
    if (printer.resolution() < TargetDpi) {
        QPrinterInfo printerInfo(printer);
        auto supportedResolutions = printerInfo.supportedResolutions();

        for (auto resolution : supportedResolutions) {
            if (resolution >= TargetDpi) {
                printer.setResolution(static_cast<int>(resolution));
            }
        }
    }
}

void RetakePrintJob::print(QPrinter& printer)
{
    _pageCount = 0;

    setPrinterResolution(printer);

    auto versionIds = QSet<int>::fromList(_mappedRetakes.keys());

    for (auto versionId : versionIds) {
        printRetakesForVersion(versionId, printer);
    }
}

void RetakePrintJob::printRetakesForVersion(int versionId, QPrinter& printer)
{
    // grab the file from the database and load it with the pdf library.
    Db::RetakeVersion retakeVersion;
    _database->retakeVersions->getRetakeVersion(versionId, retakeVersion);

    if (retakeVersion.file_id == 0) {
        return;
    }

    Db::File file;
    _database->files->getFile(retakeVersion.file_id, file);

    //get the annotations for the file and sort them by page
    Db::AnnotationVector annotations;
    _database->annotations->getAnnotationsForFile(file.id, annotations);

    QMultiHash<int, Db::Annotation*> annotationsPerPage;
    for (auto& annotation : annotations) {
        annotationsPerPage.insert(annotation.page, &annotation);
    }

    QTemporaryFile tempFile;
    tempFile.open();
    tempFile.write(file.data);

    auto pdfDocument = pdf.open(tempFile.fileName());

    const auto pageRect = printer.pageRect(QPrinter::Unit::Inch);
    const QSize printSize(static_cast<int>(pageRect.width() * printer.resolution()), static_cast<int>(pageRect.height() * printer.resolution()));

    // for each student taking this retake version need to print it out
    auto retakes = _mappedRetakes.values(versionId);

    for (auto& retake : retakes) {
        for (int i = 0; i < pdfDocument->pageCount(); i++) {
            auto page = pdfDocument->getPage(i);
            auto image = page->render(printSize);


            if (_pageCount > 0) {
                printer.newPage();
            }

            QPainter painter(&printer);
            painter.drawImage(QPoint(0,0),image);

            // render all the annotations into the image.
            auto annotations = annotationsPerPage.values(i);

            for (auto& annotation : annotations) {
                const auto normalized = Db::Annotation::fromDataString(annotation->data);
                const auto imageRect = Db::Annotation::calculateImageRect(normalized, printSize);

                const QString textStr = [annotation, retake]() -> QString {
                    switch (annotation->type){
                    case Db::Annotation::Type::Name:
                        return retake->studentName;
                    case Db::Annotation::Type::Date:
                        return retake->scheduledDate.toLocalTime().date().toString();
                    case Db::Annotation::Type::Class:
                        return retake->className;
                    default:
                        return "Unknown Value";
                    }
                }();

                painter.drawText(imageRect, Qt::AlignBottom | Qt::AlignLeft, textStr);
            }

            _pageCount += 1;
        }
    }

    delete pdfDocument;
}

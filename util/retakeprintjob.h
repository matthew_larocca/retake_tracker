#ifndef RETAKEPRINTJOB_H
#define RETAKEPRINTJOB_H

#include "database/database.h"
#include "qtpdfium/pdflibrary.h"

#include <QMultiMap>
#include <QPrinter>

class RetakePrintJob
{
public:
    RetakePrintJob(Db::Database* database) : _database(database) {}

public:
    void init(const Db::RetakeVector& retakes);
    void print(QPrinter& printer);

private:
    void printRetakesForVersion(int versionId, QPrinter& printer);
    void setPrinterResolution(QPrinter& printer);

private:
    Db::RetakeVector _retakes;
    QMultiMap<int, Db::Retake*> _mappedRetakes;

    Db::Database* _database;
    QtPdfium::Library pdf;
    int _pageCount = 0;
};

#endif // RETAKEPRINTJOB_H

#include "classdetail.h"
#include "ui_classdetail.h"

#include "tabmanager.h"

#include "dialogs/enrollstudentdialog.h"
#include "dialogs/learningobjectivedialog.h"
#include "dialogs/retakedialog.h"
#include "dialogs/editclassobjectivesdialog.h"
#include "data/classobjectivedata.h"

#include "widgets/util.h"

#include <QMessageBox>

QTreeWidgetItem *createRetakeListItem(const Db::Retake& retake);

ClassDetail::ClassDetail(int classId, TabManager *tabManager, Db::Database *database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::ClassDetail),
    _database(database),
    _tabManager(tabManager)
{
    ui->setupUi(this);

    setClassId(classId);
}

ClassDetail::~ClassDetail()
{
    delete ui;
}

void ClassDetail::updateData()
{
    ui->StudentList->clear();
    ui->LearningObjectiveList->clear();
    ui->RetakeList->clear();

    Db::Class c;
    if (_database->classes->getClass(_classId, c))
    {
        ui->ClassName->setText(c.name);

        if (!c.active)
        {
            ui->ClassName->setStyleSheet("font-style: italic");
        }

        Db::StudentVector students;
        _database->students->getStudentsInClass(_classId, students);
        Util::addStudentsToList(students, ui->StudentList);


        Db::LearningObjectiveVector learningObjectives;
        _database->learningObjectives->getLearningObjectivesForClass(_classId, learningObjectives);
        Util::addLearningObjectivesToList(learningObjectives, ui->LearningObjectiveList);

        Db::RetakeVector retakes;
        _database->retakes->getRetakesForClass(_classId, retakes);

        Util::addRetakesToList(ui->RetakeList, retakes, Util::DateDisplay::DateAndTime);
    }
}

bool ClassDetail::setClassId(int classId)
{
    _classId = classId;

    updateData();

    return true;
}

QString ClassDetail::getClassName() const
{
    return ui->ClassName->text();
}

void ClassDetail::on_AddStudentButton_clicked()
{
    EnrollStudentDialog *dialog = new EnrollStudentDialog(_classId, _database);
    dialog->setWindowTitle("Enroll a Student");

    //TODO: Ensure student is not already enrolled in the class.
    if (dialog->exec() == QDialog::Accepted)
    {
        int studentId = dialog->getSelectedStudentId();
        _database->classes->enrollStudentInClass(studentId, _classId);

        Db::Student student;
        _database->students->getStudent(studentId, student);

        Util::addStudentToList(student, ui->StudentList);
    }

    dialog->deleteLater();
}

void ClassDetail::on_StudentList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    int studentId = item->data(0, Qt::UserRole).toInt();
    _tabManager->createTabForStudent(studentId);
}

void ClassDetail::on_LearningObjectiveList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    int learningObjectiveId = item->data(0, Qt::UserRole).toInt();
    Db::LearningObjective learningObjective;
    _database->learningObjectives->getLearningObjective(learningObjectiveId, learningObjective);

    LearningObjectiveDialog *dialog = new LearningObjectiveDialog(_database, this);
    dialog->setLearningObjective(learningObjective);

    if (dialog->exec() == QDialog::Accepted)
    {
        dialog->getLearningObjective(learningObjective);
        _database->learningObjectives->updateLearningObjective(learningObjective);
        updateLearningObjectiveInList(learningObjective);
    }

    dialog->deleteLater();
}

void ClassDetail::updateLearningObjectiveInList(const Db::LearningObjective& learningObjective)
{
    for (int i = 0; i < ui->LearningObjectiveList->topLevelItemCount(); ++i)
    {
        QTreeWidgetItem* item = ui->LearningObjectiveList->topLevelItem(i);
        if (item->data(0, Qt::UserRole).toInt() == learningObjective.id)
        {
            item->setText(0, learningObjective.name);
            item->setText(1, learningObjective.description);
            break;
        }
    }
}

void ClassDetail::on_ScheduleRetakeButton_clicked()
{
    RetakeDialog *dialog = new RetakeDialog(_database);
    dialog->newRetakeForClass(_classId);

    if (dialog->exec() == QDialog::Accepted)
    {
        Db::Retake retake;
        dialog->getRetake(retake);
        _database->retakes->createRetake(retake);

        updateData();
    }

    dialog->deleteLater();
}



void ClassDetail::on_pushButton_clicked()
{
    QTreeWidgetItem *item = ui->RetakeList->currentItem();
    if (item && Util::confirmDeleteSelectedItem(this, "Retake"))
    {
        int retakeId = item->data(0, Qt::UserRole).toInt();
        _database->retakes->deleteRetake(retakeId);
        delete item;
    }
}

void ClassDetail::on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    RetakeDialog *dialog = new RetakeDialog(_database);
    dialog->modifyRetakeForStudent(item->data(0, Qt::UserRole).toInt());

    if (dialog->exec() == QDialog::Accepted)
    {
        Db::Retake retake;
        dialog->getRetake(retake);
        if (_database->retakes->updateRetake(retake))
        {
            updateData();
        }
    }

    dialog->deleteLater();
}

void ClassDetail::on_EditObjectivesButton_clicked()
{
    EditLearningObjectiveData classObjectiveData(_database);
    EditClassObjectivesDialog dialog(_classId, &classObjectiveData, _database);

    if (dialog.exec() != QDialog::Accepted) {
        return;
    }

    if (classObjectiveData.hasDeletedObjectives()) {
        QMessageBox msgBox;
        msgBox.setText("You have removed Learning Objectives from this class.");
        msgBox.setInformativeText("Continuing with this operation will delete any retakes associated with the removed learning objectives.");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        int ret = msgBox.exec();

        if (ret == QMessageBox::Cancel) {
            return;
        }
    }

    classObjectiveData.applyChanges();
    updateData();
}

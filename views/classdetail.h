#ifndef CLASSDETAIL_H
#define CLASSDETAIL_H

#include "database/database.h"
#include "idataview.h"

#include <QWidget>
#include <QTreeWidgetItem>

class TabManager;

namespace Ui {
class ClassDetail;
}

class ClassDetail : public IDataView
{
    Q_OBJECT

public:
    explicit ClassDetail(int classId, TabManager *tabManager, Db::Database *db, QWidget *parent = 0);
    ~ClassDetail();

    bool setClassId(int classId);

    QString getClassName() const;
    virtual void updateData();

private:
    void updateLearningObjectiveInList(const Db::LearningObjective& learningObjective);

private slots:
    void on_AddStudentButton_clicked();

    void on_StudentList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_LearningObjectiveList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_ScheduleRetakeButton_clicked();

    void on_pushButton_clicked();

    void on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_EditObjectivesButton_clicked();

private:
    Ui::ClassDetail *ui;
    Db::Database *_database;
    TabManager *_tabManager;
    int _classId;
};

#endif // CLASSDETAIL_H

#include "classeslist.h"
#include "ui_classeslist.h"

#include "tabmanager.h"
#include "widgets/util.h"

#include "data/classdata.h"
#include "dialogs/classdialog.h"

#include <QMessageBox>
#include <QInputDialog>

#include <vector>

ClassesList::ClassesList(TabManager *tabManager, Db::Database *database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::ClassesList),
    _tabManager(tabManager),
    _database(database)
{
    ui->setupUi(this);
}

ClassesList::~ClassesList()
{
    delete ui;
}

void ClassesList::updateData()
{
    ui->ClassList->clear();
    Db::ClassVector classes;
    _database->classes->getClasses(classes, _queryParams);

    for (int i = 0; i < classes.size(); ++i)
    {
        addClassToList(classes[i]);
    }
}

//TODO: refactor me
void ClassesList::addClassToList(const Db::Class c)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    Util::setClassItem(c, item);

    ui->ClassList->addTopLevelItem(item);
}

void ClassesList::on_AddClassButton_clicked()
{
    ClassData classData(_database);
    ClassDialog *dialog = new ClassDialog(&classData, _database);
    dialog->setWindowTitle("Add a new Class");

    if (dialog->exec() == QDialog::Accepted)
    {
        classData.setName(dialog->getClassName());
        auto classId = classData.createClass();

        if (classId > 0) {
            Db::Class c;
            c.id = classId;
            c.name = dialog->getClassName();
            addClassToList(c);
        }
    }

    dialog->deleteLater();
}

void ClassesList::on_ClassList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    int classId = item->data(0, Qt::UserRole).toInt();

    _tabManager->createTabForClass(classId);
}

void ClassesList::on_DeleteClassButton_clicked()
{
    QTreeWidgetItem *item = ui->ClassList->currentItem();

    if (item)
    {
        int classId = item->data(0, Qt::UserRole).toInt();

        QString message = "Are you sure you want to delete this Class?  This operation will also delete any Learning Objectives and Retakes associated with it.";

        if(QMessageBox::question(this, "Confirm Delete", message) == QMessageBox::Yes)
        {

            _database->retakes->deleteRetakesForClass(classId);
            _database->learningObjectives->deleteLearningObjectivesForClass(classId);
            _database->enrollments->deleteEnrollmentsForClass(classId);
            _database->classes->deleteClass(classId);
            updateData();
        }
    }
}

void ClassesList::on_ArchiveClassButton_clicked()
{
    QTreeWidgetItem *item = ui->ClassList->currentItem();

    if (item)
    {
        int classId = item->data(0, Qt::UserRole).toInt();
        Db::Class c;
        _database->classes->getClass(classId, c);

        c.active = !c.active;
        _database->classes->updateClass(c);

        updateData();
    }
}

void ClassesList::on_ClassFilter_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0:
        _queryParams.active = Db::QueryParams::Bool::True;
        break;

    case 1:
        _queryParams.active = Db::QueryParams::Bool::False;
        break;

    case 2:
        _queryParams.active = Db::QueryParams::Bool::Any;
        break;
    }

    updateData();
}

void ClassesList::on_EditClass_clicked()
{
    QTreeWidgetItem *item = ui->ClassList->currentItem();
    if (!item) return;


    QString newName = QInputDialog::getText(this, "Rename " + item->text(0), "Enter a new name:");

    if (newName.isEmpty()) {
        return;
    }

    Db::Class c;
    _database->classes->getClass(item->data(0, Qt::UserRole).toInt(), c);
    c.name = newName;
    _database->classes->updateClass(c);

    item->setText(0, newName);
}

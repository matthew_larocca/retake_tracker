#ifndef CLASSESLIST_H
#define CLASSESLIST_H

#include "database/database.h"
#include "idataview.h"

#include <QWidget>
#include <QTreeWidgetItem>

class TabManager;

namespace Ui {
class ClassesList;
}

class ClassesList : public IDataView
{
    Q_OBJECT

public:
    explicit ClassesList(TabManager *tabManager, Db::Database *database, QWidget *parent = 0);
    ~ClassesList();

public:
    virtual void updateData();

private slots:
    void on_AddClassButton_clicked();

    void on_ClassList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_DeleteClassButton_clicked();

    void on_ArchiveClassButton_clicked();

    void on_ClassFilter_currentIndexChanged(int index);

    void on_EditClass_clicked();

private:
    void addClassToList(const Db::Class c);

private:
    Ui::ClassesList *ui;
    Db::ClassQueryParams _queryParams;

    TabManager *_tabManager;
    Db::Database *_database;
};

#endif // CLASSESLIST_H

#include "home.h"
#include "ui_home.h"

#include "widgets/util.h"

#include "dialogs/retakedialog.h"
#include "util/retakeprintjob.h"

#include <QPrintDialog>
#include <QPrinter>

Home::Home(Db::Database *database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::Home),
    _database(database)
{
    ui->setupUi(this);

    updateData();
}

Home::~Home()
{
    delete ui;
}

void Home::updateData()
{
    QDateTime today = QDateTime(QDate::currentDate(), QTime(0,0));
    QDateTime tomorrow = today.addDays(1);
    QDateTime maxUpcomming = today.addDays(7);

    searchForRetakes(ui->RetakeList, today, tomorrow, Util::DateDisplay::Time);
    searchForRetakes(ui->UpcommingRetakes, tomorrow, maxUpcomming, Util::DateDisplay::DateAndTime);
}

void Home::searchForRetakes(QTreeWidget* treeWidget, const QDateTime& begin, const QDateTime& end, Util::DateDisplay dateDisplay)
{
    treeWidget->clear();

    Db::RetakeVector retakes;
    Db::RetakeQueryParams params;
    params.beginTime = begin;
    params.endTime = end;
    _database->retakes->getAllRetakes(retakes, params);
    Util::addRetakesToList(treeWidget, retakes, dateDisplay);
}


void Home::on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    onItemDoubleClicked(item);
}

void Home::on_UpcommingRetakes_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    onItemDoubleClicked(item);
}

void Home::onItemDoubleClicked(QTreeWidgetItem *item)
{
    RetakeDialog *dialog = new RetakeDialog(_database);
    dialog->modifyRetakeForStudent(item->data(0, Qt::UserRole).toInt());

    if (dialog->exec() == QDialog::Accepted)
    {
        Db::Retake retake;
        dialog->getRetake(retake);
        if (_database->retakes->updateRetake(retake))
        {
            updateData();
        }
    }

    dialog->deleteLater();
}

void Home::printRetakeList(const QList<QTreeWidgetItem*> items)
{
    if (items.size() == 0) return;

    QPrinter printer;
    QPrintDialog dialog(&printer, nullptr);

    if (dialog.exec() != QDialog::Accepted) return;

    QVector<int> retakeIds;

    for (auto& item : items) {
        retakeIds.push_back(item->data(0, Qt::UserRole).toInt());
    }

    Db::RetakeVector retakes;
    _database->retakes->getRetakes(retakeIds, retakes);

    RetakePrintJob printJob(_database);
    printJob.init(retakes);
    printJob.print(printer);

}

void Home::on_PrintTodayButton_clicked()
{
    auto selectedItems = ui->RetakeList->selectedItems();
    printRetakeList(selectedItems);
}

void Home::on_PrintSelectedButton_clicked()
{
    auto selectedItems = ui->UpcommingRetakes->selectedItems();
    printRetakeList(selectedItems);
}

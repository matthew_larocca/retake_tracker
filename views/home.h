#ifndef HOME_H
#define HOME_H

#include "database/database.h"
#include "idataview.h"

#include "widgets/util.h"

#include <QWidget>
#include <QDate>
#include <QTreeWidgetItem>

namespace Ui {
class Home;
}

class Home : public IDataView
{
    Q_OBJECT

public:
    explicit Home(Db::Database *database, QWidget *parent = 0);
    ~Home();

public:
    virtual void updateData();

private slots:
    void on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_UpcommingRetakes_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_PrintTodayButton_clicked();

    void on_PrintSelectedButton_clicked();

private:
    void searchForRetakes(QTreeWidget* treeWidget, const QDateTime& begin, const QDateTime& end, Util::DateDisplay dateDisplay);

    void onItemDoubleClicked(QTreeWidgetItem *item);
    void printRetakeList(const QList<QTreeWidgetItem*> items);

private:
    Ui::Home *ui;
    Db::Database *_database;
};

#endif // HOME_H

#ifndef IDATAVIEW_H
#define IDATAVIEW_H

#include <QWidget>

class IDataView : public QWidget
{
public:
    IDataView(QWidget *parent = 0) : QWidget(parent){}
    virtual void updateData() = 0;
};

#endif // IDATAVIEW_H

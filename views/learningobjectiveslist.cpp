#include "learningobjectiveslist.h"
#include "ui_learningobjectiveslist.h"

#include "widgets/util.h"
#include "views/tabmanager.h"
#include "annotation/annotationsource.h"
#include "dialogs/learningobjectivedialog.h"
#include "dialogs/retakeversiondialog.h"

#include <QInputDialog>
#include <QMessageBox>

LearningObjectivesList::LearningObjectivesList(TabManager *tabManager, Db::Database* database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::LearningObjectivesList),
    _database(database),
    _tabManager(tabManager)
{
    ui->setupUi(this);
    initCategoryList(true);
}

LearningObjectivesList::~LearningObjectivesList()
{
    delete ui;
}

void LearningObjectivesList::initCategoryList(bool selectInitial)
{
    Db::LearningObjectiveCategoryVector categories;
    _database->learningObjectiveCategories->getCategories(categories);

    Util::addLearningObjectiveCategoriesToList(categories, ui->CategoryList);

    if (categories.size() > 0 && selectInitial) {
        ui->CategoryList->setCurrentRow(0);
    }
}

void LearningObjectivesList::addLearningObjectiveToList(const Db::LearningObjective& learningObjective)
{
    auto item = new QTreeWidgetItem{};
    item->setText(0, learningObjective.name);
    item->setData(0, Qt::UserRole, learningObjective.id);

    ui->LearningObjectiveList->addTopLevelItem(item);
}

void LearningObjectivesList::updateObjectiveList(int categoryId)
{
    ui->LearningObjectiveList->clear();
    ui->RetakeVersionList->clear();

    Db::LearningObjectiveVector learningObjectives;
    _database->learningObjectives->getLearningObjectivesInCategory(categoryId, learningObjectives);

    for (auto& learningObjective : learningObjectives) {
        addLearningObjectiveToList(learningObjective);
    }
}

void LearningObjectivesList::updateRetakeVersionList(int learningObjectiveId)
{
    ui->RetakeVersionList->clear();
    Db::RetakeVersionVector retakeVersions;
    _database->retakeVersions->getVersionsForLearningObjective(learningObjectiveId, retakeVersions);

    for (auto& retakeVersion : retakeVersions) {
        addRetakeVersionToList(retakeVersion);
    }
}

void LearningObjectivesList::addRetakeVersionToList(const Db::RetakeVersion& retakeVersion)
{
    auto item = new QTreeWidgetItem();

    item->setText(0,retakeVersion.name);
    item->setData(0, Qt::UserRole, retakeVersion.id);
    item->setText(1, retakeVersion.description);

    ui->RetakeVersionList->addTopLevelItem(item);
}

void LearningObjectivesList::updateData()
{
    const auto currentCategoryItem = ui->CategoryList->currentItem();
    const auto currentCategoryText = currentCategoryItem ? currentCategoryItem->text() : "";

    const auto currentLearningObjectiveItem = ui->LearningObjectiveList->currentItem();
    const auto currentLearningObjectiveText = currentLearningObjectiveItem ? currentLearningObjectiveItem->text(0) : "";

    const auto retakeVersionItem = ui->RetakeVersionList->currentItem();
    const auto retakeVersionText = retakeVersionItem ? retakeVersionItem->text(0) : "";

    ui->CategoryList->clear();
    ui->LearningObjectiveList->clear();
    ui->RetakeVersionList->clear();

    initCategoryList(false);

    if (ui->CategoryList->count() > 0 && !currentCategoryText.isEmpty()) {
        listWidgetSelectByText(ui->CategoryList, currentCategoryText);
    }

    if (ui->LearningObjectiveList->topLevelItemCount() > 0 && !currentLearningObjectiveText.isEmpty()) {
        treeWidgetSelectByText(ui->LearningObjectiveList, currentLearningObjectiveText);
    }

    if (ui->RetakeVersionList->topLevelItemCount() > 0 && !retakeVersionText.isEmpty()) {
        treeWidgetSelectByText(ui->RetakeVersionList, retakeVersionText);
    }
}

void LearningObjectivesList::listWidgetSelectByText(QListWidget* listWidget, const QString& text)
{
    for (int i = 0; i < listWidget->count(); i++) {
        if (listWidget->item(i)->text() == text) {
            listWidget->setCurrentRow(i);
            break;
        }
    }
}

void LearningObjectivesList::treeWidgetSelectByText(QTreeWidget* treeWidget, const QString& text)
{
    for (int i = 0; i < treeWidget->topLevelItemCount(); i++) {
        auto item = treeWidget->topLevelItem(i);

        if (item->text(0) == text) {
            treeWidget->setCurrentItem(item);
            return;
        }
    }
}

void LearningObjectivesList::on_NewCategoryButton_clicked()
{
    auto response = QInputDialog::getText(this, "Create a new Category", "Category name:");

    if (!response.isEmpty()) {
        Db::LearningObjectiveCategory category;
        category.name = response;

        _database->learningObjectiveCategories->createCateogry(category);
        Util::addCategoryToList(category, ui->CategoryList);
    }
}

void LearningObjectivesList::on_NewLearningObjectiveButton_clicked()
{
    auto currentCategoryItem = ui->CategoryList->currentItem();
    if (!currentCategoryItem) return;

    LearningObjectiveDialog dialog(_database);
    dialog.setCategoryText(currentCategoryItem->text());

    if (dialog.exec() == QDialog::Accepted) {
        Db::LearningObjective learningObjective;
        dialog.getLearningObjective(learningObjective);
        learningObjective.categoryId = currentCategoryItem->data(Qt::UserRole).toInt();
        _database->learningObjectives->createLearningObjective(learningObjective);
        addLearningObjectiveToList(learningObjective);
    }
}

void LearningObjectivesList::on_CategoryList_currentRowChanged(int currentRow)
{
    if (currentRow != -1) {
        auto categoryId = ui->CategoryList->currentItem()->data(Qt::UserRole).toInt();
        updateObjectiveList(categoryId);
    }
}

void LearningObjectivesList::on_NewRetakeVersionButton_clicked()
{
    auto currentObjectiveItem = ui->LearningObjectiveList->currentItem();
    if (!currentObjectiveItem) return;

    AnnotationSource annotationSource(_database);
    RetakeVersionDialog dialog(_database, &annotationSource);
    if (dialog.exec() == QDialog::Accepted) {
        Db::RetakeVersion retakeVersion;
        retakeVersion.learning_objective_id = currentObjectiveItem->data(0, Qt::UserRole).toInt();
        retakeVersion.file_id = dialog.updateFile();
        dialog.getRetakeVersion(retakeVersion);
        _database->retakeVersions->createRetakeVersion(retakeVersion);
        annotationSource.apply(retakeVersion.file_id);

        addRetakeVersionToList(retakeVersion);
    }
}

void LearningObjectivesList::on_LearningObjectiveList_itemSelectionChanged()
{
    auto currentItem = ui->LearningObjectiveList->currentItem();
    if (!currentItem) return;

    updateRetakeVersionList(currentItem->data(0,Qt::UserRole).toInt());
}

void LearningObjectivesList::on_RetakeVersionList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    auto retakeVersionId = item->data(0,Qt::UserRole).toInt();
    Db::RetakeVersion retakeVersion;
    _database->retakeVersions->getRetakeVersion(retakeVersionId, retakeVersion);

    AnnotationSource annotationSource(_database);
    RetakeVersionDialog dialog(_database, &annotationSource);
    dialog.setRetakeVersion(retakeVersion);

    if (dialog.exec() == QDialog::Accepted) {
        retakeVersion.file_id = dialog.updateFile();
        dialog.getRetakeVersion(retakeVersion);
        _database->retakeVersions->updateRetakeVersion(retakeVersion);
        annotationSource.apply(retakeVersion.file_id);

        item->setText(0, retakeVersion.name);
        item->setText(1, retakeVersion.description);
    }
}


void LearningObjectivesList::on_CategoryList_itemDoubleClicked(QListWidgetItem *item)
{
    QString newName = QInputDialog::getText(this, "Rename " + item->text(), "Enter a new name:");

    if (newName.isEmpty()) {
        return;
    }

    Db::LearningObjectiveCategory category;
    category.id = item->data(Qt::UserRole).toInt();
    category.name = newName;
    _database->learningObjectiveCategories->updateCategory(category);

    item->setText(newName);
}

void LearningObjectivesList::on_DeleteObjectiveButton_clicked()
{
    auto currentItem = ui->LearningObjectiveList->currentItem();

    if (currentItem == nullptr) {
        return;
    }

    QMessageBox messageBox;
    messageBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    messageBox.setText("Are you sure you want to delete this learning objective? "
                       "This operation will also delete any associated versions and retakes. "
                       "This operation cannot be undone.");

    if (messageBox.exec() != QMessageBox::Ok) {
        return;
    }

    auto learningObjectiveId = currentItem->data(0, Qt::UserRole).toInt();

    //delete all retakes associated with this learning objective
    _database->retakes->deleteRetakesForLearningObjective(learningObjectiveId);

    //delete all files associated with these learning objectives
    QVector<int> fileIds;
    _database->retakeVersions->getVersionFileIdsForLearningObjective(learningObjectiveId, fileIds);
    _database->files->deleteFiles(fileIds);

    //delete all annotations associated with deleted files
    _database->annotations->deleteAnnotationsForFiles(fileIds);

    //delete all retake versions of this learning objective id
    _database->retakeVersions->deleteRetakeVersionsForLearningObjective(learningObjectiveId);

    //delete all class objectives for this learning objective id
    _database->classObjectives->deleteClassObjectivesForLearningObjective(learningObjectiveId);

    //delete the learning objective entry
    _database->learningObjectives->deleteLearningObjective(learningObjectiveId);
    delete currentItem;
}

#ifndef LEARNINGOBJECTIVESLIST_H
#define LEARNINGOBJECTIVESLIST_H

#include "database/database.h"
#include "idataview.h"

#include <QWidget>
#include <QTreeWidget>
#include <QListWidget>
#include <QTreeWidgetItem>
#include <QListWidgetItem>

class TabManager;

namespace Ui {
class LearningObjectivesList;
}

class LearningObjectivesList : public IDataView
{
    Q_OBJECT

public:
    explicit LearningObjectivesList(TabManager *tabManager, Db::Database* database, QWidget *parent = nullptr);
    ~LearningObjectivesList();

public:
    virtual void updateData();

private slots:

    void on_NewCategoryButton_clicked();

    void on_NewLearningObjectiveButton_clicked();

    void on_CategoryList_currentRowChanged(int currentRow);

    void on_NewRetakeVersionButton_clicked();

    void on_LearningObjectiveList_itemSelectionChanged();

    void on_RetakeVersionList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_CategoryList_itemDoubleClicked(QListWidgetItem *item);

    void on_DeleteObjectiveButton_clicked();

private:
    void initCategoryList(bool selectInitial);
    void updateObjectiveList(int categoryId);
    void updateRetakeVersionList(int learningObjectiveId);

    void addLearningObjectiveToList(const Db::LearningObjective& learningObjective);
    void addRetakeVersionToList(const Db::RetakeVersion& retakeVersion);

    void listWidgetSelectByText(QListWidget* listWidget, const QString& text);
    void treeWidgetSelectByText(QTreeWidget* treeWidget, const QString& text);

private:
    Ui::LearningObjectivesList *ui;

    Db::Database* _database;
    TabManager *_tabManager;
};

#endif // LEARNINGOBJECTIVESLIST_H

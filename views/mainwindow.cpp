#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "import/retakeimporter.h"
#include "import/learningobjectiveimport.h"
#include "export/learningobjectiveexport.h"

#include "dialogs/retakeimportdialog.h"
#include "dialogs/learningobjectiveexportdialog.h"
#include "dialogs/learningobjectiveimportdialog.h"

#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QCoreApplication::setApplicationName("Retake Tracker");

    setWindowTitle("Retake Tracker");

    _database = new Db::Database();
    _database->init();

    _tabManager = new TabManager(_database, ui->MainTabs);

    ui->MainTabs->setCurrentIndex(0);
}

MainWindow::~MainWindow()
{
    delete ui;

    delete _database;
    delete _tabManager;
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_MainTabs_tabCloseRequested(int index)
{
    _tabManager->removeTab(index);
}

void MainWindow::on_actionRetakes_triggered()
{
    RetakeImporter importer(_database);
    RetakeImportDialog dialog(importer);

    if (dialog.exec() == QDialog::Accepted) {
        importer.createRetakes();
        _tabManager->refreshCurrentTab();
    }
}

void MainWindow::on_actionLearning_Objectives_triggered()
{
    LearningObjectiveExportDialog dialog(_database);
    auto result = dialog.exec();
    if (result != QDialog::Accepted) {
        return;
    }

    auto exportFile = QFileDialog::getSaveFileName(
                nullptr,
                "Export Learning Objectives",
                "learning_objectives.data",
                "Data (*.data)");

    if (exportFile.isEmpty()) {
        return;
    }

    const auto learningObjectives = dialog.getLearningObjectives();
    LearningObjectiveExport exporter(_database);
    exporter.setLearningObjectives(learningObjectives);
    exporter.exportData(exportFile);
}

void MainWindow::on_actionLearning_Objectives_2_triggered()
{
    LearningObjectiveImportDialog dialog(_database);
    const auto result = dialog.exec();
    if (result != QDialog::Accepted) {
        return;
    }

    int categoryId = 0;
    if (dialog.isNewCategory()) {
        Db::LearningObjectiveCategory category;
        category.name = dialog.newCategoryText();
        _database->learningObjectiveCategories->createCateogry(category);
        categoryId = category.id;
    }
    else {
        categoryId = dialog.existingCategoryId();
    }

    LearningObjectiveImport importer(_database);
    importer.importPack(categoryId, dialog.filePath());

    _tabManager->refreshCurrentTab();
}

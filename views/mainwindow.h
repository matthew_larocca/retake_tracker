#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "database/database.h"
#include "tabmanager.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();

    void on_MainTabs_tabCloseRequested(int index);

    void on_actionRetakes_triggered();

    void on_actionLearning_Objectives_triggered();

    void on_actionLearning_Objectives_2_triggered();

private:
    Ui::MainWindow *ui;
    Db::Database *_database;
    TabManager *_tabManager;
};

#endif // MAINWINDOW_H

#include "retakeslist.h"
#include "ui_retakeslist.h"

#include "widgets/util.h"

#include "dialogs/retakedialog.h"
#include "dialogs/retakefilterdialog.h"

#include "util/retakeprintjob.h"

#include <QPrintDialog>
#include <QPrinter>

RetakesList::RetakesList(TabManager *tabManager, Db::Database *database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::RetakesList),
    _database(database),
    _tabManager(tabManager)
{
    ui->setupUi(this);
    ui->CustomFilterButton->hide();

    _queryParams.orderBy = Db::RetakeQueryParams::OrderBy::CreatedDate;
    _queryParams.orderDirection = Db::RetakeQueryParams::OrderDirection::Descending;
    _queryParams.limit = 100;

    _customQueryParams.beginTime = QDateTime(QDate::currentDate(), QTime(0,0));
    _customQueryParams.endTime = _customQueryParams.beginTime.addDays(1);
    _customQueryParams.limit = 100;
}

RetakesList::~RetakesList()
{
    delete ui;
}


void RetakesList::updateData()
{
    ui->RetakeList->clear();

    Db::RetakeVector retakes;
    getAllRetakes(retakes);
    Util::addRetakesToList(ui->RetakeList, retakes, Util::DateDisplay::DateAndTime);
}

void RetakesList::getAllRetakes(Db::RetakeVector& retakes)
{
    if (ui->FilterBox->currentIndex() == 3 /* Custom Filter */)
    {
        _database->retakes->getAllRetakes(retakes, _customQueryParams);
    }
    else
    {
        _database->retakes->getAllRetakes(retakes, _queryParams);
    }
}

void RetakesList::on_FilterBox_currentIndexChanged(int index)
{
    _queryParams.reset();

    switch (index)
    {
    case 0: /* All */
        _queryParams.reset();
        ui->CustomFilterButton->hide();
        break;

    case 1: /* Needs Reschedule */
    {
        _queryParams.reset();
        QDateTime today = QDateTime(QDate::currentDate(), QTime(0,0));
        _queryParams.endTime = today;
        _queryParams.completed = Db::RetakeQueryParams::Bool::False;
        ui->CustomFilterButton->hide();
        break;
    }

    case 2: /* Custom Filter */
        ui->CustomFilterButton->show();
        showFilterDialog();
        break;
    }

    updateData();
}

void RetakesList::on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    RetakeDialog *dialog = new RetakeDialog(_database);
    dialog->modifyRetakeForStudent(item->data(0, Qt::UserRole).toInt());

    if (dialog->exec() == QDialog::Accepted)
    {
        Db::Retake retake;
        dialog->getRetake(retake);
        if (_database->retakes->updateRetake(retake))
        {
            updateData();
        }
    }

    dialog->deleteLater();
}

void RetakesList::showFilterDialog()
{
    RetakeFilterDialog* retakeFilterDialog = new RetakeFilterDialog();
    retakeFilterDialog->initWithParams(_customQueryParams);

    if (retakeFilterDialog->exec() == QDialog::Accepted)
    {
        retakeFilterDialog->applyParams(_customQueryParams);
        updateData();
    }

    retakeFilterDialog->deleteLater();
}

void RetakesList::on_CustomFilterButton_clicked()
{
    showFilterDialog();
}

void RetakesList::on_PrintButton_clicked()
{
    QPrinter printer;

    QPrintDialog dialog(&printer, nullptr);

    if (dialog.exec() == QDialog::Accepted) {
        Db::RetakeVector retakes;
        getAllRetakes(retakes);

        RetakePrintJob printJob(_database);
        printJob.init(retakes);
        printJob.print(printer);
    }
}

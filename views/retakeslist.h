#ifndef RETAKESLIST_H
#define RETAKESLIST_H

#include "database/database.h"
#include "models/retake.h"

#include "idataview.h"
#include "tabmanager.h"

#include <QWidget>
#include <QTreeWidgetItem>


namespace Ui {
class RetakesList;
}

class RetakesList : public IDataView
{
    Q_OBJECT

public:
    explicit RetakesList(TabManager *tabManager, Db::Database *database, QWidget *parent = 0);
    ~RetakesList();

public:
    virtual void updateData();

private:
    void showFilterDialog();
    void getAllRetakes(Db::RetakeVector& retakes);

private slots:
    void on_FilterBox_currentIndexChanged(int index);

    void on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_CustomFilterButton_clicked();

    void on_PrintButton_clicked();

private:
    Ui::RetakesList *ui;
    Db::Database *_database;
    TabManager *_tabManager;
    Db::RetakeQueryParams _queryParams;
    Db::RetakeQueryParams _customQueryParams;
};

#endif // RETAKESLIST_H

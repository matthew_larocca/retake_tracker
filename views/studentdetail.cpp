#include "studentdetail.h"
#include "ui_studentdetail.h"

#include "widgets/util.h"

#include "dialogs/retakedialog.h"

StudentDetail::StudentDetail(int studentId, Db::Database* database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::StudentDetail),
    _database(database)
{
    ui->setupUi(this);

    setStudentId(studentId);

}

StudentDetail::~StudentDetail()
{
    delete ui;
}

bool StudentDetail::setStudentId(int studentId)
{
    _studentId = studentId;

    updateData();

    return true;
}

void StudentDetail::updateData()
{
    ui->RetakeList->clear();

    Db::Student student;
    _database->students->getStudent(_studentId, student);
    ui->StudentName->setText(student.firstName + " " + student.lastName);
    ui->StudentEmail->setText(student.email);

    Db::RetakeVector retakes;
    Db::RetakeQueryParams params;
    params.orderDirection = Db::RetakeQueryParams::OrderDirection::Descending;
    params.studentIds.push_back(_studentId);
    _database->retakes->getAllRetakes(retakes, params);
    Util::addRetakesToList(ui->RetakeList, retakes, Util::DateDisplay::DateAndTime);
}

QString StudentDetail::getStudentName() const
{
    return ui->StudentName->text();
}

void StudentDetail::on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    RetakeDialog *dialog = new RetakeDialog(_database);
    dialog->modifyRetakeForStudent(item->data(0, Qt::UserRole).toInt());

    if (dialog->exec() == QDialog::Accepted)
    {
        Db::Retake retake;
        dialog->getRetake(retake);
        if (_database->retakes->updateRetake(retake))
        {
            updateData();
        }
    }

    dialog->deleteLater();
}

#include <QMessageBox>

void StudentDetail::on_AddRetakeButton_clicked()
{
    RetakeDialog *dialog = new RetakeDialog(_database);
    dialog->newRetakeForStudent(_studentId);

    if (dialog->exec() == QDialog::Accepted)
    {
        Db::Retake retake;
        dialog->getRetake(retake);

        if (_database->retakes->createRetake(retake))
        {
            updateData();
        }
    }

    dialog->deleteLater();
}

void StudentDetail::on_DeleteRetakeButton_clicked()
{
    QTreeWidgetItem *item = ui->RetakeList->currentItem();
    if (item && Util::confirmDeleteSelectedItem(this, "Retake"))
    {
        int retakeId = item->data(0, Qt::UserRole).toInt();
        _database->retakes->deleteRetake(retakeId);
        delete item;
    }

    updateData();
}

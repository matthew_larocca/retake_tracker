#ifndef STUDENTDETAIL_H
#define STUDENTDETAIL_H

#include "database/database.h"
#include "idataview.h"

#include <QWidget>
#include <QTreeWidgetItem>

namespace Ui {
class StudentDetail;
}

class StudentDetail : public IDataView
{
    Q_OBJECT

public:
    explicit StudentDetail(int studentId, Db::Database* database, QWidget *parent = 0);
    ~StudentDetail();

public:
    bool setStudentId(int studentId);
    virtual void updateData();

    QString getStudentName() const;


private slots:
    void on_RetakeList_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_AddRetakeButton_clicked();

    void on_DeleteRetakeButton_clicked();

private:
    Ui::StudentDetail *ui;
    Db::Database* _database;
    int _studentId;
};

#endif // STUDENTDETAIL_H

#include "studentslist.h"
#include "ui_studentslist.h"

#include "tabmanager.h"

#include "widgets/util.h"
#include "dialogs/studentdialog.h"

StudentsList::StudentsList(TabManager *tabManager, Db::Database* database, QWidget *parent) :
    IDataView(parent),
    ui(new Ui::StudentsList),
    _database(database),
    _tabManager(tabManager)
{
    ui->setupUi(this);
}

StudentsList::~StudentsList()
{
    delete ui;
}

void StudentsList::updateData()
{
    ui->StudentList->clear();

    Db::StudentVector students;
    _database->students->getAllStudents(students, _queryParams);

    Util::addStudentsToList(students, ui->StudentList);
}

void StudentsList::on_AddStudentButton_clicked()
{
    StudentDialog *dialog = new StudentDialog();
    dialog->setWindowTitle("Add a new Student");

    if( dialog->exec() == QDialog::Accepted)
    {
        Db::Student student;
        dialog->getStudent(student);
        student.active = true;
        bool result = _database->students->createStudent(student);

        if (result)
        {
            updateData();
        }
    }

    dialog->deleteLater();
}

void StudentsList::on_StudentList_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);

    int studentId = item->data(0, Qt::UserRole).toInt();

    _tabManager->createTabForStudent(studentId);
}

void StudentsList::on_pushButton_clicked()
{
    QTreeWidgetItem *item = ui->StudentList->currentItem();

    if (item)
    {
        Db::Student student;
        _database->students->getStudent(item->data(0, Qt::UserRole).toInt(), student);

        StudentDialog *dialog = new StudentDialog();
        dialog->setStudent(student);
        dialog->setWindowTitle("Edit Student");

        if( dialog->exec() == QDialog::Accepted)
        {
            dialog->getStudent(student);
            _database->students->updateStudent(student);
            Util::setStudentItem(student, item);
        }
    }
}


void StudentsList::on_ArchiveStudentButton_clicked()
{
    QTreeWidgetItem* item = ui->StudentList->currentItem();

    if (item)
    {
        int studentId = item->data(0, Qt::UserRole).toInt();
        Db::Student student;
        _database->students->getStudent(studentId, student);

        student.active = !student.active;
        _database->students->updateStudent(student);

        updateData();
    }
}

void StudentsList::on_FilterBox_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0:
        _queryParams.active = Db::QueryParams::Bool::True;
        break;

    case 1:
        _queryParams.active = Db::QueryParams::Bool::False;
        break;

    case 2:
        _queryParams.active = Db::QueryParams::Bool::Any;
        break;
    }

    updateData();
}

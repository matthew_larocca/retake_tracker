#ifndef STUDENTSLIST_H
#define STUDENTSLIST_H

#include "database/database.h"
#include "idataview.h"

#include <QWidget>
#include <QTreeWidgetItem>

class TabManager;

namespace Ui {
class StudentsList;
}

class StudentsList : public IDataView
{
    Q_OBJECT

public:
    explicit StudentsList(TabManager *tabManager, Db::Database *database, QWidget *parent = 0);
    ~StudentsList();

public:
    virtual void updateData();

private slots:
    void on_AddStudentButton_clicked();

    void on_StudentList_itemDoubleClicked(QTreeWidgetItem *item, int column);


    void on_pushButton_clicked();

    void on_ArchiveStudentButton_clicked();

    void on_FilterBox_currentIndexChanged(int index);

private:
    Ui::StudentsList *ui;
    Db::Database *_database;
    TabManager *_tabManager;
    Db::StudentQueryParams _queryParams;
};

#endif // STUDENTSLIST_H

#include "tabmanager.h"

#include "views/studentdetail.h"
#include "views/classdetail.h"

#include "views/home.h"
#include "views/studentslist.h"
#include "views/classeslist.h"
#include "views/retakeslist.h"
#include "views/learningobjectiveslist.h"

#include <QPushButton>
#include <QVariant>

#include "idataview.h"

enum TabType
{
    LIST, STUDENT, CLASS
};

#ifdef __APPLE__
    #define TabButtonPosition QTabBar::LeftSide
#else
    #define TabButtonPosition QTabBar::RightSide
#endif

TabManager::TabManager(Db::Database* database, QTabWidget *tabWidget) :
    _tabWidget(tabWidget),
    _database(database),
    _skipNextUpdate(false)
{
    QObject::connect(_tabWidget, &QTabWidget::currentChanged, [&](int index){
        this->onTabChange(index);
    });

    Home *home = new Home(_database, _tabWidget);
    StudentsList *studentsList = new StudentsList(this, _database, _tabWidget);
    ClassesList *classesList = new ClassesList(this, _database, _tabWidget);
    RetakesList *retakesList = new RetakesList(this, _database, _tabWidget);
    LearningObjectivesList *learningObjectivesList = new LearningObjectivesList(this, _database, _tabWidget);

    _tabWidget->addTab(home, "Home");
    _tabWidget->addTab(studentsList, "Students");
    _tabWidget->addTab(classesList, "Classes");
    _tabWidget->addTab(learningObjectivesList, "Learning Objectives");
    _tabWidget->addTab(retakesList, "Retakes");

    // this prevents the user from being able to close the standard tabs
    _tabWidget->tabBar()->tabButton(0, TabButtonPosition)->resize(0,0);
    _tabWidget->tabBar()->tabButton(1, TabButtonPosition)->resize(0,0);
    _tabWidget->tabBar()->tabButton(2, TabButtonPosition)->resize(0,0);
    _tabWidget->tabBar()->tabButton(3, TabButtonPosition)->resize(0,0);
    _tabWidget->tabBar()->tabButton(4, TabButtonPosition)->resize(0,0);
}

void TabManager::createTabForStudent(int studentId)
{
    auto tab = _studentTabs.find(studentId);
    if (tab == _studentTabs.end())
    {
        StudentDetail *studentDetail = new StudentDetail(studentId, _database);
        studentDetail->setProperty("type", (int)STUDENT);
        studentDetail->setProperty("id", studentId);

        QString tabName = "Student: " + studentDetail->getStudentName();
        _tabWidget->addTab(studentDetail, tabName);
        _studentTabs[studentId] = studentDetail;

        _skipNextUpdate = true;
        _tabWidget->setCurrentIndex(_tabWidget->count() - 1);
    }
    else
    {
        QString tabName = "Student: " + tab.value()->getStudentName();
        setActiveTabByName(tabName);
    }
}

void TabManager::createTabForClass(int classId)
{
    auto tab = _classTabs.find(classId);

    if (tab == _classTabs.end())
    {
        ClassDetail *classDetail = new ClassDetail(classId, this, _database);
        classDetail->setProperty("type", (int)CLASS);
        classDetail->setProperty("id", classId);

        QString tabName = "Class: " + classDetail->getClassName();
        _tabWidget->addTab(classDetail, tabName);
        _classTabs[classId] = classDetail;

        _skipNextUpdate = true;
        _tabWidget->setCurrentIndex(_tabWidget->count() - 1);
    }
    else
    {
        QString tabName = "Class: " + tab.value()->getClassName();
        setActiveTabByName(tabName);
    }
}

void TabManager::removeTab(int index)
{
    QWidget *widget = _tabWidget->widget(index);
    int tabType = widget->property("type").toInt();
    int id = widget->property("id").toInt();

    switch(tabType)
    {
        case STUDENT:
        _studentTabs.remove(id);
        break;

    case CLASS:
        _classTabs.remove(id);
        break;
    };

    _tabWidget->removeTab(index);
    widget->deleteLater();
}

void TabManager::refreshCurrentTab()
{
    IDataView * dataView = dynamic_cast<IDataView*>(_tabWidget->widget(_tabWidget->currentIndex()));
    dataView->updateData();
}

void TabManager::setActiveTabByName(const QString& name)
{
    int tabCount = _tabWidget->count();

    for (int i = 0; i < tabCount; ++i)
    {
        QString tabText = _tabWidget->tabText(i);
        if(_tabWidget->tabText(i) == name)
        {
            _tabWidget->setCurrentIndex(i);
            return;
        }
    }
}

void TabManager::onTabChange(int index)
{
    IDataView * dataView = dynamic_cast<IDataView*>(_tabWidget->widget(index));

    if (dataView && !_skipNextUpdate)
    {
        dataView->updateData();
    }

    _skipNextUpdate = false;
}


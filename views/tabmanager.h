#ifndef TABMANAGER_H
#define TABMANAGER_H

#include "database/database.h"

#include <QTabWidget>
#include <QMap>

class StudentDetail;
class ClassDetail;

class TabManager
{
public:
    TabManager(Db::Database* database, QTabWidget *tabWidget);

    void removeTab(int index);
    void refreshCurrentTab();

    void createTabForStudent(int studentId);
    void createTabForClass(int classId);

private:
    void setActiveTabByName(const QString& name);

    void onTabChange(int index);

private:
    typedef QMap<int, StudentDetail*> StudentDetailMap;
    typedef QMap<int, ClassDetail*> ClassDetailMap;

    StudentDetailMap _studentTabs;
    ClassDetailMap _classTabs;

private:
    QTabWidget *_tabWidget;
    Db::Database* _database;

    bool _skipNextUpdate;

};

#endif // TABMANAGER_H

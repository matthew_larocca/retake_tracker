#include "classrosterimport.h"
#include "ui_classrosterimport.h"

#include "import/studentimporter.h"
#include "widgets/util.h"

#include <QFileDialog>

ClassRosterImport::ClassRosterImport(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClassRosterImport)
{
    ui->setupUi(this);
}

ClassRosterImport::~ClassRosterImport()
{
    delete ui;
}

void ClassRosterImport::on_RosterFileButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
    dialog.setNameFilter("Text Documents (*.txt)");

    if (dialog.exec() == QDialog::Accepted) {
        QString selectedFile = dialog.selectedFiles()[0];
        ui->RosterFilePath->setText(selectedFile);

        StudentImporter importer(nullptr);
        importer.parseClassRoster(selectedFile);
        Util::addStudentsToList(importer.students(), ui->RosterList);
        _classData->addStudents(importer.students());
    }
}

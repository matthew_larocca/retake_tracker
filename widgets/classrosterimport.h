#ifndef CLASSROSTERIMPORT_H
#define CLASSROSTERIMPORT_H

#include "data/classdata.h"

#include <QWidget>

namespace Ui {
class ClassRosterImport;
}

class ClassRosterImport : public QWidget
{
    Q_OBJECT

public:
    explicit ClassRosterImport(QWidget *parent = nullptr);
    ~ClassRosterImport();

public:
    void init(ClassData* classData) { _classData = classData; }

private slots:
    void on_RosterFileButton_clicked();

private:
    Ui::ClassRosterImport *ui;
    ClassData* _classData = nullptr;
};

#endif // CLASSROSTERIMPORT_H

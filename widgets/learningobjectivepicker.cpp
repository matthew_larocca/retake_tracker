#include "learningobjectivepicker.h"
#include "ui_learningobjectivepicker.h"

#include "widgets/util.h"

#include <QVariant>

LearningObjectivePicker::LearningObjectivePicker(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LearningObjectivePicker)
{
    ui->setupUi(this);
}

LearningObjectivePicker::~LearningObjectivePicker()
{
    delete ui;
}

void LearningObjectivePicker::init(ILearningObjectiveData* classData, Db::Database* database)
{
    _classData = classData;
    _database = database;

    Db::LearningObjectiveCategoryVector categories;
    _database->learningObjectiveCategories->getCategories(categories);

    for (auto& category : categories) {
        ui->CategoryPicker->addItem(category.name, category.id);
    }
}

void LearningObjectivePicker::setSelectedObjectives(const Db::LearningObjectiveVector& learningObjectives)
{
    for (auto& learningObjective : learningObjectives) {
        auto listItem = new QListWidgetItem{};

        listItem->setText(learningObjective.name);
        listItem->setData(Qt::UserRole, learningObjective.id);

        ui->SelectedLearningObjectives->addItem(listItem);
    }
}

void LearningObjectivePicker::setCurrentLearningObjectiveList(int categoryId)
{
    ui->CurrentLearningObjectives->clear();

    Db::LearningObjectiveVector learningObjectives;
    _database->learningObjectives->getLearningObjectivesInCategory(categoryId, learningObjectives);

    for (auto& learningObjective : learningObjectives) {
        auto item = new QListWidgetItem{};

        item->setText(learningObjective.name);
        item->setData(Qt::UserRole, learningObjective.id);

        ui->CurrentLearningObjectives->addItem(item);
    }
}

void LearningObjectivePicker::on_CategoryPicker_currentIndexChanged(int index)
{
    if (index < 0) return;

    const auto categoryId = ui->CategoryPicker->currentData().toInt();
    setCurrentLearningObjectiveList(categoryId);
}

void LearningObjectivePicker::addItemToSelectedObjectives(QListWidgetItem* item)
{
    auto learningObjectiveId = item->data(Qt::UserRole).toInt();
    if (_classData->addLearningObjective(learningObjectiveId)) {
        auto listItem = new QListWidgetItem{};

        listItem->setText(item->text());
        listItem->setData(Qt::UserRole, item->data(Qt::UserRole));

        ui->SelectedLearningObjectives->addItem(listItem);
    }
}

void LearningObjectivePicker::on_AddSelectedButton_clicked()
{
    auto selectedItems = ui->CurrentLearningObjectives->selectedItems();

    for (auto& selectedItem : selectedItems) {
        addItemToSelectedObjectives(selectedItem);
    }
}

void LearningObjectivePicker::on_AddAllButton_clicked()
{
    for (int i = 0; i < ui->CurrentLearningObjectives->count(); i++) {
        addItemToSelectedObjectives(ui->CurrentLearningObjectives->item(i));
    }
}

void LearningObjectivePicker::on_RemoveAllButton_clicked()
{
    _classData->clearLearningObjectives();
    ui->SelectedLearningObjectives->clear();
}

void LearningObjectivePicker::on_RemoveSelectedButton_clicked()
{
    auto selecteditems = ui->SelectedLearningObjectives->selectedItems();

    for (auto& selectedItem : selecteditems) {
        _classData->removeLearningObjective(selectedItem->data(Qt::UserRole).toInt());
        delete selectedItem;
    }
}

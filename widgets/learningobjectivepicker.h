#ifndef LEARNINGOBJECTIVEPICKER_H
#define LEARNINGOBJECTIVEPICKER_H

#include "data/classdata.h"
#include "database/database.h"
#include "data/classobjectivedata.h"

#include <QWidget>
#include <QListWidgetItem>

namespace Ui {
class LearningObjectivePicker;
}

class LearningObjectivePicker : public QWidget
{
    Q_OBJECT

public:
    explicit LearningObjectivePicker(QWidget *parent = nullptr);
    ~LearningObjectivePicker();

public:
    void init(ILearningObjectiveData* classData, Db::Database* database);
    void setSelectedObjectives(const Db::LearningObjectiveVector& classObjectives);

private slots:
    void on_CategoryPicker_currentIndexChanged(int index);

    void on_AddSelectedButton_clicked();

    void on_AddAllButton_clicked();

    void on_RemoveAllButton_clicked();

    void on_RemoveSelectedButton_clicked();

private:
    void setCurrentLearningObjectiveList(int categoryId);
    void addItemToSelectedObjectives(QListWidgetItem* item);

private:
    Ui::LearningObjectivePicker *ui;

    ILearningObjectiveData* _classData = nullptr;
    Db::Database* _database = nullptr;
};

#endif // LEARNINGOBJECTIVEPICKER_H

#include "pdfview.h"

#include <QDebug>
#include <QtMath>

PdfView::PdfView(QWidget *parent) : QGraphicsView(parent)
{
    _scene = new QGraphicsScene();
    this->setScene(_scene);
}

PdfView::~PdfView()
{
    delete _scene;
}

void PdfView::resizeEvent(QResizeEvent *event)
{
    renderCurrentPage();
    QWidget::resizeEvent(event);
}

QSize PdfView::calculateImageSize() const
{
    auto pageSize = _currentPage->getSize();
    auto aspect = pageSize.width() / pageSize.height();

    auto canvasSize = size();
    return QSize(canvasSize.width(), qFloor(qreal(canvasSize.width()) / aspect));
}

void PdfView::renderCurrentPage()
{
    if (!_currentPage) {
        return;
    }

    auto imageSize = calculateImageSize();

    QImage image = _currentPage->render(imageSize);

    if (!_pdfPixmapItem) {
        _pdfPixmapItem = _scene->addPixmap(QPixmap::fromImage(image));
    }
    else {
        _pdfPixmapItem->setPixmap(QPixmap::fromImage(image));
    }

    _scene->setSceneRect(0.0, 0.0, imageSize.width(), imageSize.height());
}

QtPdfium::Page* PdfView::page() const
{
    return _currentPage;
}

void PdfView::setPage(int pageNumber)
{
    _currentPage = _document->getPage(pageNumber);

    if (_currentPage) {
        renderCurrentPage();
    }
}

void PdfView::setDocument(QtPdfium::Document* document)
{
    _document = document;
    setPage(0);
}

QtPdfium::Document* PdfView::document() const
{
    return _document;
}

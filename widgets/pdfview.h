#ifndef PDFVIEW_H
#define PDFVIEW_H

#include "qtpdfium/pdflibrary.h"

#include <QGraphicsView>
#include <QMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>


class PdfView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit PdfView(QWidget *parent = nullptr);
    ~PdfView() override;

public:    
    void setDocument(QtPdfium::Document* document);
    QtPdfium::Document* document() const;

    void setPage(int pageNumber);
    QtPdfium::Page* page() const;

    QSize calculateImageSize() const;

protected:
    void resizeEvent(QResizeEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override { event->ignore(); }
    void mouseReleaseEvent(QMouseEvent *event) override { event->ignore(); }
    void mouseMoveEvent(QMouseEvent *event) override { event->ignore(); }

private:
    void renderCurrentPage();

private:
    QtPdfium::Document* _document = nullptr;
    QtPdfium::Page* _currentPage = nullptr;

    QGraphicsScene* _scene = nullptr;
    QGraphicsPixmapItem* _pdfPixmapItem = nullptr;
};

#endif // PDFVIEW_H

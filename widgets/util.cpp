#include "util.h"

#include <QMessageBox>
#include <QFile>
#include <QFileInfo>

namespace Util {
void addStudentToList(const Db::Student& student, QTreeWidget *tree)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    setStudentItem(student, item);

    tree->addTopLevelItem(item);
}

void addStudentsToList(const Db::StudentVector& students, QTreeWidget *tree)
{
    for (int i = 0; i < students.size(); ++i)
    {
        addStudentToList(students[i], tree);
    }
}

void setStudentItem(const Db::Student& student, QTreeWidgetItem *item)
{
    item->setData(0, Qt::UserRole, student.id);
    item->setText(0, student.firstName);
    item->setText(1, student.lastName);
    item->setText(2, student.email);

    if (!student.active)
    {
        QColor inactive(125,125,125);
        item->setTextColor(0, inactive);
        item->setTextColor(1, inactive);
        item->setTextColor(2, inactive);
    }
}

void addLearningObjectiveToList(const Db::LearningObjective& learningObjective, QTreeWidget *tree)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setData(0, Qt::UserRole, learningObjective.id);
    item->setText(0, learningObjective.name);
    item->setText(1, learningObjective.description);

    tree->addTopLevelItem(item);
}

void addLearningObjectivesToList(const Db::LearningObjectiveVector& learningObjectives, QTreeWidget *tree)
{
    for (int i = 0; i < learningObjectives.size(); ++i)
    {
        addLearningObjectiveToList(learningObjectives[i], tree);
    }

    for(int i = 0; i < tree->columnCount(); i++)
        tree->resizeColumnToContents(i);
}

QString getStudentFullName(const Db::Student& student)
{
    return student.firstName + " " + student.lastName;
}

void addClassesToComboBox(const Db::ClassVector& classes, QComboBox *comboBox)
{
    for (int i = 0; i < classes.size(); ++i)
    {
        comboBox->addItem(classes[i].name, classes[i].id);
    }
}

void addLearningObjectivesToComboBox(const Db::LearningObjectiveVector& learningObjectives, QComboBox *comboBox)
{
    for (int i = 0; i < learningObjectives.size(); ++i)
    {
        comboBox->addItem(learningObjectives[i].name, learningObjectives[i].id);
    }
}

bool confirmDeleteSelectedItem(QWidget* parent, QString itemType)
{
    QString prompt = "Are you sure you want to delete the selected " + itemType + "?";
    QMessageBox::StandardButton reply = QMessageBox::question(parent, "Confrim Delete", prompt, QMessageBox::Yes|QMessageBox::No);

    return reply == QMessageBox::Yes;
}

void setClassItem(const Db::Class& c, QTreeWidgetItem *item)
{
    item->setData(0, Qt::UserRole, c.id);
    item->setText(0, c.name);

    if (!c.active)
    {
        item->setTextColor(0, QColor(125,125,125));
    }
}

void addRetakesToList(QTreeWidget* treeWidget, const Db::RetakeVector& retakes, Util::DateDisplay dateDisplay)
{
    for (int i = 0; i < retakes.size(); i++)
    {
        QTreeWidgetItem *item = Util::CreateRetakeItem(retakes[i], dateDisplay);
        treeWidget->addTopLevelItem(item);
    }

    for(int i = 0; i < treeWidget->columnCount(); i++)
        treeWidget->resizeColumnToContents(i);
}

QTreeWidgetItem* CreateRetakeItem(const Db::Retake& retake, DateDisplay dateDisplay)
{
    int column = 0;
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    item->setData(0, Qt::UserRole, retake.id);
    item->setText(column++, retake.studentName);
    item->setText(column++, retake.className);
    item->setText(column++, retake.LearningObjectiveName);
    item->setText(column++, retake.retakeVersionName);

    if (dateDisplay == DateDisplay::Time)
        item->setText(column++, retake.scheduledDate.toLocalTime().time().toString());
    else
        item->setText(column++, retake.scheduledDate.toLocalTime().toString());

    item->setCheckState(column++, retake.retakeCompleted ? Qt::Checked : Qt::Unchecked);
    item->setText(column++, retake.comments);

    return item;
}

QString fileNameFromPath(const QString& path)
{
    QFile f(path);
    QFileInfo fileInfo(f.fileName());
    return fileInfo.fileName();
}

void addRetakeVersionsToComboBox(QComboBox* comboBox, const Db::RetakeVersionVector& retakeVersions)
{
    for (auto& retakeVersion : retakeVersions){
        comboBox->addItem(retakeVersion.name, retakeVersion.id);
    }
}

void addLearningObjectiveCategoriesToList(const Db::LearningObjectiveCategoryVector categories, QListWidget* list)
{
    for (auto& category : categories) {
        addCategoryToList(category, list);
    }
}

void addCategoryToList(const Db::LearningObjectiveCategory& category, QListWidget* list)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText(category.name);
    item->setData(Qt::UserRole, category.id);

    list->addItem(item);
}

}

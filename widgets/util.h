#ifndef UI_UTIL_H
#define UI_UTIL_H

#include "database/database.h"

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QListWidget>
#include <QComboBox>

namespace Util {
    void addStudentToList(const Db::Student& student, QTreeWidget *tree);
    void addStudentsToList(const Db::StudentVector& students, QTreeWidget *tree);
    void setStudentItem(const Db::Student& student, QTreeWidgetItem *item);

    QString getStudentFullName(const Db::Student& student);

    void addLearningObjectiveToList(const Db::LearningObjective& learningObjective, QTreeWidget *tree);
    void addLearningObjectivesToList(const Db::LearningObjectiveVector& learningObjectives, QTreeWidget *tree);
    void addLearningObjectivesToComboBox(const Db::LearningObjectiveVector& learningObjectives, QComboBox *comboBox);

    void addClassesToComboBox(const Db::ClassVector& classes, QComboBox *comboBox);
    void setClassItem(const Db::Class& c, QTreeWidgetItem *item);

    bool confirmDeleteSelectedItem(QWidget* parent, QString itemType);

    enum class DateDisplay{DateAndTime, Time};
    void addRetakesToList(QTreeWidget* treeWidget, const Db::RetakeVector& retakes, Util::DateDisplay dateDisplay);
    QTreeWidgetItem* CreateRetakeItem(const Db::Retake& retake, DateDisplay dateDisplay);

    QString fileNameFromPath(const QString& path);

    void addRetakeVersionsToComboBox(QComboBox* comboBox, const Db::RetakeVersionVector& retakeVersions);

    void addLearningObjectiveCategoriesToList(const Db::LearningObjectiveCategoryVector categories, QListWidget* list);
    void addCategoryToList(const Db::LearningObjectiveCategory& category, QListWidget* list);
}
#endif // UI_UTIL_H
